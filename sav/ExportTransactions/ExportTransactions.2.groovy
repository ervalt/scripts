/**
 * Script that returns the transfers according to the 
 * following form parameters:
 * - groups: Comma separated list of group internal names
 * - dateFrom: Initial date of the period
 * - dateTo: End date of the period
 * - GW 04/08/2016
 */
import org.cyclos.entities.banking.Account
import org.cyclos.entities.banking.UserAccountType
import org.cyclos.entities.system.CustomFieldPossibleValue
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserGroup
import org.cyclos.impl.users.UserServiceLocal
import org.cyclos.impl.utils.QueryHelper
import org.cyclos.impl.utils.persistence.DBQuery
import org.cyclos.model.banking.accounts.AccountHistoriesOverviewQuery
import org.cyclos.model.banking.accounts.AccountVO
import org.cyclos.model.banking.accounts.AccountWithStatusVO
import org.cyclos.model.banking.accounttypes.AccountTypeNature
import org.cyclos.model.banking.transactions.TransactionVO
import org.cyclos.model.users.groups.BasicGroupVO
import org.cyclos.model.users.groups.UserGroupVO
import org.cyclos.model.users.users.UserOrderBy
import org.cyclos.model.users.users.UserQuery
import org.cyclos.model.users.users.UserWithFieldsVO
import org.cyclos.model.utils.DatePeriodDTO
import org.cyclos.model.utils.ModelHelper
import org.cyclos.server.utils.DateHelper
import org.cyclos.services.users.UserService

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils
import org.apache.commons.lang.time.DateUtils

import org.cyclos.entities.banking.SystemAccount
import org.cyclos.entities.banking.Transfer

import org.cyclos.entities.banking.QTransaction
import org.cyclos.entities.banking.QTransfer
import org.cyclos.entities.banking.QTransferFeeTransfer
import org.cyclos.entities.banking.QUserAccount
import com.querydsl.core.types.Projections

// import com.querydsl.core.types.*
// import com.querydsl.sql.*
// import com.querydsl.core.types.dsl.*
// import com.querydsl.core.types.dsl.PathBuilder
// import com.querydsl.core.types.SubQueryExpressionImpl
// import com.querydsl.sql.SQLQuery


info.addError( 1, "java.version " + System.getProperty("java.version"));
info.addError( 2, "Timezone " + TimeZone.getDefault());
// return info;


// evaluate selected groups
List<BasicGroupVO> groups = [];

for (CustomFieldPossibleValue cf : formParameters.groups)
{
	groups <<entityManagerHandler.find(UserGroup, cf.internalName);
	
}

// evaluate snapshot point in time (date and time)

/*
info.addError( 3, "dateFrom " + formParameters.dateFrom);
info.addError( 4, "dateTo " + formParameters.dateTo);
MyDate			mdFrom	= MyDate.of(formParameters.dateFrom); //.atStartOfDay();
info.addError( 5, "mdFrom is null? " + (mdFrom == null));
if (mdFrom) info.addError( 6, "mdFrom.isNull? " + mdFrom.isNull());
// MyDate			mdFrom	= MyDate.of(formParameters.dateFrom	?: new Date()).atStartOfDay();

info.addError( 9, "formParameters.dateTo is null? " + (formParameters.dateTo == null) + " " + (formParameters.dateTo ? formParameters.dateTo.getTime(): 0 ));
MyDate			mdTo	= MyDate.of(formParameters.dateTo)
info.addError( 7, "mdTo is null? " + (mdTo == null));
if (mdTo) info.addError( 8, "mdTo.isNull? " + mdTo.isNull());

if (mdTo.isToday()) {
	mdTo.setTimeToNow();
}
else if (!mdTo.isNull())
{
	// mdTo.setTimeToEndOfDay();
}

info.addError( 10, "mdTo: " + mdTo.dateTo());
*/

Date dateTo = DateUtils.addDays(
				DateUtils.addSeconds(
					formParameters.dateTo ?: new Date()
					, -1
					)
				, 1
				)

if (formParameters.dateFrom > dateTo)
{
	
	return new ResultError(300, "To date must be later than from that, or empty for today's date.");
}

SimpleDateFormat	datetimeFormatter =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

info.addError( 10, "dateFrom: " + datetimeFormatter.format(formParameters.dateFrom));
info.addError( 10, "dateTo: " + datetimeFormatter.format(dateTo));

//return info;

class ResultCount {
    Long id
}

/*
SQLQuery subQuery = new SQLQuery();

List<SubQueryExpression<Object[]>> sq = new ArrayList<SubQueryExpression<Object[]>>();
 
SQLQuery sqTIn = subQuery
			.from(
				qTIn.transfer
			)
info.addError( 101, sqTIn.getSQL().getSQL())
// sq.add(sq().from(qTIn.transfer));             
sq << sqTIn;
			
SQLQuery sqTOut = subQuery
			.from(
				qTOut.transfer
			)
info.addError( 101, sqTOut.getSQL().getSQL())
sq << sqTOut;

PathBuilder<Object[]> subAlias = new PathBuilder<Object[]>(Object[].class, "sub");        

SQLQuery masterQuery = new SQLQuery();
masterQuery = masterQuery
			.from(ua)
			.where(
				  ua.user().group().in(groups)
				// , ua.user().activationDate.lt(mdTo.toDate())
				//, ua.creationDate.lt(mdTo)
			)
			
info.addError( 101, masterQuery.getSQL().getSQL())

// def x = sqTIn
			// .union(sqTOut)
			// .as(subAlias)
// info.addError( 101, x.getSQL().getSQL())

// List<Object[]> results = query()
  // .from(sq().union(sq).as(subAlias))
  // .groupBy(subAlias.get("prop1"))
  // .list(subAlias.get("prop2"));

// List<SubQueryExpressionImpl> listTIn = sqTIn.list(
		  // qTIn.id
		// , qTIn.to.as("self")
		// , qTIn.from.as("other")
		// , qTIn.date
		// , qTIn.type.name
		// , qTIn.transactionNumber
		// , sqTOut.currencyAmount.getAmount()
		// , qTIn.description
		// );
		
// ListSubQuery listTOut = sqTOut.list(
		  // sqTOut.id
		// , sqTOut.from.as("self")
		// , sqTOut.to.as("other")
		// , sqTOut.date
		// , sqTOut.type.name
		// , sqTOut.transactionNumber
		// , sqTOut.currencyAmount.getAmount() //* -1
		// , sqTOut.description
		// );

// Path pTransactionsUnion = Expressions.path(Void.class, "pTransactionsUnion");
// subQuery = new SQLSubQuery();
// subQuery = subQuery.from(subQuery.union(listTIn,listTOut).as(pTransactionsUnion));

// Path namequery = Expressions.path(Void.class, "namequery");

// DBQuery queryUA = entityManagerHandler
			// .from(ua)
			// .where(
				  // ua.user().group().in(groups)
				// , ua.user().activationDate.lt(dateTo)
				// //, ua.creationDate.lt(dateTo)
			// )
			// .leftJoin(subQuery.list(
                // Expressions.path(Long.class, innerUnion, "id"),
                // Expressions.path(Long.class, innerUnion, "clientid"),
                // Expressions.stringPath(innerUnion, "clientname")),
              // namequery)
          // .on(t.id.eq(Expressions.path(Long.class, namequery, "id")));
		  
		  
		  
		  
// SQLSubQuery subQuery = new SQLSubQuery();
// subQuery = subQuery.from(t).join(t.fk462bdfe3e03a52d4, QClient.client);
// ListSubQuery clientByPaid = subQuery.list(t.id.as("id"), t.paidId.as("clientid"),
                                // QClient.client.name.as("clientname"));

// subQuery = new SQLSubQuery();
// subQuery = subQuery.from(t).where(t.paidId.isNull(), t.clientname.isNotNull());
// ListSubQuery clientByName = subQuery.list(t.id, Expressions.constant(-1L), 
                                  // t.clientname);
// I now need to build a path expressions to refer back to my inner query. It doesn't seem to matter which class I use for the path, so I've picked Void to emphasize this.

// subQuery = new SQLSubQuery();
// Path innerUnion = Expressions.path(Void.class, "innernamequery");
// subQuery = subQuery.from(subQuery.union(clientByPaid,clientByName).as(innerUnion));
// And a further path expression to express the on clause. Note that I join to a list() of the union query, with each column selected using the innerUnion path defined earlier.

// Path namequery = Expressions.path(Void.class, "namequery");
// query = query.leftJoin(subQuery.list(
                // Expressions.path(Long.class, innerUnion, "id"),
                // Expressions.path(Long.class, innerUnion, "clientid"),
                // Expressions.stringPath(innerUnion, "clientname")),
              // namequery)
          // .on(t.id.eq(Expressions.path(Long.class, namequery, "id")));


SQLQuery sqlQuery = new SQLQuery();
*/

/*
DBQuery queryUAin = entityManagerHandler
// DBQuery queryUAin = sqlQuery
			.from(ua)
			.where(
				  ua.user().group().in(groups)
				, ua.user().activationDate.lt(dateTo)
			)
			.innerJoin(t)
				.on(
					t.to.eq(ua)
				,	t.date.lt(dateTo)
				,	t.date.gt(formParameters.dateFrom)
			)
			// .leftJoin(trans)
				// .on(
					// trans.eq(t.transaction)
			// )
// info.addError( 101, queryUAin.getSQL()) //.getSQL())

DBQuery queryUAout = entityManagerHandler
// DBQuery queryUAout = sqlQuery
			.from(ua)
			.where(
				  ua.user().group().in(groups)
				, ua.user().activationDate.lt(dateTo)
			)
			.innerJoin(t)
				.on(
					t.from.eq(ua)
				,	t.date.lt(dateTo)
				,	t.date.gt(formParameters.dateFrom)
			)
			.leftJoin(trans)
				.on(
					trans.eq(t.transaction)
			)
// info.addError( 101, queryUAout.getSQL()) //.getSQL())
*/

// info.addError( 101, queryUAin.getSQL()) //.getSQL())

// DBQuery queryUAout = entityManagerHandler
			// .from(t)
			// .where(
					// t.date.lt(dateTo)
				// ,	t.date.gt(formParameters.dateFrom)
			// )
// info.addError( 101, queryUAout.getSQL()) //.getSQL())

class ResultBean {
    String		username
	String		accountType
    Long		id
	Date		tranferDate
	String		transferType
	String		other
    BigDecimal	amount
	String		transferNumber
	String		transactionNumber
	String		transactionDescription
	
	private SimpleDateFormat	dateFormatter =  new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat	datetimeFormatter =  new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public int direction = 0

	public int getIdInt()
	{
		return (int) this.id
	}
	
	public String getTransferDateFormatted()
	{
		return this.dateFormatter.format(this.tranferDate)
	}
	
	public String getAmountFormatted()
	{
		return this.amount.setScale(2).toPlainString();
	}
	
	public String getDescription()
	{
		return this.transactionDescription ?: "";
	}
	
	public String getSignedAmountFormatted()
	{
		return ((BigDecimal) (this.amount * this.direction)).setScale(2).toPlainString();
	}
}

class ResultBeanIn extends ResultBean {
	
	ResultBeanIn()
	{
		this.direction = 1
	}

}

class ResultBeanOut extends ResultBean {
	
	ResultBeanOut()
	{
		this.direction = -1
	}

}

/*
def resultIn = queryUAin.select(
				Projections.bean(
				  ResultBeanIn
				, ua.user().username
				, ua.type().name.as("accountType")
				, t.id
				, t.date.as("tranferDate")
				, t.type().name.as("transferType")
				, t.transaction.fromUser.username.as("other")
				, t.amount
				, t.transaction.transactionNumberRef.transactionNumber
				, t.transaction.description.as("transactionDescription")
				)
			); 

def resultOut = queryUAin.select(
				Projections.bean(
				  ResultBeanOut
				, ua.user().username
				, ua.type().name.as("accountType")
				, t.id
				, t.date.as("tranferDate")
				, t.type().name.as("transferType")
				, t.transaction.toUser.username.as("other")
				, t.amount
				, t.transaction.transactionNumberRef.transactionNumber
				, t.transaction.description.as("transactionDescription")
				)
			); 
*/
// def ua		= QUserAccount.userAccount
// QTransfer qTIn	= new QTransfer("tIn")
// QTransfer qTOut	= new QTransfer("tOut")
// def trans	= QTransaction.transaction


QUserAccount 			uaFrom	= new QUserAccount("uaFrom")
QUserAccount			uaTo	= new QUserAccount("uaTo")
QTransfer				t		= QTransfer.transfer
QTransferFeeTransfer	tf		= QTransferFeeTransfer.transferFeeTransfer

List<ResultBean> 		rows 	= new ArrayList<ResultBean>();


DBQuery queryTo = entityManagerHandler
			.from(t)
			.where(
					t.date.lt(dateTo)
				,	t.date.goe(formParameters.dateFrom)
				,	t.transaction.toUser.group().in(groups)
			)

def resultIn = queryTo.select(
				Projections.bean(
				  ResultBeanIn
				, t.transaction.toUser.username
				, t.to.type().name.as("accountType")
				, t.id
				, t.date.as("tranferDate")
				, t.type().name.as("transferType")
				// , t.transaction.fromUser.username.as("other")
				, t.amount
				, t.transactionNumberRef.transactionNumber.as("transferNumber")
				, t.transaction.transactionNumberRef.transactionNumber
				, t.transaction.description.as("transactionDescription")
				)
			); 

// rows.addAll(resultIn.fetch());

// DBQuery queryFeeFrom = queryTo
			// .innerJoin(tf)
			// .on(
				// tf.transaction().eq(t.transaction)
			// )
DBQuery queryFeeFrom = entityManagerHandler
			.from(tf)
			.where(
					tf.date.lt(dateTo)
				,	tf.date.goe(formParameters.dateFrom)
				,	tf.transaction.toUser.group().in(groups)
			)

def resultInFee = queryFeeFrom.select(
				Projections.bean(
				  ResultBeanIn
				, tf.transaction.toUser.username
				, tf.to.type().name.as("accountType")
				, tf.id
				, tf.date.as("tranferDate")
				, tf.transferFee().name.as("transferType")
				// , t.transaction.fromUser.username.as("other")
				, tf.amount
				, tf.transactionNumberRef().transactionNumber.as("transferNumber")
				, tf.transaction.transactionNumberRef.transactionNumber
				, tf.transaction.description.as("transactionDescription")
				)
			); 
rows.addAll(resultInFee.fetch());

DBQuery queryFrom = entityManagerHandler
			.from(t)
			.where(
					t.date.lt(dateTo)
				,	t.date.goe(formParameters.dateFrom)
				,	t.transaction.fromUser.group().in(groups)
			)

def resultOut = queryFrom.select(
				Projections.bean(
				  ResultBeanOut
				, t.transaction.fromUser.username
				, t.from.type().name.as("accountType")
				, t.id
				, t.date.as("tranferDate")
				, t.type().name.as("transferType")
				// , t.transaction.toUser.username.as("other")
				, t.amount
				, t.transactionNumberRef.transactionNumber.as("transferNumber")
				, t.transaction.transactionNumberRef.transactionNumber
				, t.transaction.description.as("transactionDescription")
				)
			); 

// rows.addAll(resultOut.fetch());

// DBQuery queryFeeTo = queryFrom
			// .innerJoin(tf)
			// .on(
				// tf.transaction.eq(t.transaction)
			// )


			//def totalCount = query.singleResult(u.id.countDistinct())



// List<ResultBean> rows 		= ArrayUtils.addAll(rowsIn, rowsOut);

// return info;
// List<ResultCount> resultCount = queryUA
			// .select(
				// Projections.bean(
					  // ResultCount
					// , ua.id
					// )
			// )
			// .fetch();
			
// def rows = []
// def totalCount = result.getTotalCount()
// Build the result
return [
    columns: [
        [header: "Member login", property: "username", width: "20%"],
        [header: "Account", property: "accountType", width: "20%"],
        [header: "Transaction type", property: "transferType", width: "20%"],
        [header: "Date", property: "transferDateFormatted", width: "20%"],
        [header: "Amount", property: "signedAmountFormatted", width: "20%"],
        [header: "From / To", property: "other", width: "20%"],
        [header: "Transfer Number", property: "transferNumber", width: "20%"],
        [header: "Transaction Number", property: "transactionNumber", width: "20%"],
        [header: "Description", property: "description", width: "20%"]
    ],
    rows: rows,
    // totalCount: rows.size()
]
