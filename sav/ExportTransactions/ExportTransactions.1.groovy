/**
 * Script that returns the transfers according to the 
 * following form parameters:
 * - groups: Comma separated list of group internal names
 * - fromDate: Initial date of the period
 * - toDate: End date of the period
 * - GW 04/08/2016
 */
import org.cyclos.entities.banking.Account
import org.cyclos.entities.banking.UserAccountType
import org.cyclos.entities.system.CustomFieldPossibleValue
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserGroup
import org.cyclos.impl.users.UserServiceLocal
import org.cyclos.impl.utils.QueryHelper
import org.cyclos.impl.utils.persistence.DBQuery
import org.cyclos.model.banking.accounts.AccountHistoriesOverviewQuery
import org.cyclos.model.banking.accounts.AccountVO
import org.cyclos.model.banking.accounts.AccountWithStatusVO
import org.cyclos.model.banking.accounttypes.AccountTypeNature
import org.cyclos.model.banking.transactions.TransactionVO
import org.cyclos.model.users.groups.BasicGroupVO
import org.cyclos.model.users.groups.UserGroupVO
import org.cyclos.model.users.users.UserOrderBy
import org.cyclos.model.users.users.UserQuery
import org.cyclos.model.users.users.UserWithFieldsVO
import org.cyclos.model.utils.DatePeriodDTO
import org.cyclos.model.utils.ModelHelper
import org.cyclos.server.utils.DateHelper
import org.cyclos.services.users.UserService


import org.cyclos.entities.banking.SystemAccount
import org.cyclos.entities.banking.Transfer



// evaluate selected groups
List<BasicGroupVO> groups = [];

for (CustomFieldPossibleValue cf : formParameters.groups)
{
	groups <<entityManagerHandler.find(UserGroup, cf.internalName);
	
}

def period = ModelHelper.datePeriod(
    conversionHandler.toDateTime(formParameters.fromDate),
    conversionHandler.toDateTime(formParameters.toDate))


def params = new AccountHistoriesOverviewQuery([
    	groups: groups,
    	period: period
    ])

def result = accountService.searchAccountHistoriesOverview(params)
def rows = []
def totalCount = result.getTotalCount()

result.getPageItems().each{ r ->
    def from
    def fromAccount 

    if(r.from.type.nature == AccountTypeNature.SYSTEM){
    	from = 'System'
    } else {
        fromAccount = entityManagerHandler.find(Account, r.from.id)
    	from = fromAccount.user.username
    }
    
    def to
    def toAccount
    if(r.to.type.nature == AccountTypeNature.SYSTEM){
        toAccount = entityManagerHandler.find(Account, r.to.id)
    	to = 'System'
    } else {
        toAccount = entityManagerHandler.find(Account, r.to.id)
    	to = toAccount.user.username
    }
    
    def transfer = entityManagerHandler.find(Transfer, r.id)
    def description = transfer.transaction != null ? transfer.transaction.description : ''
    


        rows <<[from: from,
                accountName: formatter.format(fromAccount),
                type: r.type.name,
                date: formatter.format(r.date),
                amount: formatter.format(r.currencyAmount.getAmount()),
                to: to,
                transactionNumber: r.transactionNumber,
                description: formatter.format(description)
            ]
      
 
}

// Build the result
return [
    columns: [
        [header: "From", property: "from", width: "20%"],
        [header: "Account", property: "accountName", width: "20%"],
        [header: "Transfer type", property: "type", width: "20%"],
        [header: "Date", property: "date", width: "20%"],
        [header: "Amount", property: "amount", width: "20%"],
        [header: "To", property: "to", width: "20%"],
        [header: "Transaction Number", property: "transactionNumber", width: "20%"],
        [header: "Description", property: "description", width: "20%"]
    ],
    rows: rows,
    totalCount: totalCount
]
