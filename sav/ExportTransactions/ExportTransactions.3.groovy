/**
 * Script that returns the transfers according to the 
 * following form parameters:
 * - groups: Comma separated list of group internal names
 * - fromDate: Initial date of the period
 * - toDate: End date of the period
 * - GW 04/08/2016
 */
import org.cyclos.entities.banking.Account;
import org.cyclos.entities.banking.UserAccountType;
import org.cyclos.entities.system.CustomFieldPossibleValue;
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserGroup
import org.cyclos.impl.users.UserServiceLocal
import org.cyclos.impl.utils.QueryHelper
import org.cyclos.impl.utils.persistence.DBQuery
import org.cyclos.model.banking.accounts.AccountHistoriesOverviewQuery
import org.cyclos.model.banking.accounts.AccountVO
import org.cyclos.model.banking.accounts.AccountWithStatusVO
import org.cyclos.model.banking.accounttypes.AccountTypeNature
import org.cyclos.model.banking.transactions.TransactionVO
import org.cyclos.model.users.groups.BasicGroupVO
import org.cyclos.model.users.groups.UserGroupVO
import org.cyclos.model.users.users.UserOrderBy
import org.cyclos.model.users.users.UserQuery
import org.cyclos.model.users.users.UserWithFieldsVO
import org.cyclos.model.utils.DatePeriodDTO
import org.cyclos.model.utils.ModelHelper
import org.cyclos.server.utils.DateHelper
import org.cyclos.services.users.UserService


import org.cyclos.entities.banking.SystemAccount
import org.cyclos.entities.banking.Transfer

import org.cyclos.entities.banking.QTransaction
import org.cyclos.entities.banking.QTransfer
import org.cyclos.entities.banking.QUserAccount

import com.querydsl.core.types.*
import com.querydsl.sql.*
import com.querydsl.core.types.dsl.*
// import com.querydsl.core.types.dsl.PathBuilder
// import com.querydsl.core.types.SubQueryExpressionImpl
// import com.querydsl.sql.SQLQuery


info.addError( 1, "java.version " + System.getProperty("java.version"));
info.addError( 2, "Timezone " + TimeZone.getDefault());
// return info;


// evaluate selected groups
List<BasicGroupVO> groups = [];

for (CustomFieldPossibleValue cf : formParameters.groups)
{
	groups <<entityManagerHandler.find(UserGroup, cf.internalName);
	
}

// evaluate snapshot point in time (date and time)
info.addError( 3, "fromDate " + formParameters.fromDate);
info.addError( 4, "toDate " + formParameters.toDate);
MyDate			mdFrom	= MyDate.of(formParameters.fromDate); //.atStartOfDay();
info.addError( 5, "mdFrom is null? " + (mdFrom == null));
if (mdFrom) info.addError( 6, "mdFrom.isNull? " + mdFrom.isNull());
// MyDate			mdFrom	= MyDate.of(formParameters.fromDate	?: new Date()).atStartOfDay();

info.addError( 9, "formParameters.toDate is null? " + (formParameters.toDate == null) + " " + (formParameters.toDate ? formParameters.toDate.getTime(): 0 ));
MyDate			mdTo	= MyDate.of(formParameters.toDate)
info.addError( 7, "mdTo is null? " + (mdTo == null));
if (mdTo) info.addError( 8, "mdTo.isNull? " + mdTo.isNull());

if (mdTo.isToday()) {
	mdTo.setTimeToNow();
}
else if (!mdTo.isNull())
{
	mdTo.setTimeToEndOfDay();
}

info.addError( 10, "mdTo: " + mdTo.toDate());

	
def ua		= QUserAccount.userAccount
def t		= QTransfer.transfer
QTransfer qTIn	= new QTransfer("tIn")
QTransfer qTOut	= new QTransfer("tOut")
def trans	= QTransaction.transaction

class ResultCount {
    Long id
}


SQLQuery subQuery = new SQLQuery();

List<SubQueryExpression<Object[]>> sq = new ArrayList<SubQueryExpression<Object[]>>();
 
SQLQuery sqTIn = subQuery
			.from(
				qTIn.transfer
			)
info.addError( 101, sqTIn.getSQL().getSQL())
// sq.add(sq().from(qTIn.transfer));             
sq << sqTIn;
			
SQLQuery sqTOut = subQuery
			.from(
				qTOut.transfer
			)
info.addError( 101, sqTOut.getSQL().getSQL())
sq << sqTOut;

PathBuilder<Object[]> subAlias = new PathBuilder<Object[]>(Object[].class, "sub");        

SQLQuery masterQuery = new SQLQuery();
masterQuery = masterQuery
			.from(ua)
			.where(
				  ua.user().group().in(groups)
				// , ua.user().activationDate.lt(mdTo.toDate())
				//, ua.creationDate.lt(mdTo)
			)
			
info.addError( 101, masterQuery.getSQL().getSQL())

def x = sqTIn
			.union(sqTOut)
			// .as(subAlias)
info.addError( 101, x.getSQL().getSQL())

// List<Object[]> results = query()
  // .from(sq().union(sq).as(subAlias))
  // .groupBy(subAlias.get("prop1"))
  // .list(subAlias.get("prop2"));

// List<SubQueryExpressionImpl> listTIn = sqTIn.list(
		  // qTIn.id
		// , qTIn.to.as("self")
		// , qTIn.from.as("other")
		// , qTIn.date
		// , qTIn.type.name
		// , qTIn.transactionNumber
		// , sqTOut.currencyAmount.getAmount()
		// , qTIn.description
		// );
		
// ListSubQuery listTOut = sqTOut.list(
		  // sqTOut.id
		// , sqTOut.from.as("self")
		// , sqTOut.to.as("other")
		// , sqTOut.date
		// , sqTOut.type.name
		// , sqTOut.transactionNumber
		// , sqTOut.currencyAmount.getAmount() //* -1
		// , sqTOut.description
		// );

// Path pTransactionsUnion = Expressions.path(Void.class, "pTransactionsUnion");
// subQuery = new SQLSubQuery();
// subQuery = subQuery.from(subQuery.union(listTIn,listTOut).as(pTransactionsUnion));

// Path namequery = Expressions.path(Void.class, "namequery");

// DBQuery queryUA = entityManagerHandler
			// .from(ua)
			// .where(
				  // ua.user().group().in(groups)
				// , ua.user().activationDate.lt(dateTo)
				// //, ua.creationDate.lt(dateTo)
			// )
			// .leftJoin(subQuery.list(
                // Expressions.path(Long.class, innerUnion, "id"),
                // Expressions.path(Long.class, innerUnion, "clientid"),
                // Expressions.stringPath(innerUnion, "clientname")),
              // namequery)
          // .on(t.id.eq(Expressions.path(Long.class, namequery, "id")));
		  
		  
		  
		  
// SQLSubQuery subQuery = new SQLSubQuery();
// subQuery = subQuery.from(t).join(t.fk462bdfe3e03a52d4, QClient.client);
// ListSubQuery clientByPaid = subQuery.list(t.id.as("id"), t.paidId.as("clientid"),
                                // QClient.client.name.as("clientname"));

// subQuery = new SQLSubQuery();
// subQuery = subQuery.from(t).where(t.paidId.isNull(), t.clientname.isNotNull());
// ListSubQuery clientByName = subQuery.list(t.id, Expressions.constant(-1L), 
                                  // t.clientname);
// I now need to build a path expressions to refer back to my inner query. It doesn't seem to matter which class I use for the path, so I've picked Void to emphasize this.

// subQuery = new SQLSubQuery();
// Path innerUnion = Expressions.path(Void.class, "innernamequery");
// subQuery = subQuery.from(subQuery.union(clientByPaid,clientByName).as(innerUnion));
// And a further path expression to express the on clause. Note that I join to a list() of the union query, with each column selected using the innerUnion path defined earlier.

// Path namequery = Expressions.path(Void.class, "namequery");
// query = query.leftJoin(subQuery.list(
                // Expressions.path(Long.class, innerUnion, "id"),
                // Expressions.path(Long.class, innerUnion, "clientid"),
                // Expressions.stringPath(innerUnion, "clientname")),
              // namequery)
          // .on(t.id.eq(Expressions.path(Long.class, namequery, "id")));



return info;

// DBQuery queryUA = entityManagerHandler
			// .from(ua)
			// .where(
				  // ua.user().group().in(groups)
				// , ua.user().activationDate.lt(dateTo)
				// //, ua.creationDate.lt(dateTo)
			// )
			// .innerJoin(t)
				// .on(
					// t.from.eq(ua).or(t.to.eq(ua))
				// ,	t.date.lt(dateTo)
			// )
			// .leftJoin(trans)
				// .on(
					// trans.eq(t.transaction)
			// )

// List<ResultCount> resultCount = queryUA
			// .select(
				// Projections.bean(
					  // ResultCount
					// , ua.id
					// )
			// )
			// .fetch();
			
// def rows = []
// def totalCount = result.getTotalCount()

// result.getPageItems().each{ r ->
    // def from
    // def fromAccount 

    // if(r.from.type.nature == AccountTypeNature.SYSTEM){
    	// from = 'System'
    // } else {
        // fromAccount = entityManagerHandler.find(Account, r.from.id)
    	// from = fromAccount.user.username
    // }
    
    // def to
    // def toAccount
    // if(r.to.type.nature == AccountTypeNature.SYSTEM){
        // toAccount = entityManagerHandler.find(Account, r.to.id)
    	// to = 'System'
    // } else {
        // toAccount = entityManagerHandler.find(Account, r.to.id)
    	// to = toAccount.user.username
    // }
    
    // def transfer = entityManagerHandler.find(Transfer, r.id)
    // def description = transfer.transaction != null ? transfer.transaction.description : ''
    


        // rows <<[from: from,
                // accountName: formatter.format(fromAccount),
                // type: r.type.name,
                // date: formatter.format(r.date),
                // amount: formatter.format(r.currencyAmount.getAmount()),
                // to: to,
                // transactionNumber: r.transactionNumber,
                // description: formatter.format(description)
            // ]
      
 
// }

// Build the result
return [
    columns: [
        [header: "From", property: "from", width: "20%"],
        [header: "Account", property: "accountName", width: "20%"],
        [header: "Transfer type", property: "type", width: "20%"],
        [header: "Date", property: "date", width: "20%"],
        [header: "Amount", property: "amount", width: "20%"],
        [header: "To", property: "to", width: "20%"],
        [header: "Transaction Number", property: "transactionNumber", width: "20%"],
        [header: "Description", property: "description", width: "20%"]
    ],
    rows: rows,
    totalCount: totalCount
]
