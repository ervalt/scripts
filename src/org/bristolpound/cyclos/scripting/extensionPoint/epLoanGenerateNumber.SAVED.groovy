import org.cyclos.entities.banking.Payment;
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserCustomFieldValue;

print "epLoanGenerateNumber_v1";

User u			= transaction.getToUser();
String bcuMNo	= u.getCustomValues().find{UserCustomFieldValue ucfv -> ucfv.field.internalName == 'bcumembernumber'}.getStringValue();
String no		= BpCheckDigit.BP_CHECK_DIGIT.format("${bcuMNo}BP-$transaction.id");

// Cyclos < 4.9
//transaction.transactionNumberRef.transactionNumber

// Cyclos >= 4.9
transaction.transactionNumber =  no;
if (transaction instanceof Payment) {
	Payment payment = (Payment) transaction
	if (payment.transfer != null) {
		payment.transfer.transactionNumber = payment.transactionNumber
	}
}

println "-> $no"
