import org.cyclos.entities.banking.Account;
import org.cyclos.entities.banking.AccountType;
import org.cyclos.entities.banking.TransactionCustomField
import org.cyclos.entities.users.RecordCustomField
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserRecord
import org.cyclos.impl.banking.LocateAccountOwnerResult
import org.cyclos.model.EntityNotFoundException
import org.cyclos.model.banking.accounts.AccountHistoryQuery;
import org.cyclos.model.banking.accounts.AccountOwner
import org.cyclos.model.banking.accounts.AccountVO;
import org.cyclos.model.banking.transferstatus.TransferStatusVO;
import org.cyclos.model.banking.transfertypes.TransferTypeVO;


AccountType	accountType	= entityManagerHandler.find(AccountType, 'loan_account')

User getUser(RecordCustomField f)
{
	UserRecord ur = record

	return ur.getUser();
}

User getUser(TransactionCustomField f)
{
	AccountOwner				aoFrom		= fromOwner;
	AccountOwner				aoTo		= fromTo;

	return (aoFrom instanceof User) ? aoFrom : aoTo;
}

Account getAccount(AccountType accountType, RecordCustomField f)
{
	// Find the account
	Account		account		= accountService.load(getUser(f), accountType)
}

Account getAccount(AccountType accountType, TransactionCustomField f)
{
	LocateAccountOwnerResult	laorFrom	= fromOwnerResult;
	LocateAccountOwnerResult	laorTo		= toOwnerResult;

	try
	{
		return laorFrom.getAccount(accountType)
	}
	catch(EntityNotFoundException e)
	{
		return laorTo.getAccount(accountType)
	}

}


// Find the account
Account		account		= getAccount(accountType, field)

// The account history has transfers. We need the transactions.
def q = new AccountHistoryQuery()
q.setUnlimited()
q.account = new AccountVO(account.id)
q.transferTypes = [
	new TransferTypeVO(internalName: 'loanReconciliation.bp_loan')
]
q.statuses = [
	new TransferStatusVO(internalName: 'loanStatus.onSchedule'),
	new TransferStatusVO(internalName: 'loanStatus.new'),

]
def transfers = accountService.searchAccountHistory(q).pageItems

// Return the transaction ids
return transfers.collect {it.transactionId}
// Does not work:
//return transfers.collect {[label: "test", value: it.transactionId, className: it.getClass().getName()]}

