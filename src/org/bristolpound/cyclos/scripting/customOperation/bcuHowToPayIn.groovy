import org.cyclos.entities.users.User;
import org.cyclos.model.system.fields.CustomFieldVO;
import org.cyclos.entities.system.CustomOperation;

CustomOperation cu = binding.customOperation;

User user = binding.user ?: binding.sessionData.loggedUser;

def bean = scriptHelper.wrap(user)

return cu.informationText.replace('{bcumembernumber}', bean.bcumembernumber);
