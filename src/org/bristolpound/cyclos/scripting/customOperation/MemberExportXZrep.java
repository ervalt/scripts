/**
 *
 * Generates an X- or Z-Report for the current user.
 *
 * Included libraries:
 *		- ErrorHandling
 *
 * Script parameters:
 * 		- reportType	String		Required	[reportShowZ,zNew]
 *												x, reportShowZ: generates a X-report
 *												z, reportShowZ: shows last Z-report
 *												zNew: generates a Z-report (resetting the start date)
 *
 * Global Variable Store:
 *		- None
 *
 * System Records:
 *		- None
 *
 * User Records:
 *		- Z-report
 *
 * Custom form fields:
 * 		- Date			dateTo		Optional	Date part of point in time for the snapshot.
 *												Null yields in current date.
 *		- String		timeTo		Optional	Time part of the snapshot in the format
 *												"HH:mm".
 *												Null yields in current time.
 *												Seconds are always "59".
 *
 * Returns
 // *		org.cyclos.model.system.operations.CustomOperationPageResult:
 // * 		- int			#			Optional	Line number
 // * 		- String		Member		Default		Username of the account owner.
 // * 		- String 		Number		Optional	Account id
 // * 		- String 		Type		Default		Account type
 // * 		- Boolean		Active		Optional	Account active
 // * 		- BigDecimal	Balance		Default		Account balance at the given point in time
 // *												(see fields).
 *
 * Version:
 *		0.5, 2016-11-22
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 * 		- Get lables from language
 *
 **/

 //import
import java.util.Date;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.time.DateUtils

import org.cyclos.entities.banking.Account
import org.cyclos.entities.banking.UserAccountType
import org.cyclos.entities.system.CustomFieldPossibleValue
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserGroup
import org.cyclos.impl.users.UserServiceLocal
import org.cyclos.impl.utils.QueryHelper
import org.cyclos.impl.utils.persistence.DBQuery
import org.cyclos.model.banking.accounts.AccountHistoriesOverviewQuery
import org.cyclos.model.banking.accounts.AccountVO
import org.cyclos.model.banking.accounts.AccountWithStatusVO
import org.cyclos.model.banking.accounttypes.AccountTypeNature
import org.cyclos.model.banking.transferfilters.TransferFilterVO
import org.cyclos.model.users.groups.BasicGroupVO
import org.cyclos.model.users.groups.UserGroupVO
import org.cyclos.model.users.users.UserOrderBy
import org.cyclos.model.users.users.UserQuery
import org.cyclos.model.users.users.UserWithFieldsVO
import org.cyclos.model.utils.DatePeriodDTO
import org.cyclos.model.utils.ModelHelper
import org.cyclos.server.utils.DateHelper
import org.cyclos.services.users.UserService

import org.cyclos.model.users.records.UserRecordQuery;
// import org.cyclos.model.users.records.UserRecordSearchData;
import org.cyclos.services.users.RecordService;
import org.cyclos.services.users.RecordTypeService;

import org.cyclos.entities.banking.Transfer
import org.cyclos.entities.users.RecordCustomField
import org.cyclos.entities.users.SystemRecord
import org.cyclos.entities.users.SystemRecordType
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserRecord
import org.cyclos.entities.users.UserRecordType
import org.cyclos.impl.system.ScriptHelper
import org.cyclos.impl.users.RecordServiceLocal
import org.cyclos.impl.utils.persistence.EntityManagerHandler
import org.cyclos.model.EntityNotFoundException
import org.cyclos.model.banking.accounts.AccountHistoryEntryVO
import org.cyclos.model.banking.accounts.AccountHistoryOverviewEntryVO
import org.cyclos.model.banking.accounts.AccountHistoryQuery
import org.cyclos.model.banking.accounts.AccountHistoryStatusVO
import org.cyclos.model.banking.accounts.InternalAccountOwner
import org.cyclos.model.banking.transfertypes.TransferTypeVO
import org.cyclos.model.system.fields.CustomFieldVO
import org.cyclos.model.system.fields.CustomFieldValueForSearchDTO
import org.cyclos.model.users.records.RecordDataParams
import org.cyclos.model.users.records.UserRecordDTO
import org.cyclos.model.users.records.UserRecordVO
import org.cyclos.model.users.recordtypes.RecordTypeVO
import org.cyclos.model.users.users.UserLocatorVO
import org.cyclos.model.users.users.UserVO
import org.cyclos.utils.Page
import org.cyclos.utils.ParameterStorage

import org.cyclos.model.CyclosException

info.addError( 1, "pageContext " + pageContext);
info.addError( 1, "isCSV " + isCSV);
info.addError( 1, "isPDF " + isPDF);
info.addError( 1, "isPAGE " + isPAGE);
// return info;

info.addError( 1, user.username + " " + user.id);
	

info.addError( 1, user.products.size());
info.addError( 1, user.user.username + " " + user.user.id);

def ul = new UserLocatorVO(user.id);
info.addError( 1, ul.username + " " + ul.id);

class ResultMessageList extends ResultList
{
	ResultMessageList()
	{
		this.addColumn([header: "Message", property: "description", width: "100%", align: "left"]);
	}
}


class UserRecordZReport
{
	private	UserRecordZReportList	urZRep
	private UserRecord 				urReport;
	// private UserRecordDTO			dtoReport;
	private	Map<String, Object> 	wrapReport;
	
	UserRecordZReport(UserRecordZReportList urZRep, UserRecord urReport)
	{
		this.urZRep		= urZRep;
		
		// if (urReport == null)
		// {
			// throw new CyclosException("urReport is null in UserRecordZReport(UserRecordZReportList urZRep, UserRecord urReport)");
		// }
		
		// if (urReport.type != this.urZRep.recordType)
		// {
			// throw new CyclosException("urReport.type ("+urReport.type+") is not this.urZRep.recordType ("+this.urZRep.recordType+")");
		// }
		
		this.setUserRecord(urReport);
	}
	
	UserRecordZReport(UserRecordZReportList urZRep, UserRecordVO urReportVO)
	{
		this.urZRep		= urZRep;
		
		UserRecord urReport = urZRep.entityManagerHandler.find(UserRecord, urReportVO.id)
		
		// if (urReport == null || urReport.type != this.urZRep.recordType)
		// {
			// throw new CyclosException("urReport is null in UserRecordZReport(UserRecordZReportList urZRep, UserRecordVO urReport)");
		// }
		
		this.setUserRecord(urReport);
	}
	
	UserRecordZReport(UserRecordZReportList urZRep, Long id)
	{
		this.urZRep		= urZRep;
		
		// if (id == null)
		// {
			// throw new CyclosException("urReport is null in UserRecordZReport(UserRecordZReportList urZRep, Long id)");
		// }
		
		UserRecord urReport = urZRep.entityManagerHandler.find(UserRecord, id);
		
		// if (urReport == null || urReport.type != this.urZRep.recordType)
		// {
			// throw new EntityNotFoundException();
		// }
		
		this.setUserRecord(urReport);
	}
	
	UserRecordZReport(
		UserRecordZReportList urZRep,
		Date		reportGenerationDate, 
		BigDecimal	totalIn,
		BigDecimal	totalOut,
		BigDecimal	balance
		)
	{
		
	}
	
	public UserRecordZReport getPreviousReport()
	{
		if (urReport == null) return null;
		if (wrapReport.prevReportId == null) return null;
		
		return new UserRecordZReport(this.urZRep, wrapReport.prevReportId);
	}
	
	public UserRecord getUserRecord()
	{
		return this.urReport;
	}
	
	public setUserRecord(UserRecord urReport)
	{
		this.urReport	= urReport;
		//this.dtoReport	= (urReport == null) ? null : this.urZRep.recordService.toDTO(this.urReport);
		this.wrapReport = (urReport == null) ? null : this.urZRep.scriptHelper
								.wrap(this.urReport, this.urZRep.recordType.fields)
	}
	
	public UserRecordDTO getDto()
	{
		// if (this.urReport == null)
		// {
			// throw new CyclosException("this.urReport is null in getDto()");
		// }
		
		return this.urZRep.recordService.toDTO(this.urReport)
		return this.dtoReport;
	}
	
	public get(String property)
	{
		
		// if (this.urReport == null)
		// {
			// throw new EntityNotFoundException();
		// }
		
		if (property.length() > 9 && "Formatted" == org.apache.commons.lang.StringUtils.right(property, 9 ))
		{
			
			property = property.substring(0, Math.max(0, property.length() - 9));
			
			if (property == "totalDelta")
			{
				return ((BigDecimal) ((this.wrapReport["totalIn"]?:0)-(this.wrapReport["totalOut"]?:0))).setScale(2).toPlainString();
			}
			else if (!this.wrapReport.containsKey(property))
			{
				return null;
			}
			else if (this.wrapReport."$property" == null)
			{
				return "";
			}
			else if (this.wrapReport."$property" instanceof Date)
			{
				return new SimpleDateFormat("yyyy-MM-dd HH:mm").format( this.wrapReport."$property");
			}
			else if (this.wrapReport."$property" instanceof BigDecimal)
			{
				return (String) this.wrapReport."$property".setScale(2).toPlainString();
			}			
		}
		
		return this.wrapReport."$property"
	}
	
	public set(String property, value)
	{
		// if (this.urReport == null)
		// {
			// throw new EntityNotFoundException();
		// }
		
		// if (!this.wrapReport.containsKey(property))
		// {
			// throw new EntityNotFoundException();
		// }
		
		this.wrapReport."$property" = value;
	}
	
	public save()
	{
		this.urZRep.recordService.save(this.urZRep.recordService.toDTO(this.urReport))
	}
}

class UserRecordZReportList
{
    String							recordTypeName
	User							user
	int								totalCount;

    UserRecordType					recordType
    RecordTypeVO					recordTypeVO					
    Map<String, RecordCustomField>	fields;

    private EntityManagerHandler	entityManagerHandler;
    private RecordServiceLocal		recordService;
    private ScriptHelper			scriptHelper;
	private ResultInfoList			info;
	private Map<String, Object>		PROPERTIES = new HashMap<String, Object>();					;

    public UserRecordZReportList(Object binding)
	{
		this.UserRecordZReportListHelper(binding);
    }

    public UserRecordZReportList(Object binding, ResultInfoList info)
	{
		this.info = info;
		this.UserRecordZReportListHelper(binding);
	}
	
    private UserRecordZReportListHelper(Object binding)
	{
        def params 		= binding.scriptParameters
		recordTypeName	= "z_reports";
        // recordTypeName	= params.'payment.recordType' ?: 'paypalPayment'
        // paymentIdName	= params.'payment.paymentId' ?: 'paymentId'
        // amountName		= params.'payment.amount' ?: 'amount'
        // transactionName = params.'payment.transaction' ?: 'transaction'

        user					= binding.user
		entityManagerHandler	= binding.entityManagerHandler
        recordService			= binding.recordService
        scriptHelper			= binding.scriptHelper
        recordType				= binding.entityManagerHandler.find(UserRecordType, recordTypeName)
		recordTypeVO			= new RecordTypeVO(id: recordType.id);
        fields = [:]
        recordType.fields.each {f -> fields[f.internalName] = f}
    }
	
	public getBinding()
	{
		return this.binding
	}
	
	public getRecordService()
	{
		return this.recordService
	}
	
    /**
     * Creates a payment record, for the given user and JSON,
     * as returned from PayPal's create payment REST method
     */
    public UserRecordDTO prepare(
		Long		accountId,
		Date		reportGenerationDate, 
		BigDecimal	totalIn,
		BigDecimal	totalOut,
		Long		transIn,
		Long		transOut,
		BigDecimal	balance
		)
	{
        RecordDataParams newParams				= new RecordDataParams([
													user: new UserLocatorVO(id: this.user.id),
													recordType: this.recordTypeVO,
													])
        UserRecordDTO dtoXZReport				= recordService.getDataForNew(newParams).getDto()
        Map<String, Object> wrapped				= scriptHelper.wrap(dtoXZReport, recordType.fields)
        
		wrapped["accountId"]					= (int) accountId;
		wrapped["reportGenerationDate"]			= reportGenerationDate ?: new Date();
		wrapped["totalIn"]						= totalIn;
		wrapped["totalOut"]						= totalOut;
		wrapped["transIn"]						= transIn;
		wrapped["transOut"]						= transOut;
		wrapped["balance"]						= balance;
		
        // Return the DTO
        return dtoXZReport;
    }
	
    public UserRecord create(
		Long					accountId,
		Date					reportGenerationDate,
		AccountHistoryStatusVO	pageAHS
		)
	{
		return this.create(
			accountId,
			reportGenerationDate,
			pageAHS.incoming.sum,
			pageAHS.outgoing.sum,
			pageAHS.incoming.count,
			pageAHS.outgoing.count,
			pageAHS.balanceAtEnd
		)
	}

   /**
     * Creates a payment record, for the given user and JSON,
     * as returned from PayPal's create payment REST method
     */
    public UserRecord create(
		Long		accountId,
		Date		reportGenerationDate, 
		BigDecimal	totalIn,
		BigDecimal	totalOut,
		Long		transIn,
		Long		transOut,
		BigDecimal	balance
		)
	{
		UserRecordDTO		dtoXZReport	=	this.prepare(
												accountId,
												reportGenerationDate, 
												totalIn,
												totalOut,
												transIn,
												transOut,
												balance
											);
		
		UserRecordZReport	lastReport	=	this.getLastReport(accountId);
		
		return this.save(dtoXZReport, lastReport);
	}

    public UserRecord save(
		UserRecordDTO	dtoXZReport, 
		Long			accountId
		)
	{
	}
	
    public UserRecord save(
		UserRecordDTO		dtoXZReport,
		UserRecordZReport	lastReport
		)
	{
		
		Map<String, Object> wrapXZReport;
		
		wrapXZReport					= this.scriptHelper.wrap(dtoXZReport, this.recordType.fields);
		wrapXZReport["isLastReport"]	= true;

		if (lastReport != null)
		{
			// save id to new record
			wrapXZReport["prevReportId"]	= lastReport.id;
			
			// unset isLastReport in previous report
			lastReport.isLastReport			= false;
			lastReport.save();
		}

        // Save the record DTO and return the entity
        Long id = recordService.save(dtoXZReport);
        return entityManagerHandler.find(UserRecord, id);
    }

    /**
     * Finds the record by id
     */
    public UserRecord find(Long id)
	{
        try {
            UserRecord userRecord = entityManagerHandler.find(UserRecord, id)
            if (userRecord.type != recordType)
			{
                return null
            }
            return userRecord
			
        } catch (EntityNotFoundException e)
		{
            return null
        }
    }

    /**
     * Removes the given record, but only if it is of the
     * expected type and hasn't been confirmed
     */
    public void remove(UserRecord userRecord)
	{
        
		if (userRecord.type != recordType) {
            return
        }
        
		Map<String, Object> wrapped = scriptHelper
                .wrap(userRecord, recordType.fields)
        
		if (wrapped[transactionName] != null) return
		
		entityManagerHandler.remove(userRecord)
    }
	
	public UserRecordZReport getLastReport(Long accountId)
	{
		if (PROPERTIES.containsKey("lastReport"))
		{
			return PROPERTIES.get("lastReport");
		}
		
		// create user filter
		def urq				= new UserRecordQuery();
		urq.type			= this.recordTypeVO;
		urq.user 			= new UserVO(user.id);
		urq.pageSize		= 1;
		
		Set setCustomValues	= new HashSet();
		CustomFieldValueForSearchDTO cf;
		
		cf					= new CustomFieldValueForSearchDTO();
		cf.field			= new CustomFieldVO(this.fields["isLastReport"].id);
		cf.booleanValue		= true;
		setCustomValues.add(cf);
		
		cf					= new CustomFieldValueForSearchDTO();
		cf.field			= new CustomFieldVO(this.fields["accountId"].id);
		cf.integerRange		= new org.cyclos.model.utils.IntegerRangeDTO([min: accountId, max: accountId]);
		setCustomValues.add(cf);
		
		urq.customValues	= setCustomValues;
		
		// execute query
		def pageUserRecords = recordService.search(urq);
		
		info.addError( 6, pageUserRecords.pageItems.size());
		
		if (1 == pageUserRecords.pageItems.size())
		{
			return new UserRecordZReport(this, pageUserRecords.pageItems[0]);
		}
		
		return null;
	}
	
	public getColumns()
	{
		return [
			// ToDo: Get lables from Language
			[header: "Id",			property: "id",								width: "5%", align: "left"],
			[header: "Date",		property: "reportGenerationDateFormatted",	width: "10%", align: "left"],
			[header: "Total In",	property: "totalInFormatted",				width: "10%", align: "right"],
			[header: "Total Out",	property: "totalOutFormatted",				width: "10%", align: "right"],
			[header: "Trans In",	property: "transInFormatted",				width: "10%", align: "right"],
			[header: "Trans Out",	property: "transOutFormatted",				width: "10%", align: "right"],
			[header: "Total Net",	property: "totalDeltaFormatted",			width: "10%", align: "right"],
			[header: "Balance",		property: "balanceFormatted",				width: "10%", align: "right"],
			[header: "Account", 	property: "accountId", 						width: "10%", align: "right"],
			[header: "Previous",	property: "prevReportId",					width: "10%", align: "right"],
			[header: "last",		property: "isLastReport",					width: "10%", align: "right"],
			]
	}

	// public List<UserRecordZReport> getRows()
	public List getRows()
	{
		// List<Map<String, Object>> rows = [];
		// List<UserRecordZReport> rows = [];
		def rows = [];
		
		// create user filter
		def urq		= new UserRecordQuery();
		urq.type	= this.recordTypeVO;
		urq.user 	= new UserVO(user.id);

		// execute query
		def pageUserRecords = recordService.search(urq);
		this.totalCount = pageUserRecords.totalCount;

		// loop through resultset
		for ( UserRecordVO u : pageUserRecords.pageItems )
		{
			// rows <<new UserRecordZReport(this, u.id)
			UserRecordZReport ur = new UserRecordZReport(this, u.id);
			if (ur != null)
			{
				rows <<[
						id:								(int) ur.id, 
						reportGenerationDate:			ur.reportGenerationDate,
						reportGenerationDateFormatted:	ur.reportGenerationDateFormatted,
						totalIn:						ur.totalIn,
						totalInFormatted:				ur.totalInFormatted,
						totalOut:						ur.totalOut,
						totalOutFormatted:				ur.totalOutFormatted,
						totalDelta:						ur.totalDelta,
						totalDeltaFormatted:			ur.totalDeltaFormatted,
						transIn:						ur.transIn,
						transInFormatted:				ur.transInFormatted,
						transOut:						ur.transOut,
						transOutFormatted:				ur.transOutFormatted,
						balance:						ur.balance,
						balanceFormatted:				ur.balanceFormatted,
						accountId:						ur.accountId,
						prevReportId:					ur.prevReportId,
						isLastReport:					ur.isLastReport,
						]
			}
		}
		
		return rows;
	}
}

def sP	= binding.scriptParameters;
def fP	= binding.formParameters;

String repType = sP.reportType ?: fP.reportType ? fP.reportType?.internalName : "reportShowHistory";

info.addError( 5, repType);
// return info;


UserRecordZReportList urZRep = new UserRecordZReportList(binding, info);


switch (repType.toLowerCase())
{
		
	case "reportShowZ".toLowerCase():
		repType = "z";		
	case "z":
	case "zCurrent".toLowerCase():
	case "reportShowX".toLowerCase():
		if (repType == "reportShowX") repType = "x";
	case "x":
	case "zNew".toLowerCase():
		
			// return info;
		List 				rows 		= [];
		Date 				dateFrom;
		
		// loop through the user accounts
		List<Account> myAccounts = accountService.list(user);
		
		for (Account myAccount : myAccounts )
		{
			
			AccountVO			myAccountVO	= new AccountVO(myAccount.id);
			UserRecordZReport	s			= urZRep.getLastReport(myAccount.id);
			UserRecordZReport	sPrevious;
			
			switch (repType.toLowerCase())
			{
					
				case "zCurrent".toLowerCase():
					
					if (s == null)
					{
						return  [columns: [[header: "Result", property: "text", width: "100%", align: "left"]], rows: []];
					}
					
					dateTo		= s.reportGenerationDate;
					
					sPrevious	= s.previousReport;
					
					dateFrom	= sPrevious != null
								? sPrevious.reportGenerationDate
								: DateUtils.addMilliseconds(
									// ToDo:
									// DateUtils.ceiling(myAccount.creationDate, Calendar.MINUTE),
									DateUtils.ceiling(user.activationDate, Calendar.MINUTE),
									-DateUtils.MILLIS_IN_MINUTE)
								;
				
					break
					
				case "zNew".toLowerCase():
					
					dateFrom	= s != null
								? s.reportGenerationDate
								: DateUtils.addMilliseconds(
									// DateUtils.ceiling(myAccount.creationDate, Calendar.MINUTE),
									DateUtils.ceiling(user.activationDate, Calendar.MINUTE),
									-DateUtils.MILLIS_IN_MINUTE);

					// evaluate snapshot point in time (date and time)
					if (formParameters.dateTo == null)
					{
						formParameters.dateTo = new Date();
					}						

					if (formParameters.timeTo == null)
					{
						dateTo	= DateUtils.addMilliseconds(
									DateUtils.ceiling(new Date(), Calendar.MINUTE),
									-DateUtils.MILLIS_IN_MINUTE
									)
					}
					else
					{
						dateTo	= DateUtils.parseDate(formParameters.timeTo, "HH:mm");

						def int			iMin	= (int) DateUtils.getFragmentInMinutes(
															dateTo, 
															Calendar.YEAR
															);
															
						if (iMin - 1440 >= 1440)
						{
							return new ResultError(301, "Time may not be greater than '23:59' ("+(iMin - 1440)+").");
						}
					}
					
						info.addError( 5, formatter.format(formParameters.dateTo) + " formParameters.dateTo");
						info.addError( 5, formatter.format(dateTo) + " dateTo");
						
						dateTo	= DateUtils.addSeconds(
									DateUtils.addMinutes(
										formParameters.dateTo, 
										(int) DateUtils.getFragmentInMinutes(
											dateTo, 
											Calendar.MINUTE
											)
										),
									-1);
					
						info.addError( 5, formatter.format(dateTo) + " dateTo 2");
					if (dateFrom>dateTo)
					{
						return new ResultError(302, "The given snapshot time ("+formatter.format(dateTo)+") must be past the last report date ("+formatter.format(dateFrom)+").");
					}
					if (dateTo>new Date())
					{
						return info;
						return new ResultError(303, "The given snapshot time ("+formatter.format(dateTo)+") may not lie in the future (now: "+formatter.format(new Date())+").");
					}
					break;
					
					
				default:
			
								dateFrom	= s != null
											? s.reportGenerationDate
											: DateUtils.addMilliseconds(
												// DateUtils.ceiling(myAccount.creationDate, Calendar.MINUTE),
												DateUtils.ceiling(user.activationDate, Calendar.MINUTE),
												-DateUtils.MILLIS_IN_MINUTE);
								dateTo		= DateUtils.addMilliseconds(
												DateUtils.ceiling(new Date(), Calendar.MINUTE),
												-1-DateUtils.MILLIS_IN_MINUTE
												);
			}
			
			info.addError( 7, "activationDate: " + formatter.format(user.activationDate));				
			info.addError( 7, "creationDate: " + formatter.format(myAccount.creationDate));				
			info.addError( 7, "dateFrom: " + formatter.format(dateFrom));
			info.addError( 7, "dateTo: " + formatter.format(new Date()) + " " + formatter.format(dateTo));				
			
			
			def period = ModelHelper.datePeriod(
				conversionHandler.toDateTime(dateFrom),
				conversionHandler.toDateTime(dateTo));
			
			AccountHistoryStatusVO pageAHS = accountService.getAccountHistoryStatus(
												new AccountHistoryQuery([
													account: myAccountVO,
													period: period,
												])
											);
			
			if (repType == "zNew")
			{
				
				// UserRecord
				urZRep.create(
					myAccount.id,
					dateTo, 
					pageAHS
					)
				
				
				ResultMessageList	msg = new ResultMessageList();
				msg.addError(0, scriptParameters.messageOk);
				return msg;
				
			}
			
			
			if (myAccounts.size() > 1)
			{
				rows <<[]

				rows <<[
						date:			"",
						other:			repType + "-report",
						transNo:		"",
						description:	myAccount.type.name + " ("+formatter.format(dateFrom)+" - " + formatter.format(dateTo) + ")",
						amount:			"",
					]

				rows <<[]
			}

			Page<AccountHistoryEntryVO> pageTransactions = 
					accountService.searchAccountHistory(
						new AccountHistoryQuery([
							account:		myAccountVO,
							period:			period,
							orderBy:		org.cyclos.model.banking.transactions.TransactionOrderBy.DATE_ASC,
							currentPage:	isPAGE ? currentPage	: 0,
							pageSize:		isPAGE ? pageSize		: Integer.MAX_VALUE,
						])
					);
			
			if (isPAGE)
			{
                if (pageTransactions.getCurrentPage() > 0)
                {
				rows <<[
						date:			"",
						other:			"",
						transNo:		"",
						description:	"More records on the previous page ...!",
						amount:			"",
					];
                }
			}

            totalRecords = pageTransactions.getTotalCount();
            
            info.addError( 1, "totalRecords " + totalRecords);
            info.addError( 1, "getCurrentPage " + pageTransactions.getCurrentPage());
            info.addError( 1, "getPageCount " + pageTransactions.getPageCount());
			
			for (AccountHistoryEntryVO r : pageTransactions.pageItems)
			{
				rows <<[other:				r.relatedAccount.type.nature == AccountTypeNature.SYSTEM ?  'System' : r.relatedAccount.owner.shortDisplay,
						account:			formatter.format(r.relatedAccountName ?: "N/A"),
					//	type:				r.type.name,
						date:				formatter.format(r.date ?: "N/A"),
						amount:				formatter.format(r.amount ?: "N/A"),
						transNo:			r.transactionNumber,
						description:		formatter.format(r.description) // ?: r.type.name)
					];
			}
			
			if (!isCSV)
			{
                if (pageTransactions.getCurrentPage()+1 < pageTransactions.getPageCount())
                {
				rows <<[
						date:			"",
						other:			"",
						transNo:		"",
						description:	"More records on the next page ...!",
						amount:			"",
					];
                }

				rows <<[];
				
				rows <<[
						date:			"----------------------",
						other:			"----------------------",
						transNo:		"------------------",
						description:	"Report Overall Summary",
						amount:			"--------------",
					];

				rows <<[
						date:			formatter.format(dateFrom),
						other:			"Balance at begin",
						transNo:		"",
						description:	"",
						amount:			formatter.format(pageAHS.balanceAtBegin),
					];

				rows <<[
						date:			"",
						other:			"TOTAL in",
						transNo:		"",
						description:	"" + pageAHS.incoming.count + " Transaction(s)",
						amount:			formatter.format(pageAHS.incoming.sum),
					];

				rows <<[
						date:			"",
						other:			"TOTAL out",
						transNo:		"",
						description:	"" + (pageAHS.incoming.count + pageAHS.outgoing.count) + " Transaction(s)",
						amount:			formatter.format(pageAHS.outgoing.sum),
					];

				rows <<[
						date:			"",
						other:			"TOTAL",
						transNo:		"",
						description:	"" + pageAHS.outgoing.count + " Transaction(s)",
						amount:			formatter.format(pageAHS.netInflow),
					];

				rows <<[
						date:			formatter.format(dateTo),
						other:			"Balance at end",
						transNo:		"",
						description:	"",
						amount:			formatter.format(pageAHS.balanceAtEnd),
					];
			}
		}
		
    	// return info;
    	
		// Build the result
		return [
			columns: [
				[header: "Date", property: "date", width: "16%", align: "left"],
				[header: "Member (from/to)", property: "other", width: "15%"],
				// [header: "Account", property: "account", width: "20%"],
				// [header: "Type", property: "type", width: "2.5%", align: "right"],
				[header: "Trans #", property: "transNo", width: "10%", align: "left"],
				[header: "Description", property: "description", align: "left"	],
				[header: "Amount", property: "amount", width: "10%", align: "right"],
			],
			rows: rows,
			totalCount: totalRecords
		]
		
		break;
		
	case "History".toLowerCase():
	case "reportShowHistory".toLowerCase():
		

		// urZRep.getRows();
		// return info;
		
		// Build the result
		return [
			columns: urZRep.columns,
			rows: urZRep.rows,
			// rows: [[idInt: 1]],
			// totalCount: urZRep.totalCount,
		]
		break;
}

// urZRep.getRows();
return info;

return [
    columns: [[header: "Result", property: "text", width: "100%", align: "left"]],
	rows: [[text: "test"]]
];



// UserRecordType urtZReport = entityManagerHandler.find(UserRecordType, "z_reports")

// info.addError( 1, urtZReport.version + " " + urtZReport.name + " " + urtZReport.id);



// urZRep.create(new Date(), 1, 2, 3);



return new ResultError( 1,
	user.username + " " + (int) pageUserRecords.pageItems.size());
