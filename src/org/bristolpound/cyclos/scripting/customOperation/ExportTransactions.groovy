/**
 *
 * Script that returns the transfers according to the 
 * following custom form fields.
 *
 * Script parameters:
 * 		none
 *
 * Included libraries:
 *		- libErrorHandling
 *
 * Custom form fields:
 * 		- Date			dateTo			Optional	Date part of point in time for the snapshot.
 *													Null yields in current date.
 *		- String		timeTo			Optional	Time part of the snapshot in the format
 *													"HH:mm".
 *													Null yields in "23:59".
 *													Seconds are always "59".
 *		- List			groups			Required	List of group's internal names.
 * 		- List			transferFilters	Required	List of transfer filter ids prefixed with "id"
 *
 * Returns
 *		org.cyclos.model.system.operations.CustomOperationPageResult:
 * 		- int			#			Optional	Line number
 * 		- String		userSelf	Default		Username of the from-account owner.
 * 		- String 		Account		Default		Account name
 * 		- String 		Type		Default		Transaction type
 * 		- Date			date		Default		Date of the transaction
 *		- DateTime		datetime	Optional	Date and Time of the transaction
 * 		- BigDecimal	amount		Default		Amount of the transaction
 *		- String		userOther	Default		Username of the other party
 * 		- Int			transNo		Default		Transaction number
 *		- String		description	Default		Description/Transaction text
 *
 * Version:
 *		1.2.1, 2017-01-26
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 *		- CustomOperationPageContext not working (Cyclos)
 *
 **/

import java.text.SimpleDateFormat;

import org.apache.commons.lang.time.DateUtils;

import org.cyclos.entities.banking.Account;
import org.cyclos.entities.banking.Transfer;
import org.cyclos.entities.system.CustomFieldPossibleValue;
import org.cyclos.entities.users.Group;
import org.cyclos.entities.users.User;
import org.cyclos.model.banking.accounts.AccountHistoriesOverviewQuery;
import org.cyclos.model.banking.accounts.AccountHistoryOverviewEntryVO;
import org.cyclos.model.banking.accounts.AccountVO;
import org.cyclos.model.banking.accounttypes.AccountTypeNature;
import org.cyclos.model.banking.transferfilters.TransferFilterVO;
import org.cyclos.model.banking.transfertypes.TransferTypeVO;
import org.cyclos.model.users.groups.GroupSetVO;
import org.cyclos.model.users.groups.UserGroupVO;
import org.cyclos.model.utils.DatePeriodDTO;
import org.cyclos.model.utils.ModelHelper;
import org.cyclos.utils.Page;

info.addError( 1, "pageContext " + pageContext);
info.addError( 1, "isCSV " + isCSV);
info.addError( 1, "isPDF " + isPDF);
info.addError( 1, "isPAGE " + isPAGE);
info.addError( 1, "currentPage " + currentPage);
info.addError( 1, "pageSize " + pageSize);
info.addError( 1, "calculatedPage " + (isPAGE ? currentPage	: 0));
info.addError( 1, "calculatedpageSize " + (isPAGE ? pageSize		: Integer.MAX_VALUE));
// return info;


class ScriptBindingBP {

	private static 	Object			binding;
	private static	ResultInfoList	info;

	private static init(binding, info)
	{
		this.binding	= binding;
		this.info		= info;
	}

	public static Object getBinding()
	{
		return this.binding
	}

	public static org.cyclos.impl.utils.persistence.EntityManagerHandler getEntityManagerHandler()
	{
		return binding.entityManagerHandler;
	}

	public static org.cyclos.impl.utils.conversion.ConversionHandler getConversionHandler()
	{
		return binding.conversionHandler;
	}

	public static org.cyclos.impl.utils.formatting.FormatterImpl getFormatter()
	{
		return binding.formatter;
	}

	public static getFormParameters()
	{
		return binding.formParameters
	}

	public static getScriptParameters()
	{
		return binding.scriptParameters
	}

	public static org.cyclos.impl.system.ScriptHelper getScriptHelper()
	{
		return binding.scriptHelper;
	}

	public static org.cyclos.impl.access.SessionData getSessionData()
	{
		return binding.sessionData;
	}

	public static com.fasterxml.jackson.databind.ObjectMapper getObjectMapper()
	{
		return binding.objectMapper;
	}

	public static ResultInfoList getInfo()
	{
		return info;
	}
}

ScriptBindingBP.init(binding, info);


// evaluate selected fields
Map<String, CustomFieldPossibleValue> fields	= new HashMap<String, CustomFieldPossibleValue>();

for (CustomFieldPossibleValue cf : ScriptBindingBP.formParameters.fields)
{
	fields.put(cf.internalName, cf);
}

if (fields.size() == 0)
{
	//	return new ResultError(200, "At least one output field needs to be selected.");
}


// evaluate selected groups
Boolean				allGroups	= false;
List<UserGroupVO>	groups		= [];

for (CustomFieldPossibleValue cf : ScriptBindingBP.formParameters.groups)
{
    if (allGroups == false)
    {
        if (cf.internalName.trim() == "all")
        {
            allGroups = true;
			groups		= [];
			
			def queryBG = new org.cyclos.model.users.groups.BasicGroupQuery([
				groupSet: 		new org.cyclos.model.users.groups.GroupSetVO([internalName: "memberAccounts"]),
				natures:		[org.cyclos.model.users.groups.BasicGroupNature.MEMBER_GROUP],
				currentPage:	0,
				pageSize:		Integer.MAX_VALUE,
			])
			
			info.addError( 1, "pre r ");
			
			groupService.search(queryBG).getPageItems().each
			{	
				r ->
				info.addError( 1, "r " + r.internalName);
				
				groups		<<r;
			}
			
			//return info;
        }
        else
        {
			groups		<<new UserGroupVO([internalName: cf.internalName.trim()]);
        }
    }
}

//def transferFilters = []
//if(formParameters.transferFilters)
//{
//	formParameters.transferFilters.split(',').each
//	{
//		internalName ->
//		transferFilters <<new TransferFilterVO([internalName: internalName.trim()])
//	}
//}


// evaluate snapshot point in time (date and time)
if (ScriptBindingBP.formParameters.dateTo == null)
	ScriptBindingBP.formParameters.dateTo = new Date();

ScriptBindingBP.formParameters.dateTo	= DateUtils.addSeconds(
							DateUtils.addDays(
								ScriptBindingBP.formParameters.dateTo,
								1
								),
							-1);

def period = ModelHelper.datePeriod(
		ScriptBindingBP.conversionHandler.toDateTime(ScriptBindingBP.formParameters.dateFrom),
		ScriptBindingBP.conversionHandler.toDateTime(ScriptBindingBP.formParameters.dateTo)
		)



def params = new AccountHistoriesOverviewQuery([
	//    	groups: 		groups,
	//    	transferFilters: transferFilters,
	period: 		period,
	orderBy:		org.cyclos.model.banking.transactions.TransactionOrderBy.DATE_ASC,
	currentPage:	0,
	pageSize:		Integer.MAX_VALUE,
])



public class AccountBP {
	private long							id;
	private Account							account;
	private AccountVO						vo;
	private int								isSystem				= 0;
	private String							typeName;
	private User							owner;
	private long							ownerId;
	private String							ownerName;
	private String							ownerUsername;
	private Group							ownerGroup;

	private static	Map<Long, Object> 		accounts;

	AccountBP (long id)
	{
		this.id = id;
		this.vo = new AccountVO(id);
	}

	AccountBP (AccountVO vo)
	{
		this.vo = vo;
		this.id = vo.id;
	}

	public Account getAccount()
	{
		if (this.account == null)
		{
			this.account	= ScriptBindingBP.entityManagerHandler.find(Account, this.id);
		}

		return this.account;
	}

	public int getId()
	{
		return (int) this.id
	}

	public boolean getIsSystem()
	{
		if (this.isSystem == 0)
		{
			this.isSystem = (vo.type.nature == AccountTypeNature.SYSTEM) ? +1 : -1;

			if (this.isSystem == +1)
			{
				this.ownerId		= 0;
				this.ownerUsername	= vo.type.name;
			}
		}

		return (this.isSystem == +1);
	}

	public User getOwner()
	{
		if (this.owner == null && !this.getIsSystem())
		{

			this.owner			= this.getAccount().owner;
			this.ownerId		= this.owner.id;
			this.ownerName		= this.owner.name;
			this.ownerUsername	= this.owner.username;
			this.ownerGroup		= this.owner.group;
		}

		return this.owner;
	}

	public Group getOwnerGroup()
	{
		if (this.ownerGroup == null)
		{
			this.getOwner();
		}

		return this.ownerGroup;
	}

	public boolean isOwnerGroup(UserGroupVO group)
	{
		if (this.getIsSystem() || this.getOwnerGroup() == null)
		{
			return false;
		}

		if (group.id != null)
		{
			return (this.getOwnerGroup().id == group.id);
		}

		if (group.internalName != null)
		{
			return (this.getOwnerGroup().internalName == group.internalName);
		}

		return true;
	}

	public String getOwnerUsername()
	{
		if (this.ownerUsername == null)
		{
			this.getOwner();
		}

		return this.ownerUsername;
	}

	public String getTypeName()
	{
		if (this.typeName == null)
		{
			this.typeName	= this.getAccount().type.name;
		}

		return this.typeName;
	}

	//	public get(String property)
	//	{
	//		return this."$property"
	//	}
	
	public static init()
	{
		this.accounts				= new HashMap<Long, Object>();
	}
	public static AccountBP of(long id)
	{
		return AccountBP.of_helper(new Long(id));
	}

	public static AccountBP of(AccountVO vo)
	{
		return AccountBP.of_helper(new Long(vo.id), vo);
	}

	private static AccountBP of_helper(Long id, AccountVO vo)
	{

		AccountBP a = AccountBP.accounts.get(id);

		if (a == null)
		{
			a = new AccountBP(vo == null ? id : vo);
			accounts.put(id, a);
		}

		return a;

	}
	
}
AccountBP.init();

class AccountHistoryOverviewEntryBP
{

	private 			AccountHistoryOverviewEntryVO	ahoe;
	private				AccountVO						from;
	private				AccountVO						to;
	private				String							typeName;
	private				Transfer						transfer;
	private				String							description;


	private static		Map<Long, TransferTypeVO>		transferTypes;

	public static		SimpleDateFormat				dateFormatter		=  new SimpleDateFormat("dd/MM/yyyy");
	public static		SimpleDateFormat				datetimeFormatter	=  new SimpleDateFormat("yyyy-MM-dd HH:mm");


	AccountHistoryOverviewEntryBP(AccountHistoryOverviewEntryVO	ahoe)
	{
		this.ahoe		= ahoe;
		this.typeName	= ahoe.type.name;
		this.from		= ahoe.from;
		this.to			= ahoe.to;
	}


	public int getId()
	{
		return (int) this.ahoe.id
	}

	public String getTransferDateFormatted()
	{
		return this.dateFormatter.format(ScriptBindingBP.conversionHandler.toDate(this.ahoe.date));
	}

	public String getTransferDateTimeFormatted()
	{
		return this.datetimeFormatter.format(ScriptBindingBP.conversionHandler.toDate(this.ahoe.date));
	}

	public BigDecimal getAmount()
	{
		return this.ahoe.currencyAmount.amount;
	}

	public String getAmountFormatted()
	{
		return this.getAmount().setScale(2).toPlainString();
	}

	public String getDescription()
	{
		if (this.description == null)
		{
			Transfer transfer = this.getTransfer();

			this.description = transfer.transaction != null ? transfer.transaction.description : '';
		}

		return this.description;
	}

	public AccountBP getFrom()
	{
		return AccountBP.of(this.from);
	}

	public boolean getIsValidTransferType()
	{
		return AccountHistoryOverviewEntryBP.getSelectedTransferTypes().containsKey(this.ahoe.type.id)
	}

	public String getTransactionNumber()
	{
		return this.ahoe.transactionNumber;
	}

	public Transfer getTransfer()
	{
		if (this.transfer == null)
		{
			this.transfer = ScriptBindingBP.entityManagerHandler.find(Transfer, this.ahoe.id)
		}

		return this.transfer;
	}

	public AccountBP getTo()
	{
		return AccountBP.of(this.to);
	}

	public String getType()
	{
		//return (String) (int) this.ahoe.type.id;
		return this.typeName;
	}

	private static Map<Long, TransferTypeVO> getSelectedTransferTypes()
	{

		if (AccountHistoryOverviewEntryBP.transferTypes == null)
		{

			AccountHistoryOverviewEntryBP.transferTypes		= new HashMap<Long, TransferTypeVO>();
			Boolean allTransferTypes						= false;

			for (CustomFieldPossibleValue cf : ScriptBindingBP.formParameters.transferTypes)
			{
				if (allTransferTypes == false)
				{
					if (cf.internalName.trim() == "all")
					{
						allTransferTypes = true;
						AccountHistoryOverviewEntryBP.transferTypes = new HashMap<Long, TransferTypeVO>();
						
						def queryTT = new org.cyclos.model.banking.transfertypes.TransferTypeQuery([
							currentPage:	0,
							pageSize:		Integer.MAX_VALUE,
						]);
						
						ScriptBindingBP.info.addError( 1, "pre r ");
						
						ScriptBindingBP.binding.transferTypeService.search(queryTT).getPageItems().each
						{	
							r ->
							ScriptBindingBP.info.addError( 1, "r " + r.internalName);
							
							AccountHistoryOverviewEntryBP.transferTypes.put(r.id, r);
						}
					}
					else
					{
						AccountHistoryOverviewEntryBP.transferTypes.put(new Long(cf.internalName.substring(2)), new TransferTypeVO(new Long(cf.internalName.substring(2))));
						//				ScriptBindingBP.info.addError(Integer.parseInt(cf.internalName.substring(2)), cf.value);
					}
				}
			}
		}

		return transferTypes;
	}

	private static init()
	{
		transferTypes = null;
	}

}

AccountHistoryOverviewEntryBP.init();


abstract class ResultBean
{
	protected			AccountHistoryOverviewEntryBP	ahoe;
	protected			int 							direction			= 0;
	protected			UserGroupVO						selection;
	protected			int								index;

	private static 		int								i					= 0;


	ResultBean(AccountHistoryOverviewEntryBP ahoe, UserGroupVO selection)
	{
		this.ahoe		= ahoe;
		this.selection	= selection;
		this.index		= ++this.i;
	}

	public AccountBP getAccountSelf()
	{
		return this.direction == -1 ? this.ahoe.from : this.ahoe.to;
	}

	public AccountBP getAccountOther()
	{
		return this.direction == +1 ? this.ahoe.from : this.ahoe.to;
	}

	public String getAccountName()
	{
		return this.getAccountSelf().typeName;
	}

	public String getDescription()
	{
		return this.ahoe.description;
	}

	public int getIndex()
	{
		return this.index;
	}

	public String getFrom()
	{
		return this.getAccountSelf().ownerUsername;
	}

	public String getSignedAmountFormatted()
	{
		return ((BigDecimal) (this.ahoe.amount * this.direction)).setScale(2).toPlainString();
	}

	public String getTo()
	{
		return this.getAccountOther().ownerUsername;
	}

	public String getTransferDateFormatted()
	{
		return this.ahoe.transferDateFormatted;
	}

	public String getTransferDateTimeFormatted()
	{
		return this.ahoe.transferDateTimeFormatted;
	}

	public String getTransactionNumber()
	{
		return this.ahoe.transactionNumber;
	}

	public String getType()
	{
		return this.ahoe.type;
	}

	public String getSelection()
	{
		return (String) ((String) this.selection.id) + this.selection.internalName;
	}

	public String getMatch()
	{
		return ((String) this.getAccountSelf().id) + "-" + ((String) this.getAccountSelf().getOwnerGroup().id) + this.getAccountSelf().getOwnerGroup().internalName;
	}
	
	public static init()
	{
		this.i = 0;
	}
}

public class ResultBeanIn extends ResultBean
{
	ResultBeanIn(AccountHistoryOverviewEntryBP ahoe, UserGroupVO selection)
	{
		super(ahoe, selection);
		this.direction = +1
	}

}

class ResultBeanOut extends ResultBean
{

	ResultBeanOut(AccountHistoryOverviewEntryBP	ahoe, UserGroupVO selection)
	{
		super(ahoe, selection);
		this.direction = -1
	}

}




Page<AccountHistoryOverviewEntryVO>	result	= accountService.searchAccountHistoriesOverview(params);
List<ResultBean>					rows	= [];

ResultBean.init();

result.getPageItems().each
{
	r ->

	AccountHistoryOverviewEntryBP ahoe = new AccountHistoryOverviewEntryBP(r);

	if (ahoe.isValidTransferType)
	{
		for (UserGroupVO group : groups)
		{
			if (group.internalName == null) 1/0;

			if (ahoe.getFrom().isOwnerGroup(group))
			{
				rows <<new ResultBeanOut(ahoe, group);
			}

			if (ahoe.getTo().isOwnerGroup(group))
			{
				rows <<new ResultBeanIn(ahoe, group);
			}
		}
	}

}

//return info;

// evaluate columns to be displayed
def columns = [];

if (fields.containsKey("index"))
	columns <<[header: "#",									property: "index",					width: "2.5%",	align: "right"];

if (fields.containsKey("userSelf"))
	columns <<[header: fields.get("userSelf").value,		property: "from",					width: "14%"];

if (fields.containsKey("account"))
	columns <<[header: fields.get("account").value,			property: "accountName",			width: "8%"];

if (fields.containsKey("transactionType"))
	columns <<[header: fields.get("transactionType").value,	property: "type",					width: "15%"];

if (fields.containsKey("date"))
	columns <<[header: fields.get("date").value,			property: "transferDateFormatted",	width: "10%"];

if (fields.containsKey("datetime"))
	columns <<[header: fields.get("datetime").value,		property: "transferDateTimeFormatted",	width: "10%"];

if (fields.containsKey("amount"))
	columns <<[header: fields.get("amount").value,			property: "signedAmountFormatted",	width: "5%",	align: "right"];

if (fields.containsKey("userOther"))
	columns <<[header: fields.get("userOther").value,		property: "to",						width: "14%"];

if (fields.containsKey("transNo"))
	columns <<[header: fields.get("transNo").value,			property: "transactionNumber",		width: "10%"];

if (fields.containsKey("description"))
	columns <<[header: fields.get("description").value,		property: "description",			width: "10%"];


// cleanup
AccountBP.init();
AccountHistoryOverviewEntryBP.init();


// Build the result
return [
	columns:	columns,
	rows:		rows,
	//	totalCount:	rows.size(),
]
