import org.cyclos.entities.users.Record;
import org.cyclos.entities.users.SystemRecord;
import org.cyclos.entities.users.SystemRecordType;
import org.cyclos.model.Bean;
import org.cyclos.model.system.fields.DynamicFieldValueVO;
import org.cyclos.impl.system.ScriptHelper;

SystemRecordType srtLoanType = entityManagerHandler.find(SystemRecordType, 'loanType');

return binding.recordService.listSystem(
	srtLoanType
	).collect{SystemRecord it -> 
		//scriptHelper.wrap(it, srtLoanType.getAllFields())
		//new DynamicFieldValueVO(it.id as String,"Hello");
		new RecordForList(it)
	};



class RecordForList
extends Record
{
	@Delegate
	private Record r;
	
	public RecordForList(Record r)
	{
		this.@r = r;
		println "Created $this"
	}

	Record getValue()
	{
		return r;
	}
	
	String getLabel()
	{
		"Hello, Echo"
	}
	
	String getName()
	{
		"Hello, Name"
	}
	
	boolean isDefaultValue()
	{
		return false;
	}
	
	String toString()
	{
		"Hello World"
	}
}