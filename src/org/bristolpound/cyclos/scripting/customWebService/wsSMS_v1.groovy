//===========================================
//------  wsSMS_v1  -------------------------
//
// @version: 0.0.2

try
{
    return WsSms.processRequest(scriptBindingBP);
}
catch(ex)
{
	return wsCreateResponse(400, "Unexpectd Error: "+ex);
}

//------  wsSMS_v1  -------------------------
//===========================================
