@echo off
setlocal enabledelayedexpansion

set output=%~dp0wsDirectoryApi_v1.static.groovy
call %~dp0..\library\Build_lib_header.bat

echo wsDirectoryApi_v1.groovy
echo. 								>>%output%
cat %~dp0\wsDirectoryApi_v1.groovy	>>%output%

set output=
call ..\library\Build_libWsDirectory.bat
