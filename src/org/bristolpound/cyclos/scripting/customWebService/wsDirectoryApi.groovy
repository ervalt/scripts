/*
 *===========================================
 *------  wsDirectoryApi  -------------------
 *
 * @version: 0.0.1
 *
 **********************************************
 * Custom web service details
 * --------------------------
 *
 * @httpMethod:
 *  Both
 *
 * @runAs:
 *  Guest
 *
 * @scriptParameters:
 *
 * @urlMappings:
 *  directory
 *  directory/{ver}
 *  directory/{ver}/
**/


import org.apache.commons.lang3.StringUtils


// check version
if (!pathVariables.isSet("ver"))
{
	return LibWs.wsCreateResponse(404, "wsDirectoryApi: missing version in URL '"+path+"'");
}

String apiVersion = StringUtils.removeStart(pathVariables.getString("ver"), "v");

try
{
	Integer.parseInt(apiVersion);
	return LibWs.wsCreateResponse(404, "wsDirectoryApi: Unsupported API version '" + apiVersion + "'");
}
catch(ex)
{
	return LibWs.wsCreateResponse(404, "wsDirectoryApi: Unsupported version string '" + apiVersion + "'");
}

return LibWs.wsCreateResponse(400, "wsDirectoryApi: Unexpectd Error");

//------  wsDirectoryApi  -------------------
//===========================================
