/*
 *===========================================
 *------  LibWsDirectoryApi_v1  -------------
 *===========================================
 *
 * @version: 1.6.1
 *
**/

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap
import java.util.LinkedHashMap;

import com.github.zafarkhaja.semver.Version;
import com.github.zafarkhaja.semver.ParseException;

//import org.cyclos.impl.users.UserCustomFieldPossibleValueCategoryServiceLocal
import org.cyclos.model.system.fields.CustomFieldVO;
import org.cyclos.model.users.users.ViewProfileData;
import org.cyclos.model.Bean
import org.cyclos.model.system.fields.CustomFieldPossibleValueCategoryVO;
import org.cyclos.model.system.fields.CustomFieldPossibleValueQuery;
import org.cyclos.model.system.fields.CustomFieldPossibleValueVO;
import org.cyclos.entities.users.Group;
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserGroup;
import org.cyclos.model.users.groups.UserGroupVO;
import org.cyclos.model.users.users.UserWithFieldsVO;
import org.cyclos.entities.users.UserCustomFieldValue;
import org.cyclos.utils.Page
import org.cyclos.utils.ParameterStorage;
import org.cyclos.model.users.users.UserLocatorVO;
import org.cyclos.entities.users.UserCustomFieldPossibleValue;
import org.cyclos.entities.users.UserAddress;

class WsDirectoryRoot
{
	private ScriptBindingBP             context;
	private WsUserFields                fields;
	private WsGroupings                 groupings;
	private WsDirectory                 directory;
	private Date                        serverTimestamp;
	private ArrayList<Class<WsGroup>>   groupingClasses;

	protected Closure                   getUsersFunction;

	public  LinkedHashMap<String, String> jsonPathTranslations  =   new HashMap<String, String>();
	public  Map<String, String>         addressMapping;

	public Version                      apiVersion;
	public  ArrayList<WsField>          staticFields;
	public HashMap<String, Object>      profileFieldsConfig;

	public static final Version         version                 =   Version.valueOf('1.6.1');
	public static final String          resultObjectType        =   "org.bristolpound.api.cyclos.directory.json.data";

	WsDirectoryRoot(ScriptBindingBP context)
	{
		this(
			  context
			, null
			, null
			, null
		);
	}

	WsDirectoryRoot(ScriptBindingBP context, String apiVersion)
	{
			this(
				  context
				, apiVersion
				, null
				, null
			);

	}

	WsDirectoryRoot(ScriptBindingBP context, Version apiVersion)
	{
		this(
			  context
			, apiVersion
			, null
			, null
		);
	}

	WsDirectoryRoot(
			  ScriptBindingBP           context
			, String                    apiVersion
			, Closure                   getUsers
			, Map<String, Object>       options
			)
	{
		this(
			  context
			, ScriptBindingBP.parseVersion(apiVersion, Exception, "wsDirectoryApi_v1: Unsupported API string '" + apiVersion + "'")
			, getUsers
			, options
		);
	}

	WsDirectoryRoot(
		  ScriptBindingBP               context
		, Version                       apiVersion
		, Closure                       getUsers
		, Map<String, Object>           options
	)
	{
		this.context                =   context;
		this.apiVersion             =   apiVersion ?: WsDirectoryRoot.version;
		this.getUsersFunction       =   getUsers;
		this.addressMapping         =   options.get("addressMapping");
		if (this.addressMapping == null)
		{
			this.addressMapping = (Map<String, String>) [
				id: "id",
				name: "name",
				addressLine1: "addressLine1",
				addressLine2: "addressLine2",
				complement: "complement",
				buildingNumber: "buildingNumber",
				street: "street",
				poBox: "poBox",
				neighborhood: "neighborhood",
				zip: "zip",
				city: "city",
				region: "region",
				country: "country",
				location: "location",
				]
		}
		this.staticFields           =   options.get("staticFields");
		this.serverTimestamp        =   new Date();
		this.profileFieldsConfig    =   options.get("profileFields");
		this.groupingClasses        =   options.get("groupings");
	}

	public Object evalRequest()
	{
		String              requestVersion  =   this.context.scriptParameters["version.major"];
		ParameterStorage    pathVariables   =   this.context.pathVariables;

		// special case for develop endpoint
		if (requestVersion == "0")
		{
			requestVersion = this.apiVersion.getMajorVersion();
		}

		// check minor version
		if (pathVariables.isSet("minor"))
		{
			requestVersion += "."+ pathVariables.getString("minor");
		}

		try
		{
			if (!this.apiVersion.satisfies("^" + requestVersion))
			{
				return LibWs.wsCreateResponse(
					  501
					, "wsDirectoryApi_v1: Unsupported API version 'v" + requestVersion + "'. "
					+ "Maximum version supported is 'v" + this.apiVersion + "'. "
				);
			}
		}
		catch(ex)
		{
			return LibWs.wsCreateResponse(
				  400
				, "wsDirectoryApi_v1: Unsupported API string 'v" + requestVersion + "'"
				);
		}

		if (!pathVariables.isSet("sub"))
		{
			return this.toObject();
		}

		return this.toObject(pathVariables.sub);
	}

	public Object toObject()
	{
		return [
			class:      WsDirectoryRoot.resultObjectType,
			fields:     this.getFields(),
			groupings:  this.getGroupings(),
			directory:  this.getDirectory(),
			metadata:   this.getMetadata(),
			];
	}

	public Object toObject(String sub)
	{
		Object o;

		switch (sub)
		{
			case "fields":
				o = this.getFields();
				break;

			case "groupings":
				o = this.getGroupings();
				break;

			case "directory":
				o = this.getDirectory();
				break;

			default:
				return LibWs.wsCreateResponse(404, "LibWsDirectoryApi_v1: Unsupported component '" + sub + "'");
				break;
		}

		Object x = [
				class: WsDirectoryRoot.resultObjectType,
				metadata: this.getMetadata(),
				];

		x[sub] = o;

		return x;
	}

	public Object getMetadata()
	{
		String  incrementalKey      = Long.toHexString(
										this.context.applicationHandler.idMask.apply(
											this.serverTimestamp.getTime()
										)
									);

		return [
			apiVersion:             this.apiVersion.toString(),
			serverTimestamp:        serverTimestamp,
			datasetType:            'FULL',
			incrementalKey:         incrementalKey,
			jsonPathTranslations:   jsonPathTranslations,
			info: [
				libs: [
					WsDirectoryRoot:    WsDirectoryRoot.version.toString(),
					LibWs:              LibWs.version.toString(),
					ScriptBindingBP:    ScriptBindingBP.version.toString(),
				],
			],
		];
	}

	public WsUserFields getFields()
	{
		if (this.fields == null)
		{
			this.fields = new WsUserFields(this);
		}

		return this.fields;
	}

	public WsGroupings getGroupings()
	{
		if (this.groupings == null)
		{
			this.groupings  = new WsGroupings(this, this.groupingClasses);
		}

		return this.groupings;
	}

	public WsDirectory getDirectory()
	{
		if (this.directory == null)
		{
			this.directory  = new WsDirectory(this);
		}

		return this.directory;
	}

	protected getUsers()
	{
		if (this.getUsersFunction != null)
		{
			return this.getUsersFunction();
		}

		return null
	}

	protected ScriptBindingBP getContext()
	{
		return this.context;
	}
}

class WsUserFields
	extends ArrayList<WsField>
{
	private WsDirectoryRoot     parent;

	WsUserFields(WsDirectoryRoot root)
	{
		this.parent     =   root;

		for (WsField f : this.parent.staticFields)
		{
			this.add(f);
		}

		for (CustomFieldVO cf : this.getContext().binding.userCustomFieldService.list())
		{
			Object conf =   this.parent.profileFieldsConfig.get(cf.internalName)
						?:  this.parent.profileFieldsConfig.get(cf.id)
			if (conf == null
			|| !(conf instanceof WsFieldIgnore)
			)
			{
				this.add(newWsUserCustomField(cf, conf));
			}
		}
	}

	protected ScriptBindingBP getContext()
	{
		return this.parent.context;
	}

	protected WsDirectoryRoot getRoot()
	{
		return this.parent;
	}

	private WsUserCustomField newWsUserCustomField(CustomFieldVO cf, Object conf)
	{
		if (conf instanceof WsUserCustomField)
		{
			Class[] types = new Class[2];
			types[0] = WsUserFields;
			types[1] = CustomFieldVO;

			return this.context.newInstance(conf, types, this, cf);
		}

		WsFieldOptions options = conf;

		switch (cf.type)
		{

		case "MULTI_SELECTION":
			return new WsUserCustomFieldSelectMulti(this, cf, options);

		case "SINGLE_SELECTION":
			return new WsUserCustomFieldSelectSingle(this, cf, options);

		default:
			return new WsUserCustomField(this, cf, options);

		}
	}
}

class WsFieldPossibleValues
{
	private HashMap<CustomFieldPossibleValueCategoryVO, WsUserCustomFieldCategory>  categories;

	String  type;
	Object  values;

	Boolean listEntriesOnly             = true;

	WsFieldPossibleValues(String possibleValues)
	{
		this.type           = "path";
		this.values         = possibleValues;
	}

	WsFieldPossibleValues(List<Object> possibleValues)
	{
		this.type           = "list1";
		this.values         = possibleValues;
	}

	WsFieldPossibleValues(Map<String, Object> possibleValues)
	{
		this.type           = "properties";
		this.values         = possibleValues;
	}

	WsFieldPossibleValues(Object possibleValues)
	{
		this.type           = "properties";
		this.values         = possibleValues;
	}

	WsFieldPossibleValues(WsUserCustomFieldSelect ms)
	{
		this.type           = "list2";
		this.values         = new ArrayList<WsUserCustomFieldPossibleValue>();
		this.categories     = new HashMap<CustomFieldPossibleValueCategoryVO, WsUserCustomFieldCategory>();

		Page<CustomFieldPossibleValueVO> p = ms.parent.context.binding.userCustomFieldPossibleValueService.search(
			new CustomFieldPossibleValueQuery(
				field:          ms.cf,
				currentPage:    0,
				pageSize:       Integer.MAX_VALUE,
				)
			);

		WsUserCustomFieldCategory       cat;
		WsUserCustomFieldPossibleValue  pv;

		for (CustomFieldPossibleValueVO cfpv : p)
		{
			if (cfpv.category == null)
			{
				pv  = new WsUserCustomFieldPossibleValue(ms, cfpv);
			}
			else
			{
				cat = this.categories.get(cfpv.category);

				if (cat == null)
				{
					cat = new WsUserCustomFieldCategory(ms, cfpv.category, ms.cf);
					this.categories.put(cfpv.category, cat);
				}

				pv  = new WsUserCustomFieldPossibleValue(cat, cfpv);
				cat.addChild(pv);
			}

			this.values.add(pv);
		}
	}

	public WsUserCustomFieldCategory[] getCategories()
	{
		if (this.categories != null)
		{
			return this.categories.values().toArray(new WsUserCustomFieldCategory[this.categories.size()]);
		}

		return null;
	}
}

class WsUserCustomField
	extends WsField
{
	WsUserCustomField(
		  WsUserFields                          parent
		, CustomFieldVO                         cf
		, WsFieldOptions                        options
		)
	{
		super(parent, cf, options);
	}
}

class WsUserCustomFieldSelect
	extends WsUserCustomField
{
//  private List<WsUserCustomFieldCategory>                 categories;
	private     String                                      refC;
	private     String                                      refV;

	WsUserCustomFieldSelect(WsUserFields parent, CustomFieldVO cf, WsFieldOptions options)
	{
		super(parent, cf, options);
	}


	public String getRefCategories()
	{
		if (this.refC == null)
		{
			this.refC   = this.ref + "c";

			this.parent.root.jsonPathTranslations.put(this.refC, "\$.fields." + this.cf.internalName + ".possibleValues.categories");
		}

		return this.refC;
	}

	public String getRefValues()
	{
		if (this.refV == null)
		{
			this.refV   = this.ref + "v";

			this.parent.root.jsonPathTranslations.put(this.refV, "\$.fields." + this.cf.internalName + ".possibleValues.values");
		}

		return this.refV;
	}

	public WsFieldPossibleValues getPossibleValues()
	{
		return new WsFieldPossibleValues(this);
	//  return new WsFieldPossibleValues(this.getCategories());
	}

//  protected List<WsUserCustomFieldCategory> getCategories()
//  {
//      if (this.categories == null)
//      {
//          this.categories = new ArrayList<WsUserCustomFieldCategory>();
//
//          for (CustomFieldPossibleValueCategoryVO cfpvc
//              : this.context.binding.userCustomFieldPossibleValueCategoryService.list(this.cf))
//          {
//              this.categories.add(new WsUserCustomFieldCategory(this, cfpvc, cf));
//          }
//      }
//
//      return this.categories;
//  }
}

class WsUserCustomFieldSelectSingle
	extends WsUserCustomFieldSelect
{
	WsUserCustomFieldSelectSingle(WsUserFields parent, CustomFieldVO cf, WsFieldOptions options)
	{
		super(parent, cf, options);
	}
}


class WsUserCustomFieldSelectMulti
	extends WsUserCustomFieldSelect
{
	WsUserCustomFieldSelectMulti(WsUserFields parent, CustomFieldVO cf, WsFieldOptions options)
	{
		super(parent, cf, options);
	}
}

class WsUserCustomFieldCategory
	extends WsEntity
{
	private     ArrayList<WsUserCustomFieldPossibleValue>   children;
	private     CustomFieldVO                               cf;

	protected   CustomFieldPossibleValueCategoryVO          cat;
	protected   WsUserCustomFieldSelect                     parent;

	WsUserCustomFieldCategory(
		  WsUserCustomFieldSelect               parent
		, CustomFieldPossibleValueCategoryVO    cfpvc
		, CustomFieldVO                         cf
		)
	{
		super(
			  cfpvc.internalName.substring(cf.internalName.length() + 1)
			, cfpvc.name
		)
		this.parent     = parent;
		this.cat        = cfpvc;
		this.cf         = cf;
	}

	protected ArrayList<WsUserCustomFieldPossibleValue> getChildren2()
	{
		if (this.children == null)
		{
			this.children   = new ArrayList<WsGroupCategory>();

			Page<CustomFieldPossibleValueVO> p = parent.context.binding.userCustomFieldPossibleValueService.search(
				new CustomFieldPossibleValueQuery(
					field:          cf,
					category:       cfpvc,
					currentPage:    0,
					pageSize:       Integer.MAX_VALUE,
					)
				);

			for (CustomFieldPossibleValueVO cfpv : p)
			{
				this.children.add(new WsUserCustomFieldPossibleValue(this, cfpv));
			}
		}

		return this.children;
	}

	public ArrayList<Object> getChildren()
	{
		if (this.children != null)
		{
			ArrayList<Object>   children    = new ArrayList<Object>();

			for (WsUserCustomFieldPossibleValue pv : this.children)
			{
				children.add([$ref: this.parent.refValues + "." + pv.id]);
			}

			return children;
		}

		return null;
	}

	protected addChild(WsUserCustomFieldPossibleValue pv)
	{
		if (this.children == null)
		{
			this.children   = new ArrayList<WsGroupCategory>();
		}

		this.children.add(pv);
	}
}

class WsUserCustomFieldPossibleValue
{
	String id;
	String label;

	private WsUserCustomFieldCategory           category;
	private CustomFieldPossibleValueVO          value;
	private WsUserCustomFieldSelect             field;

	WsUserCustomFieldPossibleValue(WsUserCustomFieldSelect field, CustomFieldPossibleValueVO cfpv)
	{
		this.field      =   field;
		this.value      =   cfpv;
	}

	WsUserCustomFieldPossibleValue(WsUserCustomFieldCategory category, CustomFieldPossibleValueVO cfpv)
	{
		this(category.parent, cfpv);

		this.category   =   category;
	}

	public String getId()
	{
	//  return this.value.id;
		if (this.value.internalName)
		{
			return this.value.internalName.substring(this.field.id.length() + 1);
		}

		return null;
	}

	public String getLabel()
	{
		return this.value.value;
	}

	public Object getCategory()
	{
		if (this.category)
		{
			return [$ref: this.field.refCategories + "." + this.category.id];
		}
	}

}

class WsGroupings
	extends ArrayList<WsGroup>
{
	private WsDirectoryRoot     root;

	WsGroupings(WsDirectoryRoot root, ArrayList<Class> groupingClasses)
	{
		this.root           =   root;

		Class[] types = new Class[1];
		types[0] = WsGroupings;

		for (Class<WsGroup> groupingClass : groupingClasses)
		{
			this.add(this.context.newInstance(groupingClass, types, this));
		}
	}

	protected ScriptBindingBP getContext()
	{
		return this.root.context;
	}

	protected WsDirectoryRoot getRoot()
	{
		return this.root;
	}
}

class WsEntity
{
	String id;
	String label;
	String type;
	Object options;

	protected   Object      parent;

	WsEntity()
	{
	}

	WsEntity(String id)
	{
		this.id = id;
	}

	WsEntity(String id, String label)
	{
		this(id);

		this.label  = label;
	}

	WsEntity(String id, String label, String type)
	{
		this(id, label);

		this.type   = type;
	}

	WsEntity(String id, String label, String type, Object options)
	{
		this(id, label, type);

		this.options    = options;
	}

	WsEntity(String id, String label, Object options)
	{
		this(id, label);

		this.options    = options;
	}

	protected ScriptBindingBP getContext()
	{
		return this.parent.getContext();
	}

	protected WsDirectoryRoot getRoot()
	{
		return this.parent?.root;
	}
}

class WsFieldIgnore
{
	Boolean ignore = true;
}

class WsFieldOptions
{
	Integer min                     =   null;
	Integer max                     =   null;
	Integer decimalDigits           =   null;
	Boolean multiSelect             =   null;
	String  baseUrl                 =   null;
	String  description             =   null;
}

class WsField
	extends WsEntity
{
	private     String                                      ref;

	protected   CustomFieldVO                               cf;
	protected   WsFieldPossibleValues                       possibleValues;

				String                                      description;

	private     Closure                                     valueFromFunction;

	WsField(
		  WsUserFields                          parent
		, CustomFieldVO                         cf
		, WsFieldOptions                        options
	){
		super(
			  cf.internalName ?: cf.id
			, cf.name
			, (String) cf.type
			, options
		);
		this.parent         =   parent;
		this.cf             =   cf;
	}

	WsField(String id, String label, String type)
	{
		super(id, label, type);
	}

	WsField(String id, String label, String type, WsFieldOptions options)
	{
		super(id, label, type, options);
	}

	WsField(String id, String label, String type, WsFieldOptions options, WsFieldPossibleValues possibleValues)
	{
		super(id, label, type, options);

		this.possibleValues     = possibleValues;
	}

	WsField(String id, String label, String type, Closure valueFrom)
	{
		super(id, label, type);

		this.valueFromFunction = valueFrom;
	}

	WsField(String id, String label, String type, Closure valueFrom, WsFieldOptions options)
	{
		super(id, label, type, options);

		this.valueFromFunction = valueFrom;
	}

	WsField(String id, String label, String type, Closure valueFrom, WsFieldOptions options, WsFieldPossibleValues possibleValues)
	{
		this(id, label, type, options, possibleValues);

		this.valueFromFunction = valueFrom;
	}

	protected String getRef()
	{
		if (this.ref == null)
		{
			this.ref    = "\$f" + this.cf.id;

			this.parent.root.jsonPathTranslations.put(this.ref, "\$.fields." + this.cf.internalName);
		}

		return this.ref;
	}

	public CustomFieldVO toCustomFieldVO()
	{
		return this.cf;
	}

	public WsFieldPossibleValues getPossibleValues()
	{
		return this.possibleValues;
	}

	public valueFrom(WsDirectoryEntry entry)
	{
		if (this.valueFromFunction != null)
		{
			return this.valueFromFunction(entry);
		}
		UserCustomFieldValue    cv      = entry.getCustomValue(this.cf.id);

		if (cv == null)
		{
			return null;
		}

		switch (this.cf.type)
		{

		case "MULTI_SELECTION":
			def value = new ArrayList<Object>();

			for (UserCustomFieldPossibleValue cfpv : cv.enumeratedValues)
			{
				value.add([$ref: '\$f' + this.cf.id + 'v.' + cfpv.internalName]);
			}
			return value;

		case "INTEGER":
			return cv.integerValue;

		case "DECIMAL":
			return cv.decimalValue;

		case "DATE":
			return cv.dateValue;

		case "BOOLEAN":
			return cv.booleanValue;

		case "TEXT":
			return cv.getTextValue();

		case "RICH_TEXT":
			return cv.getRichTextValue();

		default:
			return cv.stringValue;

		}
	}
}

class WsGroup
	extends WsEntity
{
	String description;
	Object field;
	def possibleValues;

	WsGroup(WsGroupings parent)
	{
		this.parent         =   parent;
	}

	WsGroup(String label, String type, String description)
	{
		this.label          =   label;
		this.type           =   type;
		this.description    =   description;
	}
}

class WsGroupMultiSelect
	extends WsGroup
{
	Boolean multiSelect;

	WsGroupMultiSelect(WsGroupings parent)
	{
		super(parent);
	}
}

class WsGroupServiceValues
{
	String id;
	String label;
	String description;
	String icon;
}

class WsGroupCategoryGroups
extends WsGroupMultiSelect
{
	WsGroupCategoryGroups(WsGroupings parent, String id, String label, String fieldname)
	{
		super(parent);

		this.id                 = id;
		this.label              = label;
		this.field              = [$ref: "\$.fields." + fieldname];
		this.options            = [
			multiSelect         : true,
			values              : "\$.fields." + fieldname + ".possibleValues.categories",
			data                : "@.category",
			];
	//  this.possibleValues     = "fields[businesscategory].categories";
	//  this.possibleValues     = new ArrayList<WsGroupCategoryGroup>();

	//  CustomFieldVO cf = root.fields.getProperty("businesscategory").cf;

	//  for (CustomFieldPossibleValueCategoryVO cfpvc
	//      : root.context.binding.userCustomFieldPossibleValueCategoryService.list(cf))
	//  {
	//      this.possibleValues.add(new WsGroupCategoryGroup(this, cfpvc, cf));
	//  }
	}
}

class WsGroupCategories
extends WsGroupMultiSelect
{
	WsGroupCategories(WsGroupings parent, String id, String label, String fieldname)
	{
		super(parent);

		this.id                 = id;
		this.label              = label;
		this.field              = [$ref: "\$.fields." + fieldname];
	//  this.description        = "To be used to represent the location on a map.";
		this.options            = [
			multiSelect         : true,
			values              : "\$.fields." + fieldname + ".possibleValues.values",
			];
	//  this.possibleValues     = "fields[businesscategory].categories.values";
	//  this.possibleValues     = new ArrayList<WsGroupCategoryGroup>();

	//  CustomFieldVO cf = root.fields.getProperty("businesscategory").cf;

	//  for (CustomFieldPossibleValueCategoryVO cfpvc
	//      : root.context.binding.userCustomFieldPossibleValueCategoryService.list(cf))
	//  {
	//      this.possibleValues.add(new WsGroupCategoryGroup(this, cfpvc, cf));
	//  }
	}
}

class WsGroupCategoryGroup
	extends WsEntity
{
	int                         count;
	ArrayList<WsGroupCategory>  children;

	private CustomFieldPossibleValueCategoryVO  cat;

	WsGroupCategoryGroup(WsGroupCategoryGroups parent, CustomFieldPossibleValueCategoryVO cfpvc, CustomFieldVO cf)
	{
		this.parent     = parent;
		this.cat        = cfpvc;
		this.children   = new ArrayList<WsGroupCategory>();

		Page<CustomFieldPossibleValueVO> p = root.context.binding.userCustomFieldPossibleValueService.search(
			new CustomFieldPossibleValueQuery(
				field:          cf,
				category:       this.cat,
				currentPage:    0,
				pageSize:       Integer.MAX_VALUE,
				)
			);

		for (CustomFieldPossibleValueVO cfpv : p)
		{
			this.children.add(new WsGroupCategory(this, cfpv));
		}

		this.count = p.totalCount;
	}

	public String getId()
	{
		return this.cat.internalName;
	}

	public String getLabel()
	{
		return this.cat.name;
	}
}

class WsGroupCategory
{
	String id;
	String label;

	private WsGroupCategoryGroup        group;
	private CustomFieldPossibleValueVO  value;

	WsGroupCategory(WsGroupCategoryGroup group, CustomFieldPossibleValueVO cfpv)
	{
		this.group  = group;
		this.value  = cfpv;
	}

	public String getId()
	{
		return this.value.id;
		return this.value.internalName;
	}

	public String getLabel()
	{
		return this.value.value;
	}

}

class WsDirectory
	extends ArrayList<WsDirectoryEntry>
//class WsDirectory extends ArrayList<ViewProfileData>
{
	protected WsDirectoryRoot               root;

	WsDirectory(WsDirectoryRoot root)
	{
		this.root           =   root;

		for (User u : root.users)
		{
			if (context.binding.userServiceSecurity.doHasViewAccess(u)
				&& u.isActive()
				&& !u.isBlocked())
			{
				this.add(new WsDirectoryEntry(this, u));
			//  org.cyclos.model.users.users.UserLocatorVO ul = new UserLocatorVO(u.id);
			//  this.add(ul);
			//  this.add(context.binding.userServiceSecurity.getViewProfileData(ul));
			//  this.add(null);
			//  if (ul != null)
			//  {
		//          ViewProfileData vpd = context.binding.userServiceSecurity.getViewProfileData(ul);

		//          if (vpd != null)
		//          {
		//          //  this.add(new WsDirectoryEntry(vpd));
		//          }
		//      }
		//      return;
			}
		}
	}

	protected ScriptBindingBP getContext()
	{
		return this.root.context;
	}

	protected WsDirectoryRoot getRoot()
	{
		return this.root;
	}
}

class WsData
{
	Object  field;
	def     value;

	WsData(String fieldName, value)
	{
		this.field  =   [$ref: "\$.fields." + fieldName];
		this.value  =   value;
	}

	WsData(WsField wsField, WsDirectoryEntry entry)
	{
		this.field  =   [$ref: "\$.fields." + wsField.id];
		this.value  =   wsField.valueFrom(entry);
	}
}

class WsDirectoryEntry
{
//  String description;

	private WsDirectory                                 parent;
	private ViewProfileData                             vpd;
	private User                                        u;
	private java.util.Map<Long, UserCustomFieldValue>   customValues;
	private java.util.ArrayList<Object>                 addresses;
	private java.util.ArrayList<WsData>                 data;


	WsDirectoryEntry(
		  WsDirectory parent
		, User u
		)
	{
	//  this.description        =   vpd.display;
	//  this.vpd                =   vpd;
		this.parent             =   parent;
		this.u                  =   u;
		this.customValues       =   new HashMap<Long, UserCustomFieldValue>();

		for (UserCustomFieldValue cv : u.customValues)
		{
			this.customValues.put(cv.field.id, cv);
		}
	}

	public String getId()
	{
		return "" + this.parent.context.applicationHandler.idMask.apply(this.u.id);
	}

	public ArrayList<WsData> getData()
	{
		if (this.data == null)
		{
			this.data = new ArrayList<WsData>();

			for (WsField wsField : this.root.getFields())
			{
				WsData wsData = new WsData(wsField, this)
				if (wsData.value != null)
				{
					this.data.add(wsData);
				}
			}
		}

		return this.data;
	}

	protected UserCustomFieldValue getCustomValue(long id)
	{
		return this.customValues[id];
	}

	protected LinkedHashMap getImage()
	{

		return (u.image
			? [
				id:             this.parent.context.applicationHandler.idMask.apply(this.u.image.id),
				contentType:    u.image.getContentType(),
				width:          u.image.width,
				height:         u.image.height,
				length:         u.image.length,
				name:           u.image.key + "_" + u.image.width + "x" + u.image.height
								+ "." + u.image.getContentType().substring(6),
				]
			:null
			);
	}

	protected java.util.ArrayList<Object> getAddresses()
	{
		if (this.addresses == null)
		{
			LinkedHashMap<String, Object>           address;
			java.util.Map<String, String>           addressMapping = this.root.addressMapping;

			this.addresses = new java.util.ArrayList<Object>();

			for (UserAddress ua : u.getVisibleAddresses())
			{
				if (!ua.isHidden())
				{
					address = new LinkedHashMap<String, Object>();

					for (String key : addressMapping.keySet())
					{
						String property = addressMapping.get(key);
						Object value;

						switch (property)
						{
							case "id":
								value = this.parent.context.applicationHandler.idMask.apply(ua.id)
								break;

							default:
								value = ua[property];
								break;
						}

						address.put(key, value);
					}

					this.addresses.add(address);
				}
			}

			if (this.addresses.size() == 0)
			{
				address = new LinkedHashMap<String, Object>();

				for (String key : addressMapping.keySet())
				{
					Object value;

					switch (key)
					{
						case "id":
							value = this.parent.context.applicationHandler.idMask.apply(0)
							break;

						case "name":
							value = u.display;
							break;

						default:
							value = null;
							break;
					}

					address.put(key, value);
				}

				this.addresses.add(address);
			}
		}

		return this.addresses;
	}

	protected java.util.Date getActivationDate()
	{
		return u.getActivationDate();
	}


	protected ScriptBindingBP getContext()
	{
		return this.root.context;
	}

	protected WsDirectoryRoot getRoot()
	{
		return this.parent.root;
	}
}
