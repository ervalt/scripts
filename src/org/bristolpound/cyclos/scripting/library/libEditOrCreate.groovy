import java.util.LinkedHashMap

import org.cyclos.entities.*;
import org.cyclos.entities.access.*;
import org.cyclos.entities.banking.*;
import org.cyclos.entities.users.*;
import org.cyclos.entities.utils.*;
import org.cyclos.entities.system.*;
import org.cyclos.impl.*;
import org.cyclos.model.*;
import org.cyclos.model.banking.accounttypes.*;
import org.cyclos.model.banking.transferfees.*;
import org.cyclos.model.banking.transfertypes.*;
import org.cyclos.model.system.extensionpoints.*;
import org.cyclos.model.system.fields.*;
import org.cyclos.model.system.scripts.*;
import org.cyclos.model.users.products.*;
import org.cyclos.model.users.records.*;
import org.cyclos.model.users.recordtypes.*;
import org.cyclos.model.utils.*;
import org.cyclos.services.*
import org.cyclos.utils.*;

def copyProperties(Object o, LinkedHashMap props)
{
	props.each{
		key, value -> o[key] = value
	}

	entityManagerHandler.persist(o, true);

	return o;
}

def copyProperties(CustomField cf, LinkedHashMap props)
{
	// check for new entity
	if (cf.id == null)
	{
		assert props.type != null, "Custom Field requires to have a type"
		assert props.name != null, "Custom Field reqires to have a name"

		CustomFieldType type = props.type;

		// Load default settings
		if (type != CustomFieldType.LINKED_ENTITY)
		{
			cf.defaultDateToday	= null;
		}

		if (type != CustomFieldType.DATE)
		{
			cf.linkedEntityType	= null;
		}

		if (!type.isString())
		{
			cf.maxWordSize		= null;
		}

		if (!type.isUseSize())
		{
			cf.size				= null;
		}

		if (!type.isBinary())
			{
			cf.allowedMimeTypes	= null;
		}

		if (props.containsKey('recordType'))
		{
			RecordType rt = props.recordType;

			if (!rt.fields.contains(cf))
			{
				rt.fields << cf;
			}
		}
	}

	return copyProperties((Object ) cf, props);
}

def editOrCreateHelper(Object x, def names, LinkedHashMap props)
{
	boolean	isNew	= false;
	def		o;
	String	s		= 'properties:\n';
	String	name;

	Class clazz = x instanceof Class ? x : null;

	if (!(names instanceof Collection))
	{
		names = [names];
	}

	names.find
	{
		assert it instanceof String || it instanceof Long || it instanceof Integer

		try
		{
			switch (x)
			{
				case ScriptType:
					clazz = CustomScript;
					props['type'] = x;

					if (it instanceof String)
					{
						o = binding.customScriptService.listByType(x).find{CustomScript cs -> cs.name == it};

						if (o == null) throw new EntityNotFoundException();

						return o;
					}

				case ExtensionPointNature:
					switch (x)
					{
					case ExtensionPointNature.TRANSACTION:
						clazz = TransactionExtensionPoint;
						break;
					default:
						throw new IllegalArgumentException("TransferPointNature $x unknown");
					}

					if (it instanceof String)
					{
						o = binding.extensionPointService.list().find{ExtensionPointVO ep -> ep.nature == x && ep.name == it};

						if (o == null) throw new EntityNotFoundException();

						o = binding.extensionPointService.toEntity(binding.extensionPointService.load(o.id))

						return o;
					}

				default:
					assert clazz != null;

					o = entityManagerHandler.find(clazz, it);
			}
		}
		catch(EntityNotFoundException e2)
		{
			if (name == null) // only do it the first time, as this will be the new internalName
			{
				((String) it).split('\\.').each{
					v -> name = v;
				}

				if (name.isNumber())
				{
					name = "z" + name
				}
			}
		}

		return o
	}

	if (name != null)
	{
		if(o == null)
		{
			o = clazz.newInstance();
			isNew = true
		}

		if (o instanceof IInternalNamedEntity)
		{
			o.internalName	= name;
		}
		else if (o instanceof INamedEntity)
		{
			o.name	= name;
		}
	}

	//def bean = binding.scriptHelper.wrap(o)

	o = copyProperties(o, props);

	if (isNew)
	{
		//return save(o);
	}

	return o;
}

def editOrCreate(Class clazz, def names, LinkedHashMap props = [])
{
	return editOrCreateHelper(clazz, names, props);
}

AdminProductAuthorizationRole editOrCreate(
	AdminProduct product,
	AuthorizationRole role,
	LinkedHashMap props
)
{
	boolean								isNew				= false;
	Set<AdminProductAuthorizationRole>	authorizationRoles	= product.getAuthorizationRoles();

	AdminProductAuthorizationRole apar = authorizationRoles.find{
		it.getAuthorizationRole() == role
	}

	if (apar == null)
	{
		apar = editOrCreate(
		AdminProductAuthorizationRole			, 0
		, [
			product					: product,
			authorizationRole		: role,
		]);

		authorizationRoles << apar;

		isNew = true
	}

	apar = copyProperties(apar, props);

	if (isNew)
	{
		product.setAuthorizationRoles(authorizationRoles)
	}

	return apar;
}

ProductSystemRecordType editOrCreate(AdminProduct ap, SystemRecordType rt, LinkedHashMap props)
{
	boolean changeRecordCustomFields	= false;
	boolean allRecordCustomFields		= false;

	Set s = ap.getSystemRecordTypes();

	ProductSystemRecordType psrt;

	s.find{
		ProductSystemRecordType it -> it.recordType == rt
	}.collect
	{
		psrt = it
	}

	if (psrt == null)
	{
		psrt = new ProductSystemRecordType();
		psrt.product	= ap;
		psrt.recordType	= rt;

		s = s + [psrt];

		allRecordCustomFields = true;
	}

	// check for new fields that have not yet been saved to the database
	List<RecordCustomField> productRecordCustomFields = psrt.getProductRecordCustomFields();

	rt.getAllFields().each
	{
		CustomField customField ->

		if (allRecordCustomFields || !productRecordCustomFields.find{ProductRecordField prf -> prf.customField == customField})
		{
			ProductRecordField prf = new ProductRecordField();
			prf.setProductRecordType(psrt);
			prf.setCustomField(customField)
			productRecordCustomFields << prf
			changeRecordCustomFields = true
		}
	}

	if (changeRecordCustomFields)
	{
		psrt.setProductRecordCustomFields(productRecordCustomFields)
	}

	props.each{
		key, value ->

		switch (key)
		{
		case 'view':
		case 'edit':
		case 'create':
			psrt.getProductRecordCustomFields().each{
				ProductRecordField field ->

				field[key] = value;
			}

		default:
			psrt[key] = value
		}
	}
	ap.setSystemRecordTypes(s);

	entityManagerHandler.persist(psrt, true);
	entityManagerHandler.persist(ap, true);

	return psrt;
}

ExtensionPoint editOrCreate(ExtensionPointNature nature, def names, LinkedHashMap props)
{
	return editOrCreateHelper(nature, names, props);
}
Record editOrCreate(RecordType type, RecordCustomField field, Object value, LinkedHashMap props)
{
	RecordTypeVO					rt	= new  RecordTypeVO(id: type.id);
	CustomFieldValueForSearchDTO	x	= new org.cyclos.model.system.fields.CustomFieldValueForSearchDTO(
		field: new CustomFieldVO(id: field.id),
		"${getFieldValuePropertyName(field)}": value
	);

	//return x.properties;

	//x.field = new CustomFieldVO(id: rcfLoanTypeName.id);
	//x.stringValue = 'test';
	List l = binding.recordService.search(
		new SystemRecordQuery(
			type: rt,
			customValues: [
				x,
			],
		)
	).getPageItems();

	Record r = l.size ? entityManagerHandler.find(SystemRecord, l.get(0).id) : {
		// Should return the existing instance, of a single form type.
		// Otherwise it would be an error
		def record = binding.recordService.newEntity(
				new RecordDataParams(recordType: rt)
		);

		record.creationDate = new Date();

	   // if (!record.persistent) throw new IllegalStateException(
	   //	 "No instance of system record ${type.name} was found")

		setFieldValue(record.getCustomValues().find{
			it.field == field
		}, value);

		return record;
	}();

	props.each{
		Object key, Object v ->

		r.getCustomValues().find{
			RecordCustomFieldValue it ->

			if (key instanceof String)
			{
				//println "looking up field.internalName $key -> $it.field.internalName"
				return it.field.internalName == key;
			}

				//println "lookingup field id $key -> $it.field.id"
			return it.field.id == key;

		}.each{
			setFieldValue(it, v);
		}
	}

	entityManagerHandler.persist(r, true);

	return r;
}

CustomScript editOrCreate(ScriptType type, def names, LinkedHashMap props)
{
	return editOrCreateHelper(type, names, props);
}

SystemAccount editOrCreate(SystemAccountType systemAccountType, LinkedHashMap props = [])
{
	assert systemAccountType;

	SystemAccount sa	= systemAccountType.getAccount() ?: new SystemAccount();

	sa.with{
		type			= systemAccountType;
		creationDate	= new Date();
	}

	return copyProperties(sa, props);
}
def getFieldValuePropertyName(CustomField cf)
{
	switch (cf.type)
	{
	case CustomFieldType.BOOLEAN:
		return 'booleanValue';

	case CustomFieldType.DATE:
		return 'dateValue';

	case CustomFieldType.DECIMAL:
		return 'decimalValue';

	case CustomFieldType.INTEGER:
		return 'integerValue';

	case CustomFieldType.STRING:
		return 'stringValue';

	case CustomFieldType.TEXT:
		return 'textValue';
	}
}

def setFieldValue(CustomFieldValue cfv, Object value)
{
	switch (cfv.field.type)
	{
		case CustomFieldType.BOOLEAN:
			cfv.booleanValue = value;
			break;

		case CustomFieldType.DATE:
			cfv.dateValue = value;
			break;

		case CustomFieldType.DECIMAL:
			cfv.decimalValue = value;
			break;

		case CustomFieldType.INTEGER:
			cfv.integerValue = value;
			break;

		case CustomFieldType.STRING:
			cfv.stringValue = value;
			break;

		case CustomFieldType.TEXT:
			cfv.textValue = value;
			break;

		case CustomFieldType.SINGLE_SELECTION:
		case CustomFieldType.MULTI_SELECTION:
			if (value == null)
			{
				cfv.setEnumeratedValues([])
				return
			}

			if (!(value instanceof Collection))
			{
				value = [value];
			}

			cfv.setEnumeratedValues(value.collect{getCustomPossibleFieldValue(cfv.field, it)}.findAll{it}.toSet());
			break;

		default:
			println "unhandled type $cfv.field.type = $value ... "
	}
}

CustomFieldPossibleValue getCustomPossibleFieldValue(CustomField cf, CustomFieldPossibleValue value)
{
	return value
}

CustomFieldPossibleValue getCustomPossibleFieldValue(CustomField cf, String value)
{
	return (
		cf.getPossibleValues().find
		{
			possibleValue -> possibleValue.internalName == value
		} ?: cf.getPossibleValues().find
		{
			possibleValue -> possibleValue.value == value
		}
	);
}

CustomFieldPossibleValue getCustomPossibleFieldValue(CustomField cf, Long value)
{
	return (
		cf.getPossibleValues().find
		{
			possibleValue -> possibleValue.id == value
		}
	);
}


CustomFieldPossibleValue getCustomPossibleFieldValue(CustomField cf, Integer value)
{
	return getCustomPossibleFieldValue(cf, value as Long);
}

SimpleEntity save(SimpleEntity o)
{
	CRUDServiceLocal service;
	String serviceName = "${o.getClass().getSimpleName().uncapitalize()}Service";

	try
	{
		service = binding[serviceName]
	}
	catch(groovy.lang.MissingPropertyException e)
	{
		println "service $serviceName does not exist."
		return o
	}

	assert service != null;

	SimpleEntity e = service.toEntity(save(service.toDTO(o)));

	println "Saved $e";

	return e;
}

EntityDTO save(EntityDTO o)
{
	CRUDServiceLocal service;
	String serviceName = "${o.getClass().getSimpleName().uncapitalize().replace('DTO','')}Service";

	try
	{
		service = binding[serviceName]
	}
	catch(groovy.lang.MissingPropertyException e)
	{
		println "service $serviceName does not exist."
		return o
	}

	assert service != null;

	EntityDTO e = save(o, service);

	println "Saved $e";

	return e;
}

EntityDTO save(EntityDTO o, CRUDServiceLocal service)
{
	service.load(service.save(o));
}

