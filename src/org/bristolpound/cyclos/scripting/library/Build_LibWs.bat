@echo off
setlocal enabledelayedexpansion

if not "%output%"=="" goto continue

set output=%~dp0\LibWs.static.groovy
call %~dp0\Build_lib_header.bat

:continue

echo libWs.groovy
echo. 								>>%output%
cat %~dp0\libWs.groovy 				>>%output%

echo libScriptBinding.groovy
echo. 								>>%output%
cat %~dp0\libScriptBinding.groovy	>>%output%
