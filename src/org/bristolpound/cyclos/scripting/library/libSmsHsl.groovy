//===========================================
//------  libSmsHsl                    ------
//
// @version: 1.0.4

import java.util.LinkedHashMap;


class WsSmsProviderHsl
	extends WsSmsProvider
{
	private final String userAgent              = "HSL SMS Gateway";

	private String  host        = "sms.haysystems.com";
	private String  port        = "443";
	private String  client_id;
	private String  password;

	public static final String providerName = "HSL";

	WsSmsProviderHsl(WsSms wsSms)
	{
		super(wsSms);

		this.client_id  = scriptParameters["hsl.client_id"];
		this.password   = scriptParameters["hsl.password"];
		this.src_number = scriptParameters["hsl.src_number"];
	}

	public WsSmsProviderHslSender getSender()
	{
		return new WsSmsProviderHslSender(this, client_id, password);
	}

	public WsSmsProviderRequestHandler getRequestHandler()
	{
		WsSmsProviderHslRequestHandler  rh;

		switch (pathVariables.getString("p1").toLowerCase())
		{
			case 'hsl':
				if (!isRequestIdentified())
				{
					throw new WsSmsRequestInvalidException();
				}

				if (request.parameters.isSet("esm"))
				{
					rh = new WsSmsProviderHslRequestHandlerOutgoingStatus(this);
				}
				else
				{
					rh = new WsSmsProviderHslRequestHandlerIncoming(this);
				}
				break;

			default:
				return null;
		};

		return super.getRequestHandler(rh);
	}

	public String getHost()
	{
		return this.host;
	}

	public String getPort()
	{
		return this.port;
	}

	public Boolean isRequestIdentified()
	{
		return  (request.headers.isSet("user-agent")) &&
				(request.headers.getString("user-agent") == this.userAgent);
	}
}

abstract class WsSmsProviderHslRequestHandler
	extends WsSmsProviderRequestHandler
{
	WsSmsProviderHslRequestHandler(WsSmsProviderHsl p)
	{
		super(p);

		parameterNames  = newParameterNames("recipient", "mobileno", "message");
	}

	public Boolean isIdentified()
	{
		return provider.isRequestIdentified();
	}
}


class WsSmsProviderHslRequestHandlerIncoming
	extends WsSmsProviderHslRequestHandler
	implements IWsSmsInboundSms
{
	public WsSmsInboundSms sms;

	WsSmsProviderHslRequestHandlerIncoming(WsSmsProviderHsl p)
	{
		super(p);
	}
}

class WsSmsProviderHslRequestHandlerOutgoingStatus
	extends WsSmsProviderHslRequestHandler
	implements IWsSmsOutboundSmsStatus
{
	public WsSmsOutboundSmsStatus status;

	WsSmsProviderHslRequestHandlerOutgoingStatus(WsSmsProviderHsl p)
	{
		super(p);
	}

	public Boolean parse()
	{
		assertValid();

		WsSmsProviderHslStatusMessage message =
			new WsSmsProviderHslStatusMessage(request.parameters.getString(parameterNames.message));

		status = new WsSmsOutboundSmsStatus(
			provider,
			new WsSmsLogIdentPhoneNumber(
				message.id,
				request.parameters.getString(parameterNames.phoneNumber)
			),
			message.status,
			message
		)

		return true;
	}


	class WsSmsProviderHslStatusMessage
	{
		static final String patternString = "id:([0-9A-F]+) "           +
											"sub:(\\d{3}) "             +
											"dlvrd:(\\d{3}) "           +
											"submit date:(\\d{10}) "    +
											"done date:(\\d{10}) "      +
											"stat:(\\w+) "              +
											"err:(\\d{3}) "             +
											"text:(.*)"
											;
		static Pattern      pattern;

		public String   id;
		public int      sub;
		public int      delivered;
		public String   submitDate;
		public String   doneDate;
		public String   status;
		public int      err;
		public String   errText;
		public String   originalMessage;

		WsSmsProviderHslStatusMessage(String message)
		{
			this.originalMessage = message;

			if (pattern == null)
			{
				pattern = Pattern.compile(patternString);
			}

			Matcher matcher = pattern.matcher(message);

			while(matcher.find())
			{
				id              = matcher.group(1);
				sub             = Integer.parseInt(matcher.group(2));
				delivered       = Integer.parseInt(matcher.group(3));
				submitDate      = matcher.group(4);
				doneDate        = matcher.group(5);
				status          = matcher.group(6);
				err             = Integer.parseInt(matcher.group(7));
				errText         = matcher.group(8);
			}
		}

		public String toString()
		{
			return ""+[
				id:id,
				sub:sub,
				delivered:delivered,
				submitDate:submitDate,
				doneDate:doneDate,
				status:status,
				err:err,
				errText:errText,
				originalMessage:originalMessage
				];
		}
	}
}

class WsSmsProviderHslSender
	extends WsSmsProviderSender
{
	private WsSmsProviderHsl        provider;
	private HTTPBuilder             http;
	private String                  client_id;
	private String                  password;

	WsSmsProviderHslSender(WsSmsProviderHsl p, String client_id, String password)
	{
		super(p);

		//String    host        = p.host;
		//String    port        = p.port;

		this.provider           = p;
		this.client_id          = client_id;
		this.password           = password;
		http                    = new HTTPBuilder("https://${p.host}:${p.port}");
	}


	public OutboundSmsStatus send(String phoneNumber, String message)
	{
		StatusLine  statusLine;
					message         = prepareMessage(message);
		WsSmsLog    smsLog          = newSmsLog("out", provider.srcNumber, phoneNumber, message);

		try {
			// Send the message
			http.get( path : '/sendtxt/',
					 query : [
						client_id:  this.client_id,
						password:   this.password,
						destaddr:   phoneNumber,
						scraddr:    provider.srcNumber,
						text:        message,
						regdeliv:   1,
					 ]
					)
			{ resp, reader ->

								statusLine  = resp.getStatusLine();
				BufferedReader  buffer      = new BufferedReader(reader);
				String          body        = buffer.readLine();
								buffer.close();
				String[]        parts       = body.split(" ");

				smsLog.smsUuid = parts[1];

				log("response status: ${resp.statusLine}");
				log('Headers: -----------');
				resp.headers.each { h ->
					log(" ${h.name} : ${h.value}");
				}

				// Read the server's response
				log('Response data: -----');
				log(body);
				log('--------------------');
			}

			smsLog.smsError = this.log;

			if (statusLine.statusCode == 200)
			{
				return OutboundSmsStatus.SUCCESS
			}
			else
			{
				return OutboundSmsStatus.REJECTED;
			}
		} catch (Exception e)
		{
			return OutboundSmsStatus.UNKNOWN_ERROR;
		}

		return OutboundSmsStatus.UNKNOWN_ERROR;
	}
}

WsSms.addProvider(WsSmsProviderHsl);

//------  libSmsHsl                    ------
//===========================================
