//===========================================
//------  libScriptBinding ------------------
//
// @version: 1.5.0

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.zafarkhaja.semver.Version;
import com.github.zafarkhaja.semver.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.groovy.GroovyException;

import org.cyclos.entities.access.Channel;
import org.cyclos.entities.access.Password;
import org.cyclos.entities.access.PasswordType;
import org.cyclos.entities.system.ChannelConfiguration;
import org.cyclos.entities.system.Configuration;
import org.cyclos.entities.system.CustomField;
import org.cyclos.entities.system.CustomFieldPossibleValue;
import org.cyclos.entities.system.CustomFieldValue;
import org.cyclos.entities.system.SmsChannelConfiguration;
import org.cyclos.entities.users.BasicUser;
import org.cyclos.entities.users.MobilePhone;
import org.cyclos.entities.users.QMobilePhone;
import org.cyclos.entities.users.RecordType;
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserCustomField;
import org.cyclos.entities.users.UserCustomFieldValue;
import org.cyclos.entities.users.UserRecord;
import org.cyclos.model.EntityNotFoundException;
import org.cyclos.model.access.passwordtypes.PasswordCharacterPolicy;
import org.cyclos.model.system.channelconfigurations.SmsChannelConfigurationDTO;
import org.cyclos.model.system.channelconfigurations.SmsChannelConfigurationData;
import org.cyclos.model.system.fields.CustomFieldPossibleValueVO;
import org.cyclos.model.system.fields.CustomFieldVO;
import org.cyclos.model.system.fields.CustomFieldValueForSearchDTO
import org.cyclos.model.users.records.UserRecordQuery;
import org.cyclos.model.users.recordtypes.RecordTypeVO;
import org.cyclos.model.users.users.UserDetailedVO;
import org.cyclos.model.users.users.UserLocatorVO;
import org.cyclos.model.users.users.UserVO;

class ScriptBindingBP {

	public static final String          version                 = Version.valueOf('1.5.0');

	protected Object                            binding;

//  private static  ResultInfoList  info;
	private static HashMap<String, Long>        systemUserIds       = new HashMap<String, Long>();
	private static HashMap<String, Pattern>     compiledPatterns    = new HashMap<String, Pattern>();
	private String log      = "";

	public \
	log(String message)
	{
		this.log = "${this.log}${message}\n";
	}

	public String \
	getLog( message)
	{
		return this.log;
	}

//  private static init(binding, info)
	ScriptBindingBP(binding)
	{
		this.binding    = binding;
//      this.info       = info;
	}

	ScriptBindingBP(ScriptBindingBP scriptBindingBP)
	{
		this.binding    = scriptBindingBP.binding;
	}

	public Boolean \
	test()
	{
		return true;
	}

	protected ScriptBindingBP \
	getContext()
	{
		return this;
	}

	public Object \
	getBinding()
	{
		return this.binding;
	}

	public org.cyclos.impl.ApplicationHandler \
	getApplicationHandler()
	{
		return this.binding.applicationHandler;
	}

	public org.cyclos.impl.utils.persistence.EntityManagerHandler \
	getEntityManagerHandler()
	{
		return this.binding.entityManagerHandler;
	}

	public org.cyclos.impl.utils.conversion.ConversionHandler \
	getConversionHandler()
	{
		return this.binding.conversionHandler;
	}

	public org.cyclos.impl.access.PasswordHandler \
	getPasswordHandler()
	{
		return this.binding.passwordHandler;
	}

	public org.cyclos.impl.users.RecordFieldHandler \
	getRecordFieldHandler()
	{
		return this.binding.recordFieldHandler;
	}

	public org.cyclos.impl.utils.formatting.FormatterImpl \
	getFormatter()
	{
		return this.binding.formatter;
	}

	public \
	getFormParameters()
	{
		return this.binding.formParameters
	}

	public \
	getScriptParameters()
	{
		return this.binding.scriptParameters
	}

	public org.cyclos.impl.system.ScriptHelper \
	getScriptHelper()
	{
		return this.binding.scriptHelper;
	}

	public org.cyclos.impl.access.SessionData \
	getSessionData()
	{
		return this.binding.sessionData;
	}

	public com.fasterxml.jackson.databind.ObjectMapper \
	getObjectMapper()
	{
		return this.binding.objectMapper;
	}

	public org.cyclos.utils.ParameterStorage \
	getPathVariables()
	{
		return this.binding.pathVariables;
	}

	public org.cyclos.model.utils.RequestInfo \
	getRequest()
	{
		return this.binding.request;
	}

	public org.cyclos.impl.system.ConfigurationServiceLocal \
	getConfigurationService()
	{
		return this.binding.configurationService;
	}

	public org.cyclos.impl.users.RecordServiceLocal \
	getRecordService()
	{
		return this.binding.recordService;
	}

	public org.cyclos.impl.users.UserServiceLocal \
	getUserService()
	{
		return this.binding.userService;
	}

	public String \
	userMaskPassword(BasicUser u, String text)
	{
		for (Password p : u.passwords)
		{
			text = userMaskPassword(u, p, text);
		}

		return text;
	}

	public String \
	userMaskPassword(BasicUser u, Password p, String text)
	{
		Pattern pattern;
		String  patternString = "";

		for (Object x : [
			[name: "lower", pattern: "a-z", policy: p.type.lowerCaseLetters],
			[name: "upper", pattern: "A-Z", policy: p.type.upperCaseLetters],
			[name: "number", pattern: "0-9", policy: p.type.numbers],
			[name: "special", pattern: "`@!\"#\$%&'()*+,-./:;<=>?\\[\\\\\\]^_{}~", policy: p.type.specialCharacters],
		])
		{
			switch (x.policy)
			{
				case PasswordCharacterPolicy.valueOf("NOT_ALLOWED"):
					//ignore
					continue;

				case PasswordCharacterPolicy.valueOf("ALLOWED"):
					patternString = patternString + x.pattern;
					break;

				case PasswordCharacterPolicy.valueOf("REQUIRED"):
					patternString = patternString + x.pattern;
					break;
			}
		}

		if (patternString == "")
		{
			return text;
		}

		patternString = "[" + patternString + "]{${p.type.length.min},${p.type.length.max}}"

		pattern = compiledPatterns.get(patternString);

		if (pattern == null)
		{
			pattern = Pattern.compile(patternString);
			compiledPatterns.put(patternString, pattern);
		}

		Matcher matcher = pattern.matcher(text);

		while(matcher.find())
		{
			String match = text.substring(matcher.start(), matcher.end());

			if (passwordHandler.matches(p, match))
			{
				text    = StringUtils.replace(text, match, "[${p.type.name}:${p.status}]")

				matcher.reset();
			}
		}

		return text;
	}

	public Boolean \
	userWithMobilePhoneExists(String phoneNumber)
	{
		return this.userService.existsUserWithMobilePhone(StringUtils.prependIfMissing(phoneNumber, "+"))
	}

	public Long \
	getSystemUserId()
	{
		String system_user  = this.scriptParameters.system_user;

		if (StringUtils.isNumeric(system_user))
		{
			return applicationHandler.idMask.remove(Long.parseLong(system_user));
		}
		else if(systemUserIds.containsKey(system_user))
		{
			return systemUserIds.get(system_user);
		}
		else
		{
			UserDetailedVO      userDetailedVO      = this.getUser(system_user);

			systemUserIds.put(system_user, userDetailedVO.id);

			return userDetailedVO.id;
		}
	}

	public UserDetailedVO \
	getUserVO(String username)
	{
		UserLocatorVO       userLocatorVO       = new UserLocatorVO(username: username);

		return userService.locate(userLocatorVO)
	}

	public BasicUser \
	getUser(String username)
	{
		return getUser(this.getUserVO(username).id);
	}

	public BasicUser \
	getUser(Long userId)
	{
		return entityManagerHandler.find(BasicUser, userId)
	}

	public Long \
	findUserIdByPhoneNumber(String phoneNumber)
	{
		if (this.userWithMobilePhoneExists(phoneNumber))
		{
			return findUserByPhoneNumber(phoneNumber).id;
		}

		return getSystemUserId();
	}

	public BasicUser \
	findUserByPhoneNumber(String phoneNumber)
	{
		MobilePhone             mobilePhone         = findMobilePhone(phoneNumber);

		if (mobilePhone != null)
		{
			return mobilePhone.user;
		}

		try
		{
			return entityManagerHandler.find(BasicUser, getSystemUserId())
		}
		catch (EntityNotFoundException e)
		{
			return null
		}
	}

	public MobilePhone \
	findMobilePhone(String phoneNumber)
	{
		MobilePhone             mobilePhone         = null;

		// Resolve the normalized international phone number via the subscriber parameter
		if (phoneNumber == null)
		{
			threw new Exception("The subscriber parameter is missing")
		}

		if (!this.userWithMobilePhoneExists(phoneNumber))
		{
			return null;
		}

		// Find the mobile phone in Cyclos
		phoneNumber = StringUtils.prependIfMissing(phoneNumber, "+");

		QMobilePhone mp = QMobilePhone.mobilePhone;

		return  entityManagerHandler
				.from(mp)
				.where(
					  mp.normalizedNumber.eq(phoneNumber)
					, mp.user().status.eq(
						org.cyclos.model.users.users.UserStatus.ACTIVE)
				)
				.singleResult(mp);
	}

	public UserCustomFieldValue \
	getUserCustomFieldValue(BasicUser u, String fieldname)
	{
		for (UserCustomFieldValue cv : u.customValues)
		{
			if (cv.field.internalName == fieldname)
			{
				return cv;
			}
		}

		throw new EntityNotFoundException(UserCustomFieldValue,
			"'" + fieldname + "' not found for user '" + u.username + "'");
	}

	public String \
	getEnumeratedStringValue(CustomFieldValue v)
	{
		for (CustomFieldPossibleValue pf : v.getEnumeratedValues())
		{
			return pf.internalName;
		}
	}

	public  String \
	getEnumeratedStringValueOrDefault(
			BasicUser   u,
			String      fieldname,
			String      defaultValue
			)
	{
		String s = defaultValue;

		try
		{
			if (u != null)
			{
				UserCustomFieldValue v = getUserCustomFieldValue(u, fieldname);

				s = getEnumeratedStringValue(v)
			}
		}
		catch(EntityNotFoundException ex)
		{
			// fallback to system
		}

		if (s == defaultValue)
		{
			s = getCustomFieldDefault(fieldname);
		}

		return s;
	}

	public String \
	getCustomFieldDefault(String fieldname)
	{
		for (CustomFieldVO cf : this.binding.userCustomFieldService.list())
		{
			if (
				cf.internalName == fieldname
			)
			{
				return this.getCustomFieldDefault(
					entityManagerHandler.find(UserCustomField, cf.id)
					);
			}
		}

		throw new EntityNotFoundException(UserCustomField, fieldname);
	}

	public String \
	getCustomFieldDefault(CustomField cf)
	{
		switch (cf.type)
		{

		case "MULTI_SELECTION":
		case "SINGLE_SELECTION":

			for (CustomFieldPossibleValue pf : cf.getPossibleValues())
			{
				if (pf.isDefaultValue())
				{
					return pf.internalName;
				}
			}

			throw new EntityNotFoundException(CustomFieldPossibleValue,
				"Field '" + pf.internalName + "' has no default value");


		default:
			return cf.getDefaultValue()?.stringValue;

		}
	}

	public  UserRecord \
	getUserCustomRecord(
			String      recordTypeName,
			String      phoneNumber,
			java.util.ArrayList<Object>
						searchValues
								)
	{
		return getUserCustomRecord(
			recordTypeName,
			findUserIdByPhoneNumber(phoneNumber),
			searchValues
			);
	}

	public  UserRecord \
	getUserCustomRecord(
			String      recordTypeName,
			Long        userId,
			java.util.ArrayList<Object>
						searchValues
			)
	{
		return getUserCustomRecord(
			recordTypeName,
			new UserVO(userId),
			searchValues
			);
	}

	public  UserRecord \
	getUserCustomRecord(
			String      recordTypeName,
			UserVO      userVO,
			java.util.ArrayList<Object>
						searchValues
			)
	{
		RecordType      recordType  =   entityManagerHandler.find(RecordType, recordTypeName);
		UserRecordQuery urq         =   new UserRecordQuery();
		urq.type                    =   new RecordTypeVO(id: recordType.id);
		urq.user                    =   userVO;
		urq.pageSize                =   2000;
		Set setCustomValues         =   new HashSet();

		urq.setSharedRecordFieldSearch(false);

		String log="\nrecordTypeName=${recordTypeName}\nsearchValues=\n${searchValues}";

		CustomFieldValueForSearchDTO cf;

		for (Object o : searchValues)
		{
			CustomField f           = recordFieldHandler.findField(recordType,
																new CustomFieldVO(internalName: o.name)
															  );

			cf                      = new CustomFieldValueForSearchDTO();
			cf.field                = new CustomFieldVO(id: f.id);

			log=log + "\n${o.name}=${f.id}"

			switch (f.type)
			{

			case "MULTI_SELECTION":
			case "SINGLE_SELECTION":

				for (CustomFieldPossibleValue pf : f.getPossibleValues())
				{
					if (pf.internalName == o.value)
					{
						log=log + "\n${o.value}=${pf.id}"

						cf.enumeratedValues = [new CustomFieldPossibleValueVO(pf.id)]
						break;
					}
				}
				break;

			case "TEXT":
			case "STRING":
				cf.stringValue = o.value;
				log=log + "\n${cf.stringValue}"
				break;
			}

			setCustomValues.add(cf);
		}

		urq.customValues    = setCustomValues;

		// execute query
		def pageUserRecords = recordService.search(urq);

		log=log + "\npageUserRecords.pageItems.size=" + pageUserRecords.pageItems.size()

		if (pageUserRecords.pageItems.size() == 1)
		{
			return entityManagerHandler.find(UserRecord, pageUserRecords.pageItems[0].id);
		}

		if (pageUserRecords.pageItems.size() == 0)
		{
			return null;
		}

		throw new GroovyException("not unique record." + log)
	}


//  public static ResultInfoList getInfo()
//  {
//      return info;
//  }

	//@see http://stackoverflow.com/a/18251429/3102305

	public static <T> T \
	newInstance(
		  final Class<?>    clazz
		, final Class<?>[]  types
		, final Object...   args
	)
	throws
		ClassNotFoundException,
		NoSuchMethodException,
		InstantiationException,
		IllegalAccessException,
		IllegalArgumentException
	{
	  return (T) clazz.getConstructor(types).newInstance(args);
	}

	public static <T> T \
	newInstance(
		  final Class<?>    clazz
		, final Object...   args
	)
	throws
		ClassNotFoundException,
		NoSuchMethodException,
		InstantiationException,
		IllegalAccessException,
		IllegalArgumentException
		//InvocationTargetException
	{
		// Derive the parameter types from the parameters themselves.

		Class[] types = new Class[args.length];

		for ( int i = 0; i <types.length; i++ )
		{
			types[i] = args[i].getClass();
		}

		return (T) newInstance(clazz, types, args);
	}

	public static <T> T \
	newInstance(
		  final String      className
		, final Object...   args
	)
	throws
		ClassNotFoundException,
		NoSuchMethodException,
		InstantiationException,
		IllegalAccessException,
		IllegalArgumentException
	{
		return (T) newInstance(Class.forName(className), args);
	}

	public static Version parseVersion(final String versionString, final Class error, final String formatString)
	{
		try
		{
			return Version.valueOf(versionString);
		}
		catch(ParseException ex)
		{
			throw this.newInstance(error, formatString);
		}
	}

	public SmsChannelConfiguration \
	getSmsChannelConfig()
	{
		Configuration conf = sessionData.configuration.configuration;

		log(conf.name);

		while(true)
		{
			for (ChannelConfiguration channelCfg : conf.channelConfigurations)
			{
				log("" + [channel: channelCfg.channel.name, conf: channelCfg, isSms: channelCfg.channel.isSms(), isEnabled: channelCfg.isEnabled(), isDefined: channelCfg.isDefined()] );

				if (
					channelCfg.isEnabled()
				&&  channelCfg.isDefined()
				&&  channelCfg.channel.isSms()
				)
				{
					return channelCfg;
				}
			}

			if (conf.parent)
			{
				conf = conf.parent;
			}
			else break;
		}

		throw new EntityNotFoundException(SmsChannelConfiguration);
	}

	public Object \
	getSmsChannelConfigBp()
	{
		SmsChannelConfiguration     smsconf = getSmsChannelConfig();
		SmsChannelConfigurationDTO  smsDto  = binding.channelConfigurationService.load(smsconf.id);
		SmsChannelConfigurationData smsData = binding.channelConfigurationService.getData(smsconf.id);

		return [
			password:           smsconf.password,
			username:           smsconf.username,
			isPasswordSet:      smsconf.isPasswordSet(),
			mobileNumberSource: smsconf.mobileNumberSource,
			smsMessageSource:   smsconf.smsMessageSource,
			inboundSmsUrl:      smsData.inboundSmsUrl,
			id:                 smsconf.id,

		]
	}
}

ScriptBindingBP scriptBindingBP = new ScriptBindingBP(binding);

abstract interface bpSimpleObject
{
	abstract ScriptBindingBP                getContext();
}

//------  libScriptBinding ------------------
//===========================================
