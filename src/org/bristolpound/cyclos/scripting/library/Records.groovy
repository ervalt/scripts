/**
 *
 * Error handling library for scripts
 *
 * Version:
 *		0.1, 2016-08-22
 *
 * Script parameters:
 * 		none
 *
 * Included libraries:
 *		- none
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 *		none so far
 *
 **/


// import

//package org.bristolpound.cyclos.scripting.library.records

import org.cyclos.model.system.operations.PageResultColumn;


Boolean isCSV	= (pageContext == org.cyclos.model.system.operations.CustomOperationPageContext.CSV);
Boolean isPDF	= (pageContext == org.cyclos.model.system.operations.CustomOperationPageContext.PDF);
Boolean isPAGE	= (pageContext == org.cyclos.model.system.operations.CustomOperationPageContext.PAGE);



class ResultRow
{
	int			index			= -1;
}

class ResultList
{
	List<Object>			rows		= [];
	List<PageResultColumn>	columns		= [];
	// int						count		= 0;
	
	public addColumn(PageResultColumn header)
	{
		this.columns << header;
	}
	
	public addColumn(java.util.LinkedHashMap header)
	{
		this.columns << new PageResultColumn(header);
	}
	
	public addRow(ResultRow row)
	{
		row.index = this.rows.size();
		this.rows << row;
		// this.count += 1;
		return this.rows.size();
	}
	
	public getResult()
	{
		return [
			columns: this.columns,
			rows: this.rows,
			totalCount: this.getTotalCount(),
		]
	}
	
	public getTotalCount()
	{
		return this.rows.size();
	}
	
	public addError(int errNumber, String errDescription)
	{
		def e = new ResultError(errNumber, errDescription);
		this.addRow(e);
		return e;
	}

	public addError(int errNumber, errDescription)
	{
		return this.addError(errNumber, (String) errDescription);
	}
}
