//===========================================
//------  libSmsPlivo      ------------------
//
// @version: 1.0.9

import java.util.LinkedHashMap;

import com.plivo.helper.api.client.RestAPI as PlivoRestAPI
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;
import com.plivo.helper.util.*;


class WsSmsProviderPlivo
	extends WsSmsProvider
{
	private String  api_key;
	private String  api_token;
	public  String  url_method;

	public static final String providerName = "PLIVO";

	WsSmsProviderPlivo(WsSms wsSms)
	{
		super(wsSms);

		this.api_key    = scriptParameters["plivo.api_key"];
		this.api_token  = scriptParameters["plivo.api_token"];
		this.src_number = scriptParameters["plivo.src_number"];
		this.url_method = scriptParameters["plivo.statusMethod"]?:"POST";
	}

	public WsSmsProviderPlivoSender getSender()
	{
		return new WsSmsProviderPlivoSender(this, api_key, api_token);
	}

	public WsSmsProviderRequestHandler getRequestHandler()
	{
		WsSmsProviderPlivoRequestHandler  rh;

		switch (pathVariables.getString("p1").toLowerCase())
		{
			case 'in':
				rh = new WsSmsProviderPlivoRequestHandlerIncoming(this, api_token);
				break;

			case 'out':
				rh = new WsSmsProviderPlivoRequestHandlerOutgoingStatus(this, api_token);
				break;

			default:
				return null;
		};

		return super.getRequestHandler(rh);
	}

	public String getUrlMethod()
	{
		return this.url_method;
	}
}

abstract class WsSmsProviderPlivoRequestHandler
	extends WsSmsProviderRequestHandler
{
	private final String headerSignature    = "X-Plivo-Signature";

	private String                  api_token;

	WsSmsProviderPlivoRequestHandler(WsSmsProviderPlivo p, String api_token)
	{
		super(p);

		this.api_token  = api_token;
	}

	public Boolean isIdentified()
	{
		return request.headers.isSet(headerSignature);
	}

	public Boolean checkSignature()
	{
		if (!isIdentified())
		{
			return false;
		}

		String signature    = request.headers.getString(headerSignature);
		String url          = request.requestData.baseUrl+"/"+request.requestData.uri;

		LinkedHashMap<String, String> post_params = new LinkedHashMap<String, String>();

		for (String key: request.parameters.getNames())
		{
			String value = request.parameters.getString(key);
			post_params.put(key, value);
		}

		return XPlivoSignature.verify(url, post_params, signature, api_token);
	}

	protected assertValid()
	{
		if (isValid) return;

		super.assertValid();

		isValid = false;

		if (!checkSignature())
		{
			throw new WsSmsRequestInvalidException("Invalid Signature");
		}

		isValid = true;
	}
}


class WsSmsProviderPlivoRequestHandlerIncoming
	extends WsSmsProviderPlivoRequestHandler
	implements IWsSmsInboundSms
{
	public WsSmsInboundSms sms;

	WsSmsProviderPlivoRequestHandlerIncoming(WsSmsProviderPlivo p, String api_token)
	{
		super(p, api_token);

		parameterNames  = newParameterNames(
							"To",
							"From",
							"Text",
							[
								"type": "Type",
								"uuid": "MessageUUID",
							]);
	}
}


class WsSmsProviderPlivoRequestHandlerOutgoingStatus
	extends WsSmsProviderPlivoRequestHandler
	implements IWsSmsOutboundSmsStatus
{
	public WsSmsOutboundSmsStatus status;

	WsSmsProviderPlivoRequestHandlerOutgoingStatus(WsSmsProviderPlivo p, String api_token)
	{
		super(p, api_token);

		parameterNames  = newParameterNames(
							"To",
							"From",
							[
								"uuid": "ParentMessageUUID",
							]);
	}

	public Boolean parse()
	{
		assertValid();

		if (!pathVariables.isSet("p2"))
		{
			throw new GroovyException("parameter 'log' missing");
		}

		status = new WsSmsOutboundSmsStatus(
			provider,
			new WsSmsLogIdentIdMask(pathVariables.getString("p2")),
			request.parameters.getString("Status"),
			[
				MCC: request.parameters.getString("MCC"),
				MNC: request.parameters.getString("MNC"),
			]
		)

		return true;
	}
}

class WsSmsProviderPlivoSender
	extends WsSmsProviderSender
{
	private WsSmsProviderPlivo      provider;
	private PlivoRestAPI            api;

	WsSmsProviderPlivoSender(WsSmsProviderPlivo p, String api_key, String api_token)
	{
		super(p);

		this.provider           = p;
		api                     = new PlivoRestAPI(api_key, api_token, "v1");
	}


	public OutboundSmsStatus send(String phoneNumber, String message)
	{
		LinkedHashMap<String, String> params = new LinkedHashMap<String, String> ();

					message     = prepareMessage(message);
		WsSmsLog    smsLog      = newSmsLog("out", provider.srcNumber, phoneNumber, message);

		params.put("src",         provider.srcNumber);
		params.put("dst",         phoneNumber);
		params.put("text",        message);

		Long    id              = smsLog.id;
		String  idMask          = applicationHandler.idMask.apply(id);

		params.put("url",       sessionData.configuration.fullUrl
								+'/run/sms/v1/out/'
								+idMask);
		params.put('method',    provider.urlMethod);

		try {
			// Send the message
			MessageResponse msgResponse = api.sendMessage(params);

			smsLog.smsError = msgResponse;

			if (msgResponse.serverCode == 202)
			{
				smsLog.smsUuid = msgResponse.messageUuids.get(0).toString();

				return OutboundSmsStatus.SUCCESS
			}
			else
			{
				return OutboundSmsStatus.REJECTED;
			}
		} catch (PlivoException e)
		{
			return OutboundSmsStatus.UNKNOWN_ERROR;
		}

		return OutboundSmsStatus.UNKNOWN_ERROR;
	}
}

WsSms.addProvider(WsSmsProviderPlivo);

//------  libSmsPlivo      ------------------
//===========================================
