//===========================================
//------  libWs -----------------------------
//
// @version: 1.1.1


import java.util.LinkedHashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.zafarkhaja.semver.Version;
import com.github.zafarkhaja.semver.ParseException;
import org.apache.commons.lang3.StringUtils

import org.cyclos.entities.system.CustomWebService;
import org.cyclos.entities.users.Record;
import org.cyclos.entities.users.RecordCustomField
import org.cyclos.entities.users.RecordType;
import org.cyclos.entities.users.SystemRecord
import org.cyclos.entities.users.SystemRecordType
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserRecord
import org.cyclos.entities.users.UserRecordType
import org.cyclos.impl.system.ScriptHelper
import org.cyclos.impl.utils.persistence.EntityManagerHandler
import org.cyclos.model.system.fields.CustomFieldVO
import org.cyclos.model.system.fields.CustomFieldValueForSearchDTO
import org.cyclos.model.users.records.RecordDTO;
import org.cyclos.model.users.records.RecordDataParams
import org.cyclos.model.users.records.UserRecordDTO
import org.cyclos.model.users.records.UserRecordVO
import org.cyclos.model.users.recordtypes.RecordTypeNature;
import org.cyclos.model.users.recordtypes.RecordTypeVO
import org.cyclos.model.users.users.UserLocatorVO
import org.cyclos.model.utils.RequestInfo;
import org.cyclos.model.utils.ResponseException;
import org.cyclos.model.utils.ResponseInfo;
import org.cyclos.utils.ParameterStorage;

abstract class LibWs
{
	private static ObjectWriter         ow                      = new ObjectMapper().writer();

	public static final String          version                 = Version.valueOf('1.1.1');

	public static ResponseInfo wsCreateResponse(Exception ex)
	{
		return wsCreateResponse(500, ow.writeValueAsString(ex ?: "unknown error while evaluating request"), (String) null, (String) null)
	}

	public static ResponseInfo wsCreateResponse(int status)
	{
		return wsCreateResponse(status, (String) null, (String) null, (String) null)
	}

	public static ResponseInfo wsCreateResponse(int status, String message)
	{
		return wsCreateResponse(status, message, null, null)
	}

	public static ResponseInfo wsCreateResponse(int status, String message, String errorClass, String errorCode)
	{
		ResponseInfo r = new ResponseInfo(status, ow.writeValueAsString([
				class:      errorClass  ?: "org.cyclos.model.system.scripts.CustomScriptException",
				errorCode:  errorCode   ?: "CUSTOM_SCRIPT",
				message:    message     ?: (status <= 400 ? "" : "unknown error"),
			])
		)

		r.setHeader("Content-type", "application/json");

		return r;
	}
}


class WsRecord
	extends ScriptBindingBP
{
	private     RecordDTO               dto;
	protected   Map<String, Object>     wrapped;
	private     Record                  record;
	protected   RecordType              recordType;
	private     Boolean                 autosave    = false;

	WsRecord(ScriptBindingBP scriptBinding)
	{
		super(scriptBinding);
	}

	WsRecord(ScriptBindingBP scriptBinding, String recordTypeName)
	{
		this(scriptBinding, recordTypeName, new UserLocatorVO(id: null));
	}

	WsRecord(ScriptBindingBP scriptBinding, String recordTypeName, Long idRecord)
	{
		super(scriptBinding);

		setRecordType(recordTypeName);

		load(idRecord);
	}

	WsRecord(ScriptBindingBP scriptBinding, String recordTypeName, UserLocatorVO userLocatorVO)
	{
		super(scriptBinding);

		setRecordType(recordTypeName);

		RecordTypeVO            recordTypeVO;
		RecordDataParams        newParams;

		recordTypeVO            = new RecordTypeVO(id: recordType.id);

//        if (recordType.nature == RecordTypeNature.USER){}

		newParams               = new RecordDataParams([
			user: userLocatorVO,
			recordType: recordTypeVO,
		])

		this.dto                = recordService.getDataForNew(newParams).getDto();
		this.wrapped            = scriptHelper.wrap(dto);
	}

	protected setRecordType(String recordTypeName)
	{
		recordType              = entityManagerHandler.find(RecordType, recordTypeName)
	}

	protected setRecord(Record record)
	{
		if (record == null)
		{
			throw new WsRecordNotFoundException("record cannot be null")
		}
		this.dto                = null;
		this.record             = record;
		this.wrapped            = scriptHelper.wrap(this.record);
	}

	public load(Long idRecord)
	{
		this.setRecord(entityManagerHandler.find(Record, idRecord));
	}

	public Long save()
	{
		if (this.dto != null)
		{
			load(recordService.save(this.dto));
		}

		return this.id;
	}

	public Boolean getAutoSave()
	{
		return this.autosave;
	}

	public setAutoSave(Boolean v)
	{
		if (!this.autosave && v)
		{
			this.save();
		}
		this.autosave = v;
	}

	public Long getId()
	{
		return this.wrapped["id"];
	}

	public String getIdMask()
	{
		return ( this.wrapped["id"] == null
				? null
				: applicationHandler.idMask.apply(this.wrapped["id"])
				);
	}
}


class WsLog
	extends WsRecord
{
	WsLog(ScriptBindingBP scriptBinding)
	{
		super(scriptBinding, "wsLog");
	}

	public String getSmsHostIp()
	{
		return this.wrapped["smsHostIp"];
	}
	public setSmsHostIp(String v)
	{
		this.wrapped["smsHostIp"] = v;
	}

	public String getRequestMethod()
	{
		return this.wrapped["requestMethod"];
	}
	public setRequestMethod(String v)
	{
		this.wrapped["requestMethod"] = v;
	}

	public String getUri()
	{
		return this.wrapped["uri"];
	}
	public setUri(String v)
	{
		this.wrapped["uri"] = v;
	}

	public String getRequestData()
	{
		return this.wrapped["requestData"];
	}
	public setRequestData(String v)
	{
		this.wrapped["requestData"] = v;
	}

	public String getParameters()
	{
		return this.wrapped["parameters"];
	}
	public setParameters(String v)
	{
		this.wrapped["parameters"] = v;
	}

	public String getCookies()
	{
		return this.wrapped["cookies"];
	}
	public setCookies(String v)
	{
		this.wrapped["cookies"] = v;
	}

	public String getHeaders()
	{
		return this.wrapped["headers"];
	}
	public setHeaders(String v)
	{
		this.wrapped["headers"] = v;
	}

	public String getLog()
	{
		return this.wrapped["log"];
	}
	public setLog(String v)
	{
		this.wrapped["log"] = v;
	}
}

class WsRecordNotFoundException
	extends GroovyException
{
	public WsRecordNotFoundException()
	{
	}

	public WsRecordNotFoundException(String message)
	{
		super(message);
	}

	public WsRecordNotFoundException(Throwable cause)
	{
		super(cause);
	}

	public WsRecordNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}
}


//------  libWs -----------------------------
//===========================================
