//package org.bristolpound.cyclos.scripting.library

import org.springframework.util.ClassUtils;
import org.cyclos.*;
import org.cyclos.entities.*;
import org.cyclos.entities.access.*;
import org.cyclos.entities.banking.*;
import org.cyclos.entities.contentmanagement.*;
import org.cyclos.entities.messaging.*;
import org.cyclos.entities.system.*;
import org.cyclos.entities.users.*;
import org.cyclos.entities.utils.*;
import org.cyclos.impl.system.*;
import org.cyclos.model.access.*
import org.cyclos.model.contentmanagement.staticcontents.*;
import org.cyclos.model.contentmanagement.translations.*;
import org.cyclos.model.EntityNotFoundException;
import org.cyclos.model.banking.accounts.*;
import org.cyclos.model.banking.accounttypes.*;
import org.cyclos.model.banking.transferfees.*;
import org.cyclos.model.banking.transfertypes.*;
import org.cyclos.model.system.scripts.ScriptType;
import org.cyclos.model.system.fields.*;
import org.cyclos.model.system.networks.*;
import org.cyclos.model.users.groups.*;
import org.cyclos.model.users.products.*;
import org.cyclos.model.users.users.*;
import org.cyclos.model.utils.*;

//import com.github.zafarkhaja.semver.Version;

import BpPath.Separators
import BpTreeRootTrait.CollectionStorage

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.zafarkhaja.semver.ParseException;

import java.beans.*;
import java.util.Collection
import java.util.Date
import java.util.Map
import java.util.Set
import java.time.format.DateTimeFormatter

//import org.apache.commons.lang.StringUtils
import org.apache.commons.collections.list.SetUniqueList
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.collections.set.ListOrderedSet

import groovy.lang.Closure
import groovy.transform.*;

//java.lang.StackOverflowError

BpCyclos.create()
ScriptBindingBP scriptBindingBP = new ScriptBindingBP(binding);

//return 
println "" 
println "${new Date()}: STARTING CONFIG EXPORT ..." 
//println "" 
BpCyclosConfigExport x = new BpCyclosConfig(scriptBindingBP).Export()
//println "" 
println "${new Date()}: CONFIG EXPORT FINISHED!" 
//println "" 

BpTreeMapPlain z = x.toMap()

//println ""
println "${new Date()}: TRANSFORMATION FINISHED!"
//println ""


String ret;

ret = z.toJson()

//println ""
println "${new Date()}: JSON FINISHED!"
//println ""

return ret
System.exit(0)



class BpPath
implements \
	  Comparable \
	, CharSequence
{
	enum Separators {
		WINDOWS_SEPARATOR	('\\'	, '\\\\'	, 'Windows Path Separator'),
		UNIX_SEPARATOR		('/'	, '/'		,'*nix Path Separator'),
		JSON_SEPARATOR		('.'	, '$.'		, 'JSON Object Separator'),
		TEST_SEPARATOR		('|'	, null		, 'Test Separator'),
		WORD_SEPARATOR		(' '	, null		, 'Word Separator'),
		LIST_SEPARATOR		(','	, null		, 'List Separator'),
		
		final String separator;
		final String name;
		final String rootPrefix;
		
		private Separators(String separator, String rootPrefix, String name)
		{
			this.separator	= separator;
			this.rootPrefix	= rootPrefix;
			this.name		= name;
		}
		
		static getCardEnum(id) {
			Separators.grep{it.id == id}[0]
		}
		
		String getSeparator() {
			return separator
		}
	}
	
	public static defaultSeparator = Separators.JSON_SEPARATOR
	
	private Map<String,BpPath>		children			= new HashedMap<String,BpPath>();
	private Map<String,BpPath>		childrenSub			= new HashedMap<String,BpPath>();
	private ListOrderedSet			ancestors;
	private BpPath					parent;
	private String					name;
	private Separators				myDefaultSeparator;
	private String					mySeparator;
	
	BpPath()
	{
		this.@ancestors = new ListOrderedSet<BpPath>()
	}
	
	BpPath(String name)
	{
		this(null as BpPath, name);
	}
	
	BpPath(BpPath parent, String name)
	{
		this()
		setName(name)
		setParent(parent ?: new BpPath()); //root;
	}
	
	public BpPath appendChild(BpPath child)
	{
		assertNodeIsNotRelated(child)
		assert child != null
		
		BpPath parent = child.getParent()
		
		if (parent != this)
		{
			return child.setParent(this, true)
		}
		
		if (child.name == null)
		{
			appendChildrenSub(child)
			if (!childrenSub.containsValue(child))
			{
				// add temporary entry
				childrenSub.put("\000${System.identityHashCode(child)}", child)
			}
		}
		else
		{
			children.put(child.name, child)
		}
		
		return child
	}
	
	private appendChildrenSub(BpPath child)
	{
		boolean replaced
		
		// shifting all children's names in local list
		child.getChildren().values().each{
			BpPath grandChild ->
			
			child.name == grandChild.name || assertChildNotExists(grandChild.name)
			
		}.each{
			BpPath grandChild ->
			
			children.put(grandChild.name, grandChild)
			childrenSub.put(grandChild.name, child)
			
			replaced = replaced || child.name == grandChild.name
		}
		
		return replaced
	}

	public BpPath createChild(String name)
	{
		name == null || assertChildNotExists(name)
		
		return new BpPath(this, name);
	}
	
	private boolean assertNotNull(BpPath node)
	{
		assert node != null						, "null not allowed";
		
		return true
	}
	
	private boolean assertNodeIsNotRelated(BpPath node)
	{
	assert !hasRelative(node)					, "Node ${node.name} already related to this node";
		
		return true
	}
	
	private boolean assertChildNotExists(BpPath child)
	{
		assert !hasChild(child)					, "Node ${child.name} already a child of this node";
		
		return true
	}
	
	private boolean assertChildNotExists(String name)
	{
		assert !hasChild(name)					, "Child $name already exists in this node";
		
		return true
	}
	
	private boolean assertChildExists(BpPath child)
	{
		assert hasChild(child)					, "Child ${child} is not a child of this node";
		
		return true
	}
	
	private int assertChildExists(String name)
	{
		int i = hasChild(name)
		assert i								, "Child $name is not a child of this node";
		return i
	}
	
	private boolean assertChildValidName(String name)
	{
		assert name != null && name != ''		, "Child name invalid";
		
		return true
	}
	
	public boolean hasAncestor(BpPath node)
	{
		assertNotNull(node);
		
		return	this.@ancestors.contains(node) // includes parent and this
	}
	
	public boolean hasDescendant(BpPath node)
	{
		assertNotNull(node);
		
		return	node.hasAncestor(this)
	}

	public boolean hasChild(BpPath child)
	{
		assertNotNull(child);
		
		//		Search in sub entries				and in real entries
		return	childrenSub.containsValue(child) || children.containsValue(child)
	}
	
	public int hasChild(String name)
	{
		assertChildValidName(name)
		
		return	childrenSub.containsKey(name)	? 2
			:	children.containsKey(name)		? 1
			:	0
	}
	
	public boolean hasParent(BpPath node)
	{
		assertNotNull(node);
		
		//		Search in sub entries				and in real entries
		return	this.parent != node
	}

	public boolean hasRelative(BpPath node)
	{
		assertNotNull(node);
		
		//		Search in sub entries				and in real entries
		return	hasAncestor(node) || hasDescendant(node)
	}

	public BpPath getChild(String name)
	{
		assertChildExists(name)
		
		return children.get(name)
	}
	
	public BpPath removeChild(BpPath child)
	{
		return removeChild(child, false)
	}
	
	public BpPath removeChild(BpPath child, boolean failGracefully)
	{
		if (failGracefully && !hasChild(child))
		{
			return null
		}
		
		assertChildExists(child)
		
		String key
		
		children.entrySet().each{
			Map.Entry<String,BpPath> entry ->
				if(!key && entry.value == child)
				{
					key = entry.key;
				}
		}
		
		if (key)
		{
			children.remove(key)
			
			if (childrenSub.containsKey(key))
			{
				childrenSub.remove(key)
			}
			
			if (child.getParent() == this)
			{
				child.setParent(null)
			}
		}
	}
	
	public BpPath removeChild(String name)
	{
		switch (hasChild(name))
		{
		case 2:
			return childrenSub.getChild(name).removeChild(name)
			
		case 1:
			BpPath child = children.remove(name)
			
			child.setParent(null as BpPath)
			return child
			
		defaul:
			assertChildExists(name)
		}
	}
	
	public void removeAllChildren()
	{
		this.@children.each
		{
			String name, BpPath child ->
			
			child.setParent(null)
		}
	}
	
	public BpPath renameChild(String oldName, String newName)
	{
		int old = assertChildExists(oldName)
		assertChildNotExists(newName)
		
		BpPath child = getChild(oldName)
		
		if (old == 2)
		{
			child.renameChild(oldName, newName)
		}
		else
		{
			child.name = newName
		}
		
		return child
//		children.put(newName, child)
		
//		return removeChild(oldName)
	}

	protected String renameChild(BpPath child, String newName)
	{
		assert child != null					, "No child given to rename"
		
		if (name == '')
		{
			name = null
		}
		
		assert child.name != newName			, "New name is identical"
		
		assertChildExists(child)
		
		if (child.name == null)
		{
			
			(childrenSub.containsKey(newName) && childrenSub.get(newName) == child) || assertChildNotExists(newName)
			
			childrenSub.entrySet().each{
				Map.Entry<String,BpPath> entry ->
				if(entry.value == child)
				{
					childrenSub.remove(entry.key)
					children.remove(entry.key)
				}
			}
			
			children.put(newName, child)
		}
		else if (newName == null)
		{
			if (!appendChildrenSub(child))
			{
				children.remove(child.name)
			}
		}
		else
		{
			assertChildNotExists(newName)
			
			children.put(newName, child)
			children.remove(child.name)
		}
		
		child.setNameIgnoreParent(newName)
	}
	
	public Map<String,BpPath> getChildren()
	{
		return children
	}
	
	public String getJsonPath()
	{
		return getPath(BpPath$Separators.JSON_SEPARATOR)
	}
	
	private updateChildren()
	{
		children.values.each{ BpPath child -> child.setParent(this) }
	}
	
	public String getName()
	{
		return this.name
	}
	
	protected String setNameIgnoreParent(String newName)
	{
		String oldName = this.name
		this.name = newName
		updateChildren()
		return oldName
	}
	
	public String setName(String newName)
	{
		if (this.@name == newName) return newName
		
		if (parent)
		{
			return parent.renameChild(this, newName)
		}
		else
		{
			return setNameIgnoreParent(newName)
		}
	}
	
	public BpPath getParent()
	{
		return this.parent
	}
	
	public BpPath setParent(BpPath newParent)
	{
		return setParent(newParent, false)
	}
	
	public BpPath setParent(BpPath newParent, boolean failGracefully)
	{
		BpPath			tmpParent
		
		if (newParent != null && this.parent == newParent)
		{
			return newParent
		}
		
		if (failGracefully && this == newParent)
		{
			return this
		}
		
				// prevent infinite loop by checking $this is not an acestor of newParent
		if (newParent && assertNodeIsNotRelated(newParent))
		{
			//assert this.@ancestors.indexOf(newParent) == -1, "BpPath.setParent(): Cannot add descendant as ancestor (${this.@ancestors.indexOf(newParent)})"
		}
		
		// save previous parent
		tmpParent			= this.parent;
		
		// notify parent of removal, if exists
		this.parent?.removeChild(this, true)
		
		// set new parent
		this.parent			= newParent;
		
		// notify new parent
		newParent?.appendChild(this)
		
		this.@ancestors = new ListOrderedSet<BpPath>()
		
		if (this.parent)
		{
			this.@ancestors.addAll(this.parent.toArray())
			this.@ancestors.add(this.parent)
		}
		this.@ancestors.add(this)
		
		updateChildren()
		
		return tmpParent
	}
	
	public String getPath()
	{
		return getPath(mySeparator != null ? mySeparator : myDefaultSeparator ?: defaultSeparator)
	}
	
	public String getPath(Separators separator)
	{
		return getPath(separator.separator, separator.rootPrefix)
	}
	
	public String getPath(String separator)
	{
		return getPath(separator, null as String)
	}
	
	public String getPath(String separator, String rootPrefix)
	{
		return (rootPrefix ?: '') + this.toArray()*.name.findAll{it}.join(separator)
	}
	
	public String setSeparator(String separator)
	{
		String oldSeparator		= this.mySeparator
		this.mySeparator		= separator
	}
	
	public String setSeparator(Separators separator)
	{
		this.myDefaultSeparator	= separator
		return this.resetSeparator(null as String)
	}
	
	public String resetSeparator()
	{
		return this.setSeparator(null as Separators)
	}
	
	public BpPath[] toArray()
	{
		try {
			return ancestors?.toArray(BpPath[])
		}
		catch(StackOverflowError e) {
		    StackTraceElement[] stackTrace = e.getStackTrace();
		    // if the stack trace length is at  the limit , throw a new StackOverflowError, which will have one entry less in it.
		    if (stackTrace.length == 1024) {
		        throw new StackOverflowError();
		    }
		    throw e;  // if it is small enough, just rethrow it.
		}
		//(this.parent ? this.parent.toArray()  << name : []).findAll{it != null && it != ''}
	}
	
	@Override
	public int compareTo(Object other)
	{
		
		//<debug>println "${this.toString()}.compareTo(Object) -> ${other.toString()}"
		return compare(this, other);
	}
	
	public static int compare(BpPath p1, BpPath p2)
	{
		if (p1 == null)
		{
			return p2 == null ? 0 : 1
		}
		
		if (p2 == null)
		{
			return -1
		}
		
		if (p1.hashCode() == p2.hashCode()) return 0;
		
		BpPath[] l1	= p1?.toArray();
		BpPath[] l2	= p2?.toArray();
		
		int s1	= l1?.size() ?: 0;
		int s2	= l2?.size() ?: 0;
		
		int cmp = s1-s2
		int i	= 0;
		int j	= 0;
		
		while (cmp == 0 && i < s1 && j < s2)
		{
			p1 = l1[i++];
			p2 = l2[j++];
			
			if (p1 == null)
			{
				if (p2 == null)
				{
					continue
				}
				
				return 1
			}
			
			if (p2 == null)
			{
				return -1
			}
			
			if (p1.hashCode() == p2.hashCode()) continue;
			
			if (p1.@name == null && p2.@name == null)
			{
				continue
			}
			
			if (p1.@name == null && p2.@name != null)
			{
				return 1
			}
			else if (p2.@name == null)
			{
				return -1
			}
			
			cmp = p1.@name.compareTo(p2.@name)
		};
		
		return cmp
	}
	
	public String toString()
	{
		return this.getPath()
	}
	
	@Override
	public char charAt(int index) {
		return this.getPath().charAt(index);
	}
	
	@Override
	public int length() {
		return this.getPath().length();
	}
	
	@Override
	public CharSequence subSequence(int beginIndex, int endIndex)
	{
		return this.getPath().subSequence(beginIndex, endIndex);
	}
}


class BpClassInfo
{
	public Class				clazz
	public BeanInfo				info
	public PropertyDescriptor[]	prop
	
	private static LinkedHashMap<Class, BpClassInfo> clazzInfos = new LinkedHashMap<Class, BpClassInfo>()
	
	BpClassInfo(Class clazz)
	{
		this.clazz				= clazz;
		this.info				= Introspector.getBeanInfo(clazz);
		this.prop				= this.info.getPropertyDescriptors().findAll{
			!['class', 'metaClass'].contains(it.name)
		};
	}

	public static BpClassInfo getOrAddClazzInfo(Object o)
	{
		return getOrAddClazzInfo(o.getClass());
	}

	public static BpClassInfo getOrAddClazzInfo(Class clazz)
	{
		BpClassInfo clazzInfo = (BpClassInfo) this.clazzInfos.get(clazz)
		
		if (clazz && !clazzInfo)
		{
			clazzInfo = new BpClassInfo(clazz);

			this.clazzInfos.put(clazz, clazzInfo)
		}
		
		return clazzInfo;
	}
}


class BpEnum
	implements \
		BpEnumTrait
{
	BpEnum(
		  Enum			e
		)
	{
		BpEnumTrait.super.setEnum(e)
	}
	
	BpEnum(
			BpEnum			e
			)
	{
		BpEnumTrait.super.setEnum(e)
	}
}

trait BpEnumTrait
	implements \
		IBpContentClass
{
	private Enum				e;
	
	Enum getEnum()
	{
		return this.@e
	}
	
	Enum setEnum(BpEnumTrait newEnum)
	{
		return setEnum(newEnum.getEnum())
	}
	
	Enum setEnum(Enum newEnum)
	{
		Enum oldEnum = this.@e;
		this.@e = newEnum;
		return oldEnum;
	}
	
	Long getId()
	{
		return -this.ordinal();
		//TODO:return Long.MIN_VALUE + this.ordinal();
	}
	
	int ordinal()
	{
		return this.@e.ordinal();
	}
	
	String name()
	{
		return this.@e.name();
	}
	
	String toString()
	{
		return this.@e.toString();
	}

	@Override
	public Class getContentClass() {
		return this.@e?.getClass();
	}

	@Override
	public String getContentClassName() {
		return getContentClass()?.getSimpleName();
	}
}


class BpTree
	implements \
	Comparator \
{
	@Override
	public int compare(Object o1, Object o2) {
		return BpTree.compare(o1, o2);
	}
	
	public static int compare(BpTreeTrait o1, Object o2)
	{
		//<debug>println "BpTreeReference.compare(BpTreeReference, Object)"
		//<debug>println "${o1.toString()}.compare(Object) -> ${o2.toString()}"
		println "${o1.toString().padLeft(60)} <=> ${o2.toString()}"
		println "${o1.getPath().toString().padLeft(60)} <=> ${o2.getPath().toString()}"
		return o1.toString().compareTo(o2.toString());
	}
	
	public static int compare(BpTreeTrait o1, BpTreeTrait o2)
	{
		//<debug>println "BpTreeTrait.compare(BpTreeTrait, BpTreeTrait)"
		//<debug>println "${o1.toString()}.compare(BpTreeTrait) -> ${o1.getPath()} - ${o2.getPath()}"
		println "${o1.toString().padLeft(60)} <=> ${o2.toString()}"
		println "${o1.getPath().toString().padLeft(60)} <=> ${o2.getPath().toString()}"
		int cmp = BpPath.compare(o1.getPath(), o2.getPath())
		println "${((String) cmp).padLeft(64)}"
		
		return cmp;
		return BpPath.compare(o1.getPath(), o2.getPath());
	}
}	

interface IBpTreeLoadable
{
	public static enum LoadingStates 
	{
		CONTENT					(1i					, "Content"),
		UNRESOLVED_REFERENCES	(2i					, "Unresolved references"),
		ALL						(Integer.MAX_VALUE	, "All"), //must be last one!
		
		private static int		_all = 0;
		private final int		_mask;
		private final String	_name;
		
		private LoadingStates(int mask, String name)
		{
			if (mask == Integer.MAX_VALUE)
			{
				this._mask	= this._all;
			}
			else
			{
				this._all	|= mask
				this._mask	= mask
			}
			this._name		= name;
		}
		
		static LoadingStates first()
		{
			next(0)
		}
		
		static LoadingStates first(int mask)
		{
			all(mask)?.getAt(0);
		}
		
		static ArrayList<LoadingStates> all(int mask)
		{
			LoadingStates.findAll{
				LoadingStates it -> it.mask & mask
			}.sort{
				LoadingStates it -> it.mask
			}
		}
		
		static LoadingStates next(int x)
		{
			LoadingStates.findAll{
				LoadingStates it ->
				it.mask > x
			}.sort{it.mask}[0]
		}
		
		static LoadingStates next(LoadingStates x)
		{
			next(x.mask)
		}
		
		static LoadingStates valueOf(int mask) {
			LoadingStates.grep{LoadingStates it -> it.mask == mask}[0]
		}
		
		Object asType(Class clazz)
		{
			switch (clazz)
			{
			case int:
			case Integer:
				return this.getMask();
				
			case int:
			case Integer:
				return this.getMask();
				
			case String:
				return "IBpTreeLoadable.LoadingStates "+LoadingStates.collect{LoadingStates it -> it as String}.sort().join(', ');
				
			default:
				throw new org.codehaus.groovy.runtime.typehandling.GroovyCastException()
			}
		}
		
		String getName()
		{
			return this.@_name;
		}
		
		int getMask()
		{
			return this.@_mask;
		}
		
		boolean match(int x)
		{
			return this.mask & x
		}
		
		boolean match(LoadingStates x)
		{
			return this.mask & x.mask
		}
		
		int or(LoadingStates ls)
		{
			or(this.mask, ls.mask)
		}
		
		int or(int i)
		{
			this.mask | i
		}
		
		String toString()
		{
			return "${this.getMask()}#${this.getName()}";
		}
		
		LoadingStates next()
		{
			next(this.mask)
		}
	}
	
	abstract boolean	isReady();
	abstract int		load(LoadingStates loadingState);
	abstract int		load(int loadingStatesMask);
	abstract int		actionsRemaining();
	abstract Object		parent();
	abstract void		actionsRemaining(int actionsRemaining);
}

trait BpTreeLoadableTrait \
	implements \
		IBpTreeLoadable \
{
	private int						_actionsRemaining	= IBpTreeLoadable.LoadingStates.ALL.mask;
	
	public void actionsAdd(int actionsAdd)
	{
		this.actionsRemaining(actionsRemaining() | actionsAdd);
	}

	public int		actionsRemaining()
	{
		_actionsRemaining;
	}

	public void actionsRemaining(int actionsRemaining)
	{
		// propagate change
		if (actionsRemaining != this.@_actionsRemaining)
		{
			//println "${this.getPath()} parent: ${this.parent()?.getClass()?.getSimpleName()}"
			this.parent()?.actionsRemaining(actionsRemaining)
		}
		
		this.@_actionsRemaining = actionsRemaining;
	}

	public void actionsRemove(int actionsRemove)
	{
		// 												negate mask
		this.actionsRemaining(this.actionsRemaining() & (~actionsRemove));
	}

	public void actionsReset()
	{
		this.actionsRemaining(LoadingStates.ALL.mask)
	}

	public int evaluateResult(int loadingStatesMask, int result)
	{
		//<debug>print "evaluateResult($loadingStatesMask, $result): ${actionsRemaining()} -> "
		if (result == 0)
		{
			// all done
			this.actionsRemaining(0);
		}
		else
		{
			int wereHereAlready			= this.actionsRemaining();
			int removedOrNotRequested	= ~(loadingStatesMask & result);
			int requestedAndRemoved		= loadingStatesMask & removedOrNotRequested
			int remaining				= (wereHereAlready & (~requestedAndRemoved)) | result
			// remove all bits from loadingStatesMask
			this.actionsRemaining(remaining);
		}
		
		//<debug>println "${actionsRemaining()} for ${this.toString()}"
		return result
	}

	public boolean isReady()
	{
		this.actionsRemaining() == 0;
	}

	public int loadAll()
	{
		loadAll(IBpTreeLoadable.LoadingStates.ALL.mask)
	}
	
	public int loadAll(int loadingStatesMask)
	{
		println "loadAll($loadingStatesMask) for ${this.toString()}@${this.getPath()}"
		if (loadingStatesMask == 0)
		{
			return actionsRemaining();
		}
		
		int								i		= 0;
		int								cnt		= 0
		IBpTreeLoadable.LoadingStates	current	= IBpTreeLoadable.LoadingStates.first(loadingStatesMask);
		
		while(current && !this.isReady() && ++i)
		{
			println ""
			println "loading round $i with state $current"
			//<debug>println ""
			cnt = this.load(current);
			//<debug>println ""
			println "loading round $i -> ${IBpTreeLoadable.LoadingStates.all(cnt)} remaining"
			println ""
			current = IBpTreeLoadable.LoadingStates.first(cnt & loadingStatesMask)
		}
		
		return cnt;
	}
	
	public int load()
	{
		isReady() ? 0 : loadAll();
	}
	
	public int load(LoadingStates loadingState)
	{
		this.load(loadingState.mask)
	}
	
	public int load(int loadingStatesMask, Map map)
	{
		loadingStatesMask == 0 \
		? actionsRemaining()
		: load(loadingStatesMask, map.values());
	}
	
	public int load(int loadingStatesMask, Collection values)
	{
		if (loadingStatesMask == 0)
		{
			return actionsRemaining();
		}
		
		//<debug>print "${this.toString()}.load()?"
		if ((this.actionsRemaining() & loadingStatesMask) == 0)
		{
			//<debug>println ' -> no'
			return this.actionsRemaining()
		}
		
		//<debug>println " -> loading ${this.getPath()} ... "
		
		int remainingAll = 0
		
		values
		.findAll
		{
			// only process those entries that are loadable ...
			it instanceof IBpTreeLoadable
		}
		.findAll
		{
			// ... and are not yet ready
			IBpTreeLoadable loadable -> !loadable.isReady()
		}
		.collect
		{
			// return those with changes
			IBpTreeLoadable loadable -> remainingAll |= loadable.load(loadingStatesMask)
		}
		
		return evaluateResult(loadingStatesMask, remainingAll)
	}
}

trait BpTreeRootTrait
	extends BpTreeLoadableTrait
{
	static abstract class LoadableMap	//BpTreeRootTrait.LoadableMap
		implements \
		BpTreeTrait \
		, BpTreeLoadableTrait \
		, Map<Object,Object> \
	{
		LoadableMap(BpTreeTrait parent)
		{
			this(parent, parent.getClass().getSimpleName());
		}
		
		LoadableMap(BpTreeTrait parent, String slug)
		{
			initBpTreeTrait(parent, slug)
		}
		
		abstract protected Map getMap()
		
		@Override
		public int load(int loadingStatesMask)
		{
			this.load(loadingStatesMask, this.getMap());
		}
		
		protected getOrCreate(Object key, Class childClazz, String slug)
		{
			Object value = this.getMap().get(key);
			
			if (value == null)
			{
				//<debug>println "BpTreeRootTrait.LoadableMap.getOrCreate(): creating new list for $childClazz"
				
				value = childClazz.newInstance(this, slug);
				
				this.getMap().put(key, value)
				
				this.actionsReset()
			}
			assert value != null
			
			return value;
		}

		@Override
		public void clear() {
		}

		@Override
		public boolean containsKey(Object key)
		{
			this.getMap()?.containsKey(key);
		}

		@Override
		public boolean containsValue(Object value)
		{
			this.getMap()?.containsKey(value);
		}

		@Override
		public Set<Entry<Object, Object>> entrySet()
		{
			this.getMap()?.entrySet()?:[];
		}

		@Override
		public Object get(Object key) {
			this.getMap()?.get();
		}

		@Override
		public boolean isEmpty() {
			false;
		}

		@Override
		public Set<Object> keySet() {
			this.getMap()?.keySet();
		}

		@Override
		public Object put(Object key, Object value) {
			return null;
		}

		@Override
		public void putAll(Map<? extends Object, ? extends Object> m) {
		}

		@Override
		public Object remove(Object key) {
			return null;
		}

		@Override
		public int size()
		{
			this.getMap()?.size()?:0;
		}

		@Override
		public Collection<Object> values()
		{
			this.getMap()?.values();
		}
	}
	
	@InheritConstructors
	static class BpTreeReferenceStorage
		extends LoadableMap
	{
		protected Map<Class,ClassStorage>			_map	= new HashedMap<Class,ClassStorage>();
		
		public ClassStorage getOrCreate(Class clazz)
		{
			return super.getOrCreate(clazz, ClassStorage, clazz.getSimpleName());
		}
		
		protected Map getMap()
		{
			this.@_map;
		}
	}
	
	@InheritConstructors
	static class ClassStorage
		extends LoadableMap
	{
		protected Map<Long,CollectionStorage>		_map	= new HashedMap<Long, CollectionStorage>();
		
		public CollectionStorage getOrCreate(Long id)
		{
			return super.getOrCreate(id, CollectionStorage, id as String).setId(id);
		}
		
		protected Map getMap()
		{
			this.@_map;
		}
	}
	
	@InheritConstructors
	static class CollectionStorage
		extends LoadableMap
	{
		private Long								_id;
		private BpTreeReferenceCollection			_refCol;
		private BpTreeReference						_ref;
		
		public BpTreeReferenceCollection getCollection()
		{
			this.@_refCol;
		}
		
		@Override
		public int load(int loadingStatesMask)
		{
			this.load(loadingStatesMask, [this.getCollection()]);
		}
		
		public BpTreeReferenceCollection setCollection(BpTreeReferenceCollection col)
		{
			assert this.@_refCol == null
			
			this.@_refCol = col;
			
			return col;
		}
		
		public BpTreeReference setOwnRef(BpTreeReference ref)
		{
			assert this.@_ref == null
					
					this.@_ref = ref;
			
			return ref;
		}
		
		public CollectionStorage setId(Long id)
		{
			if (this.@_id != id)
			{
				assert this.@_id == null;
				
				this._id = id;
			}
			
			return this;
		}

		protected Map getMap()
		{
			this.@_ref;
		}
	}
	
	private BpTreeReferenceStorage _referenceStorage;
	
	void init()
	{
		assert this.options()	!= null;
		
		this._referenceStorage		= new BpTreeReferenceStorage(this, '~references');
		
		this.put('~references', this.@_referenceStorage)
		
	//	assert super.get('~references')
	//	assert (super.get('~references') instanceof Map)
	//	assert !(super.get('~references') instanceof String)
		
		addPath()
	}
	
	abstract ScriptBindingBP	binding()
	abstract BpTreeOptions		options()
	
	public BpTreeReference getOrCreateReference(
		  BpTreeTrait	parent
		, String		slug
		, Object		o
		)
	{
		if (o == null) return null
		
		Long						id 					= o.hasProperty('id')
														? o.id as Long
														: System.identityHashCode(o) as Long;
		BpTreeReference				refStub;
		Class						clazz				= o instanceof IBpContentClass
														? o.getContentClass()
														: o.getClass();
		
		//<debug>println "BpCyclosConfig.getReference(): looking for reference ${clazz}->$id"
		
		ClassStorage				classLoadedObjects	= this.@_referenceStorage.getOrCreate(clazz);
		CollectionStorage			colStore			= classLoadedObjects.getOrCreate(id);
		BpTreeReferenceCollection	refCol				= colStore.getCollection();
		
		if (refCol != null)
		{
			//<debug>println "BpCyclosConfig.getReference(): found reference for $o -A ${refCol.toString()}"
		}
		else
		{
			//<debug>println "BpCyclosConfig.getReference(): cretaing new reference for $o"
			refCol					= colStore.setCollection(
				new BpTreeReferenceCollection(this, colStore, parent.loadType(), o)
				);
			
			colStore.setOwnRef(refCol.add(colStore, slug, true))
			//<debug>println "BpCyclosConfig.getReference(): -> ${refCol.toString()}"
		}
		
		return refCol.add(parent, slug);
	}
	
	public BpTreeReference getReference(BpTreeTrait parent, String slug, Enum en)
	{
		return this.getOrCreateReference(parent, slug, new BpEnum(en));
	}
	
	public BpTreeTrait	getReference(BpTreeTrait parent, String slug, BpTreeTrait x)	{ return x;}
	public Object		getReference(BpTreeTrait parent, String slug, Object x)			{ return x;}
	
	public int load(int loadingStatesMask)
	{
		//<debug>println "load($loadingStatesMask) in root"
		if (loadingStatesMask == 0)
		{
			//<debug>println "loadingStatesMask == 0 returning ${actionsRemaining()} for ${this.toString()}@${this.getPath()}"
			return actionsRemaining();
		}
		
		if (this.isReady())
		{
			//<debug>println "loadingStatesMask == $loadingStatesMask returning 0 for ${this.toString()}@${this.getPath()}"
			return 0;
		}
		
		return evaluateResult(loadingStatesMask,
			super.load(loadingStatesMask) | this.@_referenceStorage.load(loadingStatesMask)
			);
	}
	
	public boolean			serialize(BpTreeTrait parent, String slug
		, Boolean content
		)
	{
		////<debug>println "BpCyclosConfig.serialize(): adding a primitive (${content?.getClass()?.name}) $content"
		return content;
	}
	
	public BpPath			serialize(BpTreeTrait parent, String slug
		, BpPath content
		)
	{
		////<debug>println "BpCyclosConfig.serialize(): adding a BpPath (${content?.getClass()?.name})"
		return content;
	}

	public BpTreeReference	serialize(BpTreeTrait parent, String slug
		, BpTreeReference content
		)
	{
		//<debug>println "BpCyclosConfig.serialize(): adding a BpTreeReference (${content?.getClass()?.name})"
		return content;
	}

	public BpTreeTrait		serialize(BpTreeTrait parent, String slug
		, BpTreeTrait content
		)
	{
		////<debug>println "BpCyclosConfig.serialize(): adding a BpTreeTrait (${content?.getClass()?.name})"
		return content;
	}

	public String			serialize(BpTreeTrait parent, String slug
			, Class content
			)
	{
		return content as String;
	}

	public Object			serialize(BpTreeTrait parent, String slug
		, Closure content
		)
	{
		return content
	}

	public BpTreeSet		serialize(BpTreeTrait parent, String slug
		, Collection content
		)
	{
		if (content == null) return null;
		
		//<debug>println "BpCyclosConfig.serialize(): adding a Set (${content?.getClass()?.name})"
		return new BpTreeSet(parent, slug, content)
	}

	public String			serialize(BpTreeTrait parent, String slug
			, Date content
			)
	{
		return ((Date) content).toInstant().toString();
	}

	public BpTreeRootTrait$BpTreeReferenceStorage			serialize(BpTreeTrait parent, String slug
			, BpTreeRootTrait$BpTreeReferenceStorage content
			)
	{
		return content;
	}
	
	public BpTreeReference	serialize(BpTreeTrait parent, String slug
		, Enum content
		)
	{
		//<debug>println "BpCyclosConfig.serialize(): Enum (${content?.getClass()?.name}) $content"
		return this.getReference(parent, slug, content);
	}

	public Number			serialize(BpTreeTrait parent, String slug
		, Number content
		)
	{
		////<debug>println "BpCyclosConfig.serialize(): adding a primitive (${content?.getClass()?.name}) $content"
		return content;
	}
	
	public BpTreeMap		serialize(BpTreeTrait parent, String slug
		, Map content
		)
	{
		if (content == null) return null;
		
		//<debug>println "BpCyclosConfig.serialize(): adding a Map (${content?.getClass()?.name})"
		return new BpTreeMap(parent, slug, content);
	}
	
	public abstract Object	serialize(BpTreeTrait parent, String slug
		, Object content
		)
	
	public String			serialize(BpTreeTrait parent, String slug
		, String content
		)
	{
		////<debug>println "BpCyclosConfig.serialize(): adding a primitive (${content?.getClass()?.name}) $content"
		return content;
	}

	public String			serialize(BpTreeTrait parent, String slug
			, TimeZone content
			)
	{
		return content.getID(); //GMT_ID;
	}
}


trait BpTreeTrait
	implements \
	Comparable \
{
	private BpTreeRootTrait			_root;
	private BpPath					_path		= new BpPath();
	private BpTreeTrait				_parent;
	private int						_loadType	= 0;
	
	void initBpTreeTrait()
	{
		////<debug>println "BpTreeTrait<init>(): ${this.toString()}, =${} in BpTreeTrait"
		
		assert this instanceof BpTreeRootTrait
		
		this._root		= (BpTreeRootTrait) this;
	}
	
	void initBpTreeTrait(BpTreeTrait p, String s)
	{
		assert !(this instanceof BpTreeReferenceCollection)
		
		initBpTreeTrait(p)
		
		//<debug>println "${this.toString()} init, slug: $s, parent: ${p.toString()}(${p.getPath()})"
		this._path	= p.getPath().createChild(s);
	}
	
	void initBpTreeTrait(BpTreeTrait p)
	{
		assert !(this instanceof BpTreeRootTrait)
		
		this._root		= p.root();
		this._parent	= p;
		this._loadType	= p.loadType();
	}
	
	
	void initBpTreeTrait(BpTreeRootTrait root, BpTreeRootTrait.CollectionStorage p, int loadType)
	{
		assert (this instanceof BpTreeReferenceCollection)
		
		this._root		= root;
		this._parent	= p;
		this._loadType	= loadType;
	}
	
	public BpTreeOptions options()
	{
		return this.root().options();
	}
	
	public BpTreeRootTrait root()
	{
		return this.@_root;
	}
	
	public BpTreeTrait parent()
	{
		return this.@_parent;
	}
	
	@Override
	public int compareTo(Object other)
	{
		//<debug>println "${this.toString()}.compareTo(Object) -> ${other.toString()}"
		return compare(this, other);
	}
	
	public BpPath setPath(BpPath newPath)
	{
		Object p = this.parent();
		
		if (p && p instanceof BpTreeReference)
		{
			return p.setPath(newPath);
		}
		
		p = this.@_path;
		
		this._path = newPath
		
		return p;
	}
	
	public BpPath getPath()
	{
		BpTreeTrait p = this.parent();
		return p && p instanceof BpTreeReference ? p.getPath() : this.@_path
	}
	
	public String getPath(Separators separator)
	{
		return getPath().getPath(separator)
	}
	
	public String getPath(String separator)
	{
		return getPath().getPath(separator)
	}
	
	public String getPath(String separator, String rootPrefix)
	{
		return getPath().getPath(separator, rootPrefix)
	}
	
	public String getSlug()
	{
		return getPath().getName()
	}
	
	public String getJsonPath()
	{
		return getPath().getPath(BpPath$Separators.JSON_SEPARATOR)
	}
	
	public int loadType()
	{
		return this.@_loadType;
	}
	
	public void loadType(int loadType)
	{
		this._loadType = loadType;
	}
	
	public Object serialize(String slug, Object value)
	{
		value = this.root().serialize(this, slug, value);
		
		if (this.loadType()
			&&	value instanceof IBpTreeLoadable 
			&&	!value.isReady()
		){
			
			this.evaluateResult(this.loadType(), value.load(this.loadType()));
		}
		
		return value
	}
	
	@Override
	public String toString()
	{
		return "${this.getClass().getName()}@${System.identityHashCode(this)}"
	}
}


class BpTreeEnum
	extends
		BpTreeMap
	implements \
		BpEnumTrait \
{
	BpTreeEnum(
		  BpTreeReferenceCollection	reference
		, Enum				en
		)
	{
		super(reference)
		BpEnumTrait.super.setEnum(en)
	}
	
/*	BpTreeEnum(
		  BpTreeTrait		parent
		, String			slug
		, Enum				en
		)
	{
		super(parent, slug)
		BpEnumTrait.super.setEnum(en)
	}
*/	
	BpTreeEnum(
		BpTreeReferenceCollection		reference
		, BpEnumTrait		en
		)
	{
		this(reference, en.getEnum())
	}
	
/*	BpTreeEnum(
		BpTreeTrait			parent
		, String			slug
		, BpEnumTrait		en
		)
	{
		this(parent, slug, en.getEnum())
	}
*/	
}

class BpTreeSet
	extends TreeSet<Object>
	implements \
		  BpTreeTrait \
		, BpTreeLoadableTrait \
{
	private int currentSlug		= -1;
	
	def BpTreeSet(BpTreeTrait parent, String slug)
	{
		super(
//TODO		new CyclosPropertySort(
//				new CyclosPropertySortCustomOrder(['javaClass', 'id', 'internalName', 'name'])
//			)
		)
		
		BpTreeTrait.super.initBpTreeTrait(parent, slug)
	}
	def BpTreeSet(BpTreeTrait parent, String slug, Collection c)
	{
		this(parent, slug)
		addAll(c)
	}
	
	@Override
	public boolean add(Object value)
	{
		if (value == null)
		{
			//<debug>println "${this.toString()}.add(): skip adding null"
			return;
		}
		
		//<debug>println "${this.toString()}.add(): serialising"
		value = serialize(this.getNextSlug(), value);
		
		if (value == null)
		{
			//<debug>println "${this.toString()}.add(): skip adding null"
			return;
		}
		
		this.actionsReset();
		
		//<debug>println "${this.toString()}.add(${value.toString()}) to ${super.size()} others: "
		try {
			return super.add(value)
		}
		catch(Exception e)
		{
			super.each{
				//<debug>println it.getClass()
			}
			throw e
		}
	}
	
	public void clear()
	{
		this.@currentSlug		= -1;
		super.clear();
		this.getPath().removeAllChildren();
	}
	
	private String getNextSlug ()
	{
		return ++this.@currentSlug
	}
	
	public int load(int loadingStatesMask)
	{
		loadingStatesMask == 0 \
		? actionsRemaining()
		: this.load(loadingStatesMask, super.findAll{true});
	}
	
	public int compare(Object o1, Object o2)
	{
		return super.compare(o1, o2);
	}
	
/*	@Override
	public String toString()
	{
		return "${this.getClass().getName()}@${System.identityHashCode(this)}"
	}
*/}


//@ToString(includeNames=true)
class BpTreeMap
	extends TreeMap<String,Object>
	//extends TreeMapLazyRead
	implements \
		  BpTreeTrait \
		, BpTreeLoadableTrait \
{
	static final List ignoreOnCountDefault			= ['jsonPath', 'javaClass'] 
	private	CyclosPropertyExclusions	exclusions	= CyclosPropertyExclusions.staticExclusions;
	protected List ignoreOnCount					= ignoreOnCountDefault;
	
	// Special case for BpTreeRootTrait
	def BpTreeMap(
		  CyclosPropertySortCustomOrder		specialSort
		)
	{
		super(specialSort);
		
		assert this instanceof BpTreeRootTrait
		
		// initialise traits
		BpTreeTrait.super.initBpTreeTrait()
	}
	
	def BpTreeMap(
		BpTreeReferenceCollection		reference
		)
	{
		super(CyclosPropertySortCustomOrder.staticSortOrder);
		
		BpTreeTrait.super.initBpTreeTrait(reference)
		this.addPath()
	}
	
	def BpTreeMap(
		BpTreeTrait						parent
		, String						slug
		)
	{
		super(CyclosPropertySortCustomOrder.staticSortOrder);
		
		BpTreeTrait.super.initBpTreeTrait(parent, slug)
		this.addPath()
	}
	
	def BpTreeMap(
		  BpTreeTrait					parent
		, String						slug
		, Map<String,Object>			map
		)
	{
		this(parent, slug)
		
		putAll(map);
	}
	
	def BpTreeMap(
			BpTreeReferenceCollection	reference
			, CyclosPropertyExclusions	exclusions
			)
	{
		this(reference);
		
		this.exclusions.addAll(exclusions);
	}

	def BpTreeMap(
		  BpTreeTrait					parent
		, String						slug
		, CyclosPropertyExclusions		exclusions
		)
	{
		this(parent, slug);
		
		this.exclusions.addAll(exclusions);
	}
	
	def BpTreeMap(
		BpTreeReferenceCollection		reference
		, CyclosPropertySortCustomOrder	specialSort
		)
	{
		this(reference);
		
		super.comparator()?.getSort()?.addAll(specialSort);
	}

	def BpTreeMap(
		  BpTreeTrait					parent
		, String						slug
		, CyclosPropertySortCustomOrder	specialSort
		)
	{
		this(parent, slug);
		
		super.comparator()?.getSort()?.addAll(specialSort);
	}

	def BpTreeMap(
		  BpTreeReferenceCollection		reference
		, CyclosPropertyExclusions		exclusions
		, CyclosPropertySortCustomOrder	specialSort)
	{
		this(reference, specialSort);
		
		this.exclusions.addAll(exclusions);
	}
	
	def BpTreeMap(
		BpTreeTrait						parent
		, String						slug
		, CyclosPropertyExclusions		exclusions
		, CyclosPropertySortCustomOrder	specialSort)
	{
		this(parent, slug, specialSort);
		
		this.exclusions.addAll(exclusions);
	}
	
	def BpTreeMap(
		  BpTreeTrait					parent
		, String						slug
		, CyclosPropertySortCustomOrder	specialSort
		, Map<String,Object>			map
	)
	{
		this(parent, slug, specialSort);
		
		putAll(map);
	}
	
	
	protected void addPath()
	{
		if (this.options().exportJsonPath)
		{
			put('jsonPath', getPath())
		}
	}
	
/*	@Override
	public Set<TreeMap.Entry<String,Object>> entrySet()
	{
		//this.load()
		
		return super.entrySet();
	}
*/	
	@Override
	public boolean isEmpty()
	{
		//print "${this.toString()}.size(): "
		int s = {
			
			// get the real size of the map
			int s = super.size();
			
			// if no entries or size is greater than the number of ignored keys, then return size
			if (s == 0 || s > (this?.ignoreOnCount?.size()?:0)) return s
			
			// otherwise loop through the keySet and if any non-ignored key is found
			// return the size
			super.keySet().each{
				if (!this.ignoreOnCount.contains(it)) return s
			}
			
			// only ignored keys found, hence pretend size to be 0
			return 0
		}()
		////<debug>println "$s"
		return s == 0
	}
	
	public boolean isExcluded(String key)
	{
		////<debug>println "BpTreeMap.isExcluded(): Testing key for exclusion key=$key, this=${this.toString()}, this.exclusions=${this.exclusions}, isEmpty=${this.exclusions?.isEmpty()}, contains=${this.exclusions?.contains(key)}"
		
		switch (true)
		{
			case key == null:
			case this.exclusions?.contains(key):
				//<debug>println "${this.toString()}.isExcluded(): Key '$key' excluded"
				return true
		}
		return false
	}
	
	public int load(int loadingStatesMask)
	{
		loadingStatesMask == 0 \
		? actionsRemaining()
		: this.load(loadingStatesMask, super.values());
	}
	
	public Object put(Object value)
	{
		// don't bother, if value is null, as it would be ignored anyway as a value
		assert value == null, "Cannot assign ${value.getClass().getName()} to ${this.getClass().getName()} without key"
	}
	
	
/*	public Object put(BpTreeTrait value)
	{
		//<debug>println "${this.toString()}.put(${value?.getClass()?.name}@${System.identityHashCode(value)}) with path ${value.getPath()} and content path ${value.source().getPath()}"
		//<debug>println "${this.toString()}.put(${value?.getClass()?.name}@${System.identityHashCode(value)}) parent: ${this.keySet()}"
		//<debug>println "${this.toString()}.put(${value?.getClass()?.name}@${System.identityHashCode(value)}) value:  ${value.keySet()}"
		assert value.getPath().name, "Cannot assign ${value.getClass().getName()} to ${this.getClass().getName()} without key ($value.path)"
		return put(value.getPath().name, value)
	}
*/	
	
	public Object put(String key, Object value)
	{
		return put(key, value, false)
	}
	
	public Object put(String key, Object value, boolean force)
	{
		//<debug>println "${this.toString()}.put('$key', ${value?.getClass()?.name}@${System.identityHashCode(value)})"
		
		if (value == null)
		{
			//<debug>println "${this.toString()}.put($key): skip putting null"
			return;
		}
		
		if (key == 'class' && value instanceof Class)
		{
			key = 'javaClass';
			value = value.name
		}
		else
		{
			if (value instanceof Closure)
			{
				//<debug>println "BpTreeMap.put(): Evaluating closure"
				switch (true)
				{
				case value.metaClass.respondsTo('doCall', BpTreeTrait, String):
					value = value(this, key);
				break;
				
				case value.metaClass.respondsTo('doCall', BpTreeTrait):
					value = value(this);
					break;
					
				case value.metaClass.respondsTo('doCall', String):
					value = value(key);
					break;
					
				default:
					value = value();
					break;
				}
				
				if (value == null)
				{
					//<debug>println "BpTreeMap.put(): skip putting null"
					return;
				}
			}
			
			////<debug>println "BpTreeMap.put(): checking key -> $value"
			if (!force && (value instanceof Collection || value instanceof Map) && value.isEmpty())
			{
				println "${this.toString()}.put('$key'): skip putting empty Collection/Map"
				return
			}
			
			value = this.serialize(key, value);
			
			if (value == null)
			{
				//<debug>println "${this.toString()}.put('$key'): skip putting null"
				return;
			}
		}
		
		if (force || !isExcluded(key))
		{
			
			this.actionsReset();
			
			////<debug>println "BpTreeMap.put(): "
			return super.put(key, value)
		}
		//<debug>println "BpTreeMap.put('$key'): skip putting invalid key"
	}
	
	public void putAll(Map<Object,Object> map)
	{
		map.each{
			Map.Entry<Object,Object> entry ->
			put(entry.getKey() as String, entry.value)
		}
	}
	
	@Override
	public String toString()
	{
		return "${this.getClass().getName()}@${System.identityHashCode(this)}"
	}
}

//@SelfType(Collection)
//@CompileStatic
trait CollectionAppendOrCreateTrait
{
	public static enum ACTION_TYPE {
					  APPEND,
					  REPLACE,
				  }
				  
	private ACTION_TYPE actionType = ACTION_TYPE.APPEND
	
	public getActionType()
	{
		return this.actionType
	}
	
	public addAll(CollectionAppendOrCreateTrait a)
	{
		addAll(a.actionType, (Collection<String>) a)
	}
	
	public addAll(ACTION_TYPE actionType, Collection<String> c)
	{
		if (c == null)
		{
			return
		}
		
		if (actionType == ACTION_TYPE.REPLACE)
		{
			clear();
		}
		
		if (c.size() == 0)
		{
			return
		}
		
		super.addAll(c);
	}
	
    public boolean add(String s)
    {
		//<debug>println "CollectionAppendOrCreateTrait.add(): ${this.toString()}.add($s) with ${getActionType()}"
    	return super.add(s);
    }
}


class CyclosPropertySortCustomOrder
	extends LinkedHashSet<String>
	implements \
		  CollectionAppendOrCreateTrait \
		, Comparator<String> \
{
	private static LinkedHashSet<String>			defaultSortOrder	= ['jsonPath', 'javaClass', 'id', 'internalName', 'name']
	
	public  static CyclosPropertySortCustomOrder	staticSortOrder		= new CyclosPropertySortCustomOrder(true)
	
	private boolean							used = false
	
	
	def CyclosPropertySortCustomOrder()
	{
		super(defaultSortOrder)
		//<debug>println "CyclosPropertySortCustomOrder.<init>() -> $this"
	}
	
	def CyclosPropertySortCustomOrder(boolean markUsed)
	{
		this()
		this.used = markUsed
		//<debug>println "CyclosPropertySortCustomOrder.<init>($markUsed) -> $this"
	}
	
	def CyclosPropertySortCustomOrder(Collection<String> specialSort)
	{
		super(specialSort)
		//<debug>println "CyclosPropertySortCustomOrder.<init>($specialSort) -> $this"
	}
	
	def CyclosPropertySortCustomOrder(CollectionAppendOrCreateTrait.ACTION_TYPE actionType, Collection<String> specialSort)
	{
		super()
		addAll(actionType, specialSort)
		//<debug>println "CyclosPropertySortCustomOrder.<init>($actionType, $specialSort) -> $this"
	}
	
	@Override
	public boolean add(String value)
	{
		if (this.used) return false
		return super.add(value)
	}
	
	@Override
	public int compare(String k1, String k2)
	{
		this.used = true;
		
		int c = 0;
		
		//print "CyclosPropertySortCustomOrder.compare($k1, $k2) = "
		if (k1 != k2)
		{
			for(String it : this)
			{
				if (k1 == it)
				{
					c = -1;
				}
				else if (k2 == it)
				{
					c = +1;
				}
				if (c) break;
			}
			
			c = c?:k2==null?-1:k1.compareTo(k2);
		}
		////<debug>println c;
		return c;
	}
}


class CyclosPropertyExclusions
	extends ArrayList<String>
    implements \
		CollectionAppendOrCreateTrait \
{
	private static LinkedHashSet<String>			defaultExclusions	= ['class']
	
	public  static CyclosPropertyExclusions			staticExclusions	= new CyclosPropertyExclusions();

		def CyclosPropertyExclusions(Collection c)
	{
		super(c)
		//<debug>println "CyclosPropertyExclusions.<init>($c) -> $this"
	}
	
	def CyclosPropertyExclusions()
	{
		super(defaultExclusions)
		//<debug>println "CyclosPropertyExclusions.<init>() -> $this"
	}
}


class BpTreeReferenceCollection
	implements 
		  BpTreeTrait \
		, BpTreeLoadableTrait \
		, IBpContentClass \
{
	private BpTreeMap								_content;
	private String									contentClassName;
	private BpTreeMap								template;
	private String									templateSlug;
	private boolean									_contentLoadable	= false;
	private Object									_o;
	private BpTreeReference							_referenceAtPath;
	private boolean									_authorative		= false;
	private Map<String,BpTreeReference>				_refs				= new HashedMap<String,BpTreeReference>();
	
	BpTreeReferenceCollection(
		BpTreeRootTrait								root
		, BpTreeRootTrait.CollectionStorage			parent
		, int										loadType
		, Object									o
		)
	{
		assert parent			!=	null;
		assert o				!=	null;
		
		this._o					=	o;
		this.contentClassName	=	this.getContentClassName();
		this.templateSlug		=	"${this.@_o.getClass().getSimpleName()}@${System.identityHashCode(this)}";
		
		BpTreeTrait.super.initBpTreeTrait(root, parent, loadType);
		
		//<debug>println "${this.toString()} -> creating content"
		this._content			=	BpCyclos.create(this, this.@_o)
		//<debug>println "${this.toString()} -> done."
		
		assert this.@_content	!=	null;
		
		this._contentLoadable	= (this._content instanceof IBpTreeLoadable);
	}
	
	public BpTreeReference add(
			  BpTreeTrait parent
			, String slug
			)
	{
		this.add(parent, slug, false)
	}
	
	public BpTreeReference add(
			BpTreeTrait parent
			, String slug
			, Boolean authorative
			)
	{
		//<debug>println "${this}.add(${parent.toString()}, $slug) -> BpTreeReference"
		
		assert !authorative || !this.isAuthorative()
		
		
		BpTreeReference stub = new BpTreeReference(this, parent, slug) // , this.isAuthorative()
		
		this.@_refs.put(stub.getPath().getPath(), stub);
		
		//<debug>println "${this}.add() -> BpTreeReference done: ${stub.getPath()}"
		
		if (authorative || !this.isAuthorative())
		{
		//<debug>	println ""
		//<debug>	println "Compareing reference paths for ${getContentClass()}:"
		//<debug>	println "old: ${this.getPath()}"
		//<debug>	println "new: ${stub.getPath()}"
		//<debug>	println "cmp: ${stub.getPath().compareTo(this.getPath())}"
		//<debug>	println ""
			
			if (authorative || this._referenceAtPath == null || stub.getPath().compareTo(this.@_referenceAtPath.getPath()) < 0)
			{
				this._referenceAtPath	= stub;
			}
			
			if (authorative)
			{
				this.makeAuthorative(true);
			}
			
			//<debug>println "${this}.add() -> added '${stub.getJsonPath()}' to paths list: ${stub.toString()}"
			//<debug>println "${this}.add() -> ${this.@paths.keySet()}"
		}
		
		return stub
	}
	
	public BpTreeReference getAuthorative()
	{
		this.isAuthorative() ? this.@_referenceAtPath : null;
	}
	
	public Class getContentClass()
	{
		return this.@_o.getClass()
	}
	
	public String getContentClassName()
	{
		return this.@contentClassName
	}
	
	public Set<String> getRefPaths()
	{
		this.@_refs.keySet();
	}
	
	public boolean isAuthorative()
	{
		return this.@_authorative;
	}
	
	public boolean isAuthorative(BpTreeReference reference)
	{
		return this.isAuthorative() && reference == this.@_referenceAtPath;
	}
	
	@Override
	public boolean isReady()
	{
		this.isAuthorative() \
		&& BpTreeLoadableTrait.super.isReady() \
		&& (!this.@_contentLoadable || this.@_content.isReady());
	}
	
	public int load(BpTreeReference reference, int loadingStatesMask)
	{
		reference == this.@_referenceAtPath ? load(loadingStatesMask) : 0;
	}
	
	private void makeAuthorative(boolean updateRemainingActions = false)
	{
		assert this.@_referenceAtPath	!= null	, "requires at least one reference to be adde first ${->this.toString()}.getContent(BpTreeReference)";
		
		this._authorative				= true;
		
		this.@_referenceAtPath.source(this.@_content)
		
		this.@_content.getPath().setParent(this.@_referenceAtPath.getPath(), true);
		
		if (updateRemainingActions)
		{
			actionsRemove(IBpTreeLoadable.LoadingStates.UNRESOLVED_REFERENCES.mask)
		}
	}
	
	@Override
	public int load(int loadingStatesMask)
	{
		if (loadingStatesMask == 0)
		{
			println "loadingStatesMask == 0 returning ${actionsRemaining()} for ${this.toString()}@${this.getPath()}"
			return actionsRemaining();
		}
		
		if (this.isReady())
		{
			println "loadingStatesMask == $loadingStatesMask returning 0 for ${this.toString()}@${this.getPath()}"
			return 0;
		}
		
		if(!this.isAuthorative() && IBpTreeLoadable.LoadingStates.UNRESOLVED_REFERENCES.match(loadingStatesMask))
		{
			this.makeAuthorative();
		}
		
		int res = this.isAuthorative() ? 0 : IBpTreeLoadable.LoadingStates.UNRESOLVED_REFERENCES.mask;
		
		if(this.@_contentLoadable && IBpTreeLoadable.LoadingStates.CONTENT.match(loadingStatesMask))
		{
			res |= this.@_content.load(loadingStatesMask);
		}
		
		return evaluateResult(loadingStatesMask, res)
	}
	
	private Object getObject()
	{
		//print "${this.toString()}.getSimpleElement()"
		return this.@_o
	}
	
	public int compare(Object o1, Object o2)
	{
		return super.compare(o1, o2);
	}
}

public class BpTreeReference
	implements
		BpTreeTrait \
		, BpTreeLoadableTrait \
		, Map<String,Object> \
		, Comparable \
{
	private BpTreeReferenceCollection				_ref
	private BpTreeMap								_src;
	private int										_sourceIsSet = 0; //1 = set, 2 = authorative
	
	BpTreeReference(
			  BpTreeReferenceCollection				reference
			, BpTreeTrait							parent
			, String								slug
			, Boolean								fixSource
			)
	{
		this(reference, parent, slug)
		this.@_sourceIsSet = fixSource ? 1 : 0
	}
	
	BpTreeReference(
			BpTreeReferenceCollection				reference
			, BpTreeTrait							parent
			, String								slug
			)
	{
		this._ref									= reference
		
		BpTreeTrait.super.initBpTreeTrait(parent, slug)
		
		//<debug>println getPath()
		
		BpTreeMap source = this.@_src = new BpTreeMap(this, null as String);
		
//		//<debug>println "${this.toString()}.source() -> ['javaClass', ${_ref.content.getClass().getName()}]"
		if (this.options().exportJavaClass)
		{
			source.put('javaClass', reference.getContentClassName())
		}
		
//		//<debug>println "${this.toString()}.source() -> ['\$ref', ${_ref.getPath()}]"
		//source.put('$ref', "${ -> reference.getPath()}")
	}
	
	public int compare(Object o1, Object o2)
	{
		return super.compare(o1, o2);
	}
	
	public BpTreeReference resolve()
	{
		this.source(true)
		return this
	}
	
	public BpTreeReferenceCollection ref()
	{
		return this.@_ref
	}
	
	public BpTreeMap source()
	{
		return this.@_src
	}
	
	public BpTreeMap source(boolean serious)
	{
		if (serious && !this.@_sourceIsSet)
		{
			this._sourceIsSet = 1;
			
			// make sure, authorative reference is evaluated on collection
			// -> this will set this' source, if its the one
			this.ref().load(IBpTreeLoadable.LoadingStates.UNRESOLVED_REFERENCES.mask)
			
			if (this.@_sourceIsSet != 2)
			{
				//this.source().put('others', this.ref().getRefPaths());
				this.source().put('$ref', this.ref().getAuthorative().getPath().getPath());
			}
		}
		
		return this.source();
	}
	
	public BpTreeMap source(BpTreeMap source)
	{
		assert source != this;
		
		this._sourceIsSet = 2;
		
		this._src = source;
	}
	
	public Long getId()
	{
		//print "${this.toString()}.getId()"
		Long id = ref().getObject()?.id?:0;
		
		////<debug>println " -> $id"
		return id
	}
	
	@Override
	public int load(int loadingStatesMask)
	{
		if (loadingStatesMask == 0)
		{
			return actionsRemaining();
		}
		
		if(!this.@_sourceIsSet && IBpTreeLoadable.LoadingStates.UNRESOLVED_REFERENCES.match(loadingStatesMask))
		{
			this.source(true);
		}
		
		return evaluateResult(loadingStatesMask, (this.@_sourceIsSet ? 0 : IBpTreeLoadable.LoadingStates.UNRESOLVED_REFERENCES.mask))
	}
	
	public int size()
	{
		return this.source().size()
	}
	
	public boolean isEmpty()
	{
		return size() == 0
	}
	
	@Override
	public int compareTo(Object other)
	{
		//<debug>println "${this.toString()}.compareTo(Object) -> ${other.toString()}"
		return compare(this, other);
	}
	
	@Override
	public void clear() {
	}
	
	@Override
	public boolean containsKey(Object key) {
		return containsKey((String) key);
	}
	
	public boolean containsKey(String key) {
		return this.source()?.containsKey(key);
	}
	
	@Override
	public boolean containsValue(Object value) {
		return this.source()?.containsValue(value);
	}
	
	@Override
	public Set<Entry<String, Object>> entrySet()
	{
		//now we're gettign serious
		return this.source(true).entrySet();
	}
	
	@Override
	public Object get(Object key)
	{
		return this.source().get(key)
	}
	
/*	@Override
	public boolean isReady()
	{
		return this.@_sourceIsSet && BpTreeLoadableTrait.super.isReady();
	}
*/	
	@Override
	public Set<String> keySet()
	{
		//now we're gettign serious
		return this.source().keySet();
		return this.source(true).keySet();
	}
	
	@Override
	public Object put(String key, Object value)
	{
		return null;
	}
	
	@Override
	public void putAll(Map<? extends String, ? extends Object> m)
	{
	}
	
	@Override
	public Object remove(Object key)
	{
		return null;
	}
	
	@Override
	public Collection<Object> values() {
		return this.source().values();
	}
	
	@Override
	public String toString()
	{
		return "${this.getClass().getName().replace('$','@'+System.identityHashCode(_ref)+'$')}@${System.identityHashCode(this)}"
	}
	
	public static int compare(BpTreeReference o1, Object o2)
	{
		//<debug>println "BpTreeReference.compare(BpTreeReference, Object)"
		//<debug>println "${o1.toString()}.compare(Object) -> ${o2.toString()}"
		return 1;
	}
	
	public static int compare(BpTreeReference o1, BpTreeReference o2)
	{
		//<debug>println "BpTreeReference.compare(BpTreeReference, BpTreeReference)"
		//<debug>println "${this.toString()}.compare(BpTreeReference) -> ${o1.getId()} - ${o2.getId()}"
		return o1.getId() - o2.getId();
	//	return this.get('$ref').compareTo(other.get('$ref'));
	}
	
}

class BpOptions
{
	BpOptions()
	{
		
	}
	BpOptions(Map options)
	{
		options?.each{ String key, Object value ->
		this."$key" = value
		}
	}
}

@InheritConstructors
class BpTreeOptions
	extends BpOptions
{
	public		exportJsonPath			= false;
	public		exportJavaClass			= false;
}

@InheritConstructors
class BpCyclosConfigOptions
	extends BpTreeOptions
{
	public		exportServerTimestamp	= true;
}

class BpCyclosConfig
{
	private ScriptBindingBP				_binding;
	private BpCyclosConfigOptions		_options;
	private User						user;
	private ChannelConfiguration		channelConfiguration;
	private Configuration				rootConfiguration;
	
	public	LinkedHashMap<String, String> jsonPathTranslations	=	new HashMap<String, String>();

	public static final Version			apiVersion				=	Version.valueOf('0.0.1');
	public static final Version			version					=	Version.valueOf('0.0.2');
	public static final String			resultObjectType		=	"org.bristolpound.api.cyclos.config.json.root";

	
	BpCyclosConfig(ScriptBindingBP binding)
	{
		this(binding, [:])
	}
	
	BpCyclosConfig(ScriptBindingBP binding, Map options)
	{
		this(binding, new BpCyclosConfigOptions(options))
	}
	
	BpCyclosConfig(ScriptBindingBP binding, BpCyclosConfigOptions options)
	{
		//<debug>println "BpCyclosConfig.<init>(): ${this.toString()}, super.config()=${config()} in BpCyclosConfig<init>($binding)"
		
		
		if (!(binding.sessionData.loggedUser.isGlobalAdmin() || binding.sessionData.loggedUser.	isNetworkAdmin()))
		{
			throw new Exception('must be network or admin');
		}
		
		this._binding				= binding;
		this._options				= options;
		this.user					= binding.sessionData.loggedUser;
		this.channelConfiguration	= binding.sessionData.getChannelConfiguration();
		this.rootConfiguration		= this.channelConfiguration.getConfiguration();
		
		//<debug>println "BpCyclosConfig.<init>(): ${this.user}"
		//<debug>println "BpCyclosConfig.<init>(): ${this.rootConfiguration} ${this.rootConfiguration.getLevel()}"
		while (this.rootConfiguration?.getLevel() > 1)
		{
			this.rootConfiguration = this.rootConfiguration.getParent()
			//<debug>println "BpCyclosConfig.<init>(): ${this.rootConfiguration}	${this.rootConfiguration.getLevel()}"
		}
	}
	
	
	public BpCyclosConfigExport Export()
	{
		switch (true)
		{
		case this.user.isGlobalAdmin():
			return this.Export(new GlobalNetwork(this.binding))
			
		case this.user.isNetworkAdmin():
			return this.Export(this.user.getNetwork())
			
		default:
			return this.Export(this.user)
			
		}
	}
	
	public BpCyclosConfigExport Export(SimpleEntity e)
	{
		assert e != null
		
		//<debug>println "BpCyclosConfig.Export($e)"
		BpCyclosConfigExport ex = new BpCyclosConfigExport(this, e);
		//<debug>println "BpCyclosConfig.Export($e) = ${ex.toString()}  done!";
		//<debug>println "";
		
		ex.loadAll();
		
		return ex
	}
	
	public ScriptBindingBP getBinding() //GroovyScriptEngineImpl
	{
		return this.@_binding;
	}
	
	public String getJavaClass()
	{
		return this.resultObjectType;
	}
	
	public Object getMetadata(BpTreeTrait parent)
	{
		return [
			apiVersion:				this.apiVersion.toString(),
			serverTimestamp:		this.getOptions().exportServerTimestamp ? null : null,
			configScope:			'GLOBAL',
			jsonPathTranslations:	jsonPathTranslations,
			info: [
				libs: [
					Java:				System.getProperty("java.runtime.version"),
					GroovySystem:		GroovySystem.version,
					BpCyclosConfig:		this.version.toString(),
					//LibWs:			LibWs.version.toString(),
					//ScriptBindingBP:	ScriptBindingBP.version.toString(),
				],
				server: new org.cyclos.CyclosVersion(),
				binding: [
					pricipal: binding.sessionData.getPrincipal(),
					pricipalType: binding.sessionData.getPrincipalType(),
					userIsGlobal: this.user.isGlobal(),
				],
			],
		];
	}
	
	public BpCyclosConfigOptions getOptions()
	{
		return this._options;
	}
	
	public Configuration getRootConfiguration()
	{
		return this.rootConfiguration;
	}
	
	public User getUser()
	{
		return this.user;
	}
}

class BpCyclosConfigExport
	extends BpTreeMap
	implements \
	  BpTreeRootTrait \
{
	private BpCyclosConfig									_config;
	private	String											rootNodeName	= 'config'
	private	GlobalNetwork									globalNetwork;
	
	private BpCyclosConfigExport(BpCyclosConfig config)
	{
		super(new CyclosPropertySortCustomOrder(['javaClass', 'metadata']));
		
		this._config	= config;
		
		BpTreeRootTrait.super.init();
		
		////<debug>println "BpCyclosConfigExport<init>(): ${this.toString()}, config=$config, super.config()=${super.config()} in BpCyclosConfigExport<init>($config)"
		
		put('javaClass'	, this.config().&getJavaClass);
		put('metadata'	, this.config().&getMetadata);
		
		this.globalNetwork = new GlobalNetwork(config.binding)
	}
	
	BpCyclosConfigExport(BpCyclosConfig config, BpTreeMap content)
	{
		this(config);
		
		put(this.@rootNodeName, content);
	}
	
	BpCyclosConfigExport(BpCyclosConfig config, SimpleEntity e)
	{
		this(config)
		
		if (e instanceof GlobalNetwork)
		{
			this.globalNetwork	= e;
			this.@rootNodeName	= e.getInternalName();
			
			assert this.@rootNodeName != null
		}
		
		// needs to be in a separate line, as it refers to {this} and hence requires it's own constructor
		put(this.@rootNodeName, e, true);
	}
	
	public ScriptBindingBP binding()
	{
		return this.config().getBinding();
	}
	
	public BpCyclosConfig config()
	{
		return this.@_config;
	}
	
	/*	public BpCyclosConfigExport add(SimpleEntity e)
	{
		//<debug>println "${this.toString()}.add(${e?.getClass()?.name}@${System.identityHashCode(e)})"
		
		BpTreeMap rootNode = this.get(this.@rootNodeName)
		
		assert rootNode != null
		
		//BpTreeReference serialized = super..serialize(rootNode, null as String, e).resolve()
		
		//<debug>println "${this.toString()}.add() -> ${serialized.getClass()?.name}@${System.identityHashCode(serialized)}) with path ${serialized.getPath()} and content path ${serialized.source().getPath()}"
		//<debug>println "${this.toString()}.add() -> Path: ${serialized.getPath()}"
		//<debug>println "${this.toString()}.add() -> Name: ${serialized.getPath().getName()}"
		
		rootNode.put(serialized.getPath().name, serialized)
		
		return this
	}
*/	
	public BpCyclosConfigExport add(String key, SimpleEntity e)
	{
		////<debug>println "BpCyclosConfigExport.add($key, ${e?.getClass()?.name}): ${this.toString()}, =${super.} in add"
		this.content.put(key	, super..serialize(this.content, key, e))
		
		return this
	}
	
	public getGlobalNetwork()
	{
		return this.@globalNetwork;
	}
	
	public BpTreeReference getReference(BpTreeTrait parent, String slug, SimpleEntity e)
	{
		return BpTreeRootTrait.super.getOrCreateReference(parent, slug, e);
	}
	
	public BpTreeOptions options()
	{
		return this.config().getOptions();
	}
	
	public BpTreeMapPlain toMap()
	{
		this.load()
		
		return new BpTreeMapPlain(this)
	}
	
	public String toJson()
	{
		return toMap().toJson()
	}
	
	public String toJsonPrettyPrint()
	{
		return groovy.json.JsonOutput.prettyPrint(toJson())
	}
	
	public BpTreeReference	serialize(BpTreeTrait parent, String slug
		, SimpleEntity content
		)
	{
		//<debug>println "BpCyclosConfig.serialize(): adding a SimpleEntity (${content?.getClass()?.name})"
		
		return this.getReference(parent, slug, content);
	}
	
	public Object			serialize(BpTreeTrait parent, String slug
		, Object content
		)
	{
		if (content == null) return content;
		
		String	name	= content.getClass().getName();
		Map		ret		= this.options().exportJavaClass
						? [javaClass: name]
						: [:]
		
		switch (name)
		{
		case 'org.cyclos.entities.utils.TimeInterval':
			return ret + [
				amount:						content.getAmount(),
				field:						content.getField(),
				milliseconds:				content.getMilliseconds(),
				isValid:					content.isValid(),
				];
		
		case 'org.cyclos.entities.utils.IntegerRange':
			return content;
		
		case 'org.cyclos.model.banking.accounts.SystemAccountOwner':
			return [
				javaClass:					name,
				];
			
		case 'org.cyclos.entities.utils.NumberGeneration':
			return ret + [
				padLength:					content.getPadLength(),
				pattern:					content.getPattern(),
				prefix:						content.getPrefix(),
				suffix:						content.getSuffix(),
				isUsed:						content.isUsed() ,
				];
			
		case 'org.cyclos.entities.system.AccountNumberConfiguration':
			return ret + [
				mask:						content.getMask(),
				script:						content.getScript(),
				scriptParameters:			content.getScriptParameters(),
				isEnabled:					content.isEnabled(),
				];
			
		case 'org.cyclos.entities.system.AddressConfiguration':
			return ret + [
				enabledAddressFields:		content.getEnabledAddressFields(),
				requiredAddressFields:		content.getRequiredAddressFields(),
				];
			
		case 'org.cyclos.entities.utils.LatLong':
			return ret + [
				latitude:					content.getLatitude(),
				longitude:					content.getLongitude(),
				];
			
		case 'org.cyclos.entities.system.OutboundSmsConfiguration':
			return ret + [
				encoding:					content.getEncoding(),
				gatewayUrl:					content.getGatewayUrl(),
				headers:					content.getHeaders(),
				maxMessagesPerMonth:		content.getMaxMessagesPerMonth(),
				maxMessagesToUnregistered:	content.getMaxMessagesToUnregistered(),
				password:					content.getPassword(),
				postBody:					content.getPostBody(),
				requestType:				content.getRequestType(),
				script:						content.getScript(),
				scriptParameters:			content.getScriptParameters(),
				username:					content.getUsername(),
				isAsciiOnly:				content.isAsciiOnly(), 
				isEnabled:					content.isEnabled(),
				isPasswordSet:				content.isPasswordSet(),
				];
			
		case 'org.cyclos.entities.system.SmtpConfiguration':
			return ret + [
				fromAddress:				content.getFromAddress(),
				host:						content.getHost(),
				password:					content.getPassword(),
				port:						content.getPort(),
				security:					content.getSecurity(),
				user:						content.getUser(),
				isPasswordSet:				content.isPasswordSet(),
				];
			
		case 'org.cyclos.entities.system.CustomSessionConfiguration':
			return ret + [
				script:						content.getScript(),
				scriptParameters:			content.getScriptParameters(),
				isEnabled:					content.isEnabled(),
				];
			
		case 'org.cyclos.entities.system.UserFormatConfiguration':
			return ret + [
		//TODO:	profileFields:				content.getProfileFields(),
				separator:					content.getSeparator(),
				];
			
		case 'org.cyclos.entities.utils.FileSize':
			return ret + [
				size:						content.getSize(),
				unit:						content.getUnit(),
				isValid:					content.isValid(),
				];
			
		case 'org.cyclos.entities.utils.Dimensions':
			return ret + [
				height:						content.getHeight(),
				width:						content.getWidth(),
				];
			
		case 'org.cyclos.entities.utils.Dimensions':
			return ret + [
				fromAddress:				content.getFromAddress(),
				host:						content.getHost(),
				password:					content.getPassword(),
				port:						content.getPort(),
				security:					content.getSecurity(),
				user:						content.getUser(),
				isPasswordSet:				content.isPasswordSet(),
				];
			
		default:
			println "found unknown class: $name @ ${parent.getPath()}.$slug"
			return [
				javaClass	: name ,
				data		: content.toString(),
			]
		}
	}
}

class BpTreeMapPlain
	extends TreeMap //LinkedHashMap
{
	//@Delegate
	private BpTreeMap m;
	
	public static BpTreeMapPlain toMap(Map map)
	{
		int level			= -1
		def dumpedObjects	= new HashedMap<Object,Object>()
		def PRESENT			= new Object();
		def dumpRecursively
		List paths			= [];
		
		dumpRecursively = {String k, Object y ->
			++level
			Object n;
			
			paths.add(k)
			
			int id = System.identityHashCode(y)
			//<debug>println "$paths.size: ${paths.join('.')} -> ${y.getClass().getSimpleName()}"
			//String prefix = "${(id as String).padLeft(10)}-${(level as String).padLeft(2)} ${'-'*level}"
			//print "$prefix $k: ${y instanceof BpTreeTrait ? y.getPath() + ' -> ' + dumpedObjects.size() :''} "
			
			//TODO: 
			assert !(y instanceof BpTreeTrait) || dumpedObjects.put(id, PRESENT) == null, "Object ${y.getClass().getSimpleName()}" //@${y.getPath()} $y has already been added!"
			
			if (y instanceof Closure)
			{
				//print "Evaluating ... "
				y = y()
			}
			
			if (y == null)
			{
				n = null
			}
			else if (y instanceof CharSequence)
			{
				n = y.toString()
			}
			else if (y instanceof Map)
			{
				//println "MAP"
				n = new BpTreeMapPlain();
				
				y.each{ Object key, Object value ->
					assert key != null, "Null-Key found at $paths.size for ${value.toString()}";
					
					key = (key instanceof Class) ? key.getSimpleName() : key.toString();
					
					n.put(key, dumpRecursively(key, value))
				}
			}
			else if (y instanceof Collection)
			{
				//println "SET"
				n = []
				y.eachWithIndex{Object value, i ->
					n[i] = dumpRecursively(i as String, value)
				}
			}
			else
			{
				//println "${String s =y.toString(); s.substring(0, Integer.min(s.length(),100))} (${y.getClass().getSimpleName()})"
				//println "${org.apache.commons.StringUtils.substringBefore(y.toString(), '\n')} (${y.getClass().getSimpleName()})"
				//println "${StringUtils.substringBefore(y?.toString(),'\n')?:'null'} (${y.getClass().getSimpleName()})"
				n = y
			}
			
			paths.remove(paths.size - 1)
			
			--level
			return n
		}
		
		return dumpRecursively('root', map)
	}
	
	BpTreeMapPlain()
	{
	}
	
	BpTreeMapPlain(BpTreeMap map)
	{
		//this.m = map;
		super.putAll(toMap(map))
	}
	
	public toMap()
	{
		return toMap(this)
	}
	
	public String toJson()
	{
		return toJsonGroovyJson()
	}
	
	public String toJson(Closure parser)
	{
		return parser(this.toMap())
	}
	
	public String toJsonGroovyJson()
	{
		return toJson(groovy.json.JsonOutput.&toJson)
	}
	
	public String toJsonFasterxmlJackson()
	{
		ObjectMapper mapper = new ObjectMapper();

		return toJson(mapper.&writeValueAsString)
	}
}

class GlobalNetwork
	extends Network
{
	private final	String			_internalName;
	
	private final 	ScriptBindingBP	binding;
	
	GlobalNetwork(binding)
	{
		this.binding		= binding;
		this._internalName	= binding.networkService.GLOBAL_INTERNAL_NAME
		
		assert this._internalName != null
	}
	
	@Override
	public AdminGroup					getAdministrators()
	{
		return this.binding.groupService.getSystemAdmins()
	}
	
	@Override
	public boolean isGlobal()
	{
		return true
	}
	
	@Override
	public Long getId()
	{
		return 0
	}
	
	@Override
	public String getInternalName()
	{
		return this.@_internalName;
	}
	
	@Override
	public String getName()
	{
		return "Global <Network>"
	}
	
	@Override
	public Integer getVersion()
	{
		return 1
	}
	
	@Override
	public Network getNetwork()
	{
		return null
	}
	
	@Override
	public int hashCode()
	{
		return System.identityHashCode(this)
	}
	
	@Override
	public String toString()
	{
		return "${this.getClass().getName()}#$id: $name, internalName=$_internalName"
	}
	
	/*
	public Configuration					getDefaultConfiguration()
	{}

	public java.util.Set<BpCyclosAdminGroup>	getManagedByGroups()
	{
		java.util.Set<BpCyclosAdminGroup> s = []
		
		this.s().getManagedByGroups().each{
			ag ->
			s << new BpCyclosAdminGroup(this, 'managedByGroups', ag)
		}
		
		return s
	}

	public Configuration					getParentConfiguration()
	{
		return ''
	}
	
	public BpCyclosAdminGroup					getRegisteredByGroup()
	{
		return new BpCyclosAdminGroup(
			this
			, 'registeredByGroup'
			, this.s().getRegisteredByGroup()
		)
	}
	*/
}

abstract class BpCyclos
{
	protected static Map<String,Class>	loadedClasses	= new TreeMap<String,Class>()
	private   static GroovyShell		_groovyShell;
	
	static {
		//<debug>println "BpCyclos<clinit>(): BpCyclos  ..."
		
		this._groovyShell = new GroovyShell(BpCyclos.getClassLoader())
		
		BpCyclos.getClassLoader().getLoadedClasses().findAll({
			Class clazz ->  //<debug>println "BpCyclos<clinit>(): checking ${clazz}"; 
			clazz.getName().startsWith('BpCyclos')
		}).each({
			Class clazz -> //<debug>println "BpCyclos<clinit>(): checking ${clazz.getName()} -> ${java.lang.reflect.Modifier.isAbstract(clazz.getModifiers())} ${clazz.getInterfaces()}"
			////<debug>println "BpCyclos<clinit>(): ${clazz.getName()}.getAllInterfaces() ->${ClassUtils.getAllInterfaces(clazz)}"
		}).findAll({
			Class clazz -> clazz.getInterfaces().contains(IBpCyclos) //&& !java.lang.reflect.Modifier.isAbstract(clazz.getModifiers())
		}).each({
			Class clazz -> BpCyclos.addCreator(clazz) //BpCyclosSimpleEntity.&create
		})
	}
	
	protected static boolean addCreator(Class clazz)
	{
		addCreator(clazz, clazz.cyclosClasses)
	}
	
	protected static boolean addCreator(Class clazz, Class<?>[] cyclosClasses)
	{
		//<debug>println "BpCyclos.addCreator($clazz, $cyclosClasses) adding constructor for $clazz ..."
		
		cyclosClasses.each{ Class cyclosClass ->
			
			String body = (
				clazz.respondsTo('getCreatorCode', Class, Class) ? clazz : BpCyclos
				).getCreatorCode(clazz, cyclosClass).replace('\t',' ') as String
			
			//<debug>println body
			
			groovy.lang.Closure closure  = _groovyShell.evaluate(body.replace('\n', '; '))
			
			clazz.getMetaClass().static.create = closure
			BpCyclos.getMetaClass().static.create << closure
			
			loadedClasses.put(cyclosClass.name, clazz)
		}
	}
	
	private static String getCreatorCode(Class clazz, Class cyclosClass)
	{
		"""\
		{	BpTreeReferenceCollection reference, ${cyclosClass.name} content ->
			
			/*<debug>
			println "${clazz.name}.create(): adding a ${cyclosClass.name} (\${content?.getClass()?.name})"
			*/
			return new ${clazz.name}(reference, content)
		}
		"""
	}

	public static boolean create()
	{
		//dummy function to initialize the class.
	}
}

interface IBpContentClass
{
	public Class	getContentClass()
	public String	getContentClassName()
}

interface IBpCyclos
{
	static Class<?>[]	cyclosClasses
	static String		creatorCode;
}

class BpCyclosAccount
extends BpCyclosNetworkedEntity
implements \
IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [Account]
	}
	
	public BpCyclosAccount(
		BpTreeReferenceCollection			reference
		, Account				ac
		)
	{
		super(reference, ac, new CyclosPropertyExclusions([
			'accountLimitLog',			// life data
			'amountReservations',		// life data
			'closedAccountBalances',	// life data
			'lastBalanceClosingDate',	// life data
			'negativeSince',			// life data
			]
		));
	}
}

class BpCyclosAdminGroup
	extends BpCyclosGroup
{
	public static Class<?>[] getCyclosClasses()
	{
		return [AdminGroup]
	}
	
	private Long			id;
	private BpCyclosConfig	config;
	private Object			binding;
	
	BpCyclosAdminGroup(
		BpTreeReferenceCollection				reference
		, AdminGroup				ag
		)
	{
		super(reference, ag)
		
		//<debug>println "${this.toString()}.<init>()"
	}
	
	public java.util.Set<Product>			getAllProducts()
	{
		java.util.Set<BpCyclosAdminProduct> s = []
		
		this.simpleEntity().getAllProducts().each{
			o -> s << new BpCyclosAdminProduct(this, 'allProducts', o)
		}
		
		return s
	}
	
	public java.util.Set<BpCyclosNetwork>		getManagedNetworks()
	{}
	
	public java.util.Set<Configuration>		getPossibleParentConfigurations()
	{}
}

@InheritConstructors
class BpCyclosAdminProduct
	extends BpCyclosUserManagementProduct
{
//	public static Boolean initialized = BpCyclosWrapper.initializeMe(BpCyclosAdminProduct, AdminProduct)
	
	public String							getMyInfo()
	{
		return "${this.toString()}"
	}
	
	java.util.Set<AdminGroup>				getAccessibleAdminGroups()
	{}
	
	java.util.Set<VoucherConfiguration>		getAccessibleVoucherConfigurations()
	{}
	
	AdminGroup								getAdminGroup()
	{}
	
	AdminGroupAccessibility					getAdminGroupAccessibility()
	{}
	
	java.util.Set<Language>					getApplicationTranslation()
	{}
	
	java.util.Set<AdminProductAuthorizationRole>	getAuthorizationRoles()
	{}
	
	java.util.Set<PaymentTransferType>		getChargebackPaymentsToSystem()
	{}
	
	java.util.Set<? extends BasicGroup>		getGroups()
	{}
	
	java.util.Set<Configuration>			getManageConfigurations()
	{}
	
	java.util.Set<ProductManagement>		getManagedProducts()
	{}
	
	java.util.Set<Product>					getManageIndividualProducts()
	{}
	
	java.util.Set<MessageCategory>			getManageMessagesSentToSysWithCat()
	{}
	
	ProductNature							getNature()
	{}
	
	java.util.Set<MessageCategory>			getSendMessagesFromSysWithCat()
	{}
	
	SpecificConfigurationAccess				getSpecificConfigurationAccess()
	{}
	
	java.util.Set<SystemAccountType>		getSystemAccounts()
	{}
	
	java.util.Set<CustomOperation>			getSystemCustomOperationsRun()
	{}
	
	java.util.Set<SystemImageCategory>		getSystemImageCategoriesManage()
	{}
	
	java.util.Set<SystemImageCategory>		getSystemImageCategoriesView()
	{}
	
	java.util.Set<PaymentTransferType>		getSystemPaymentRequestsSendToUser()
	{}
	
	java.util.Set<PaymentTransferType>		getSystemPayments()
	{}
	
	java.util.Set<ProductSystemRecordType>	getSystemRecordTypes()
	{}
	
	java.util.Set<PaymentTransferType>		getSystemToSystemPayments()
	{}
	
	java.util.Set<PaymentTransferType>		getSystemToUserExternalPayments()
	{}
	
	java.util.Set<PaymentTransferType>		getSystemToUserPayments()
	{}
	
	java.util.Set<TransferFilter>			getSystemTransferFilters()
	{}
	
	java.util.Set<MessageCategory>			getViewMessagesSentToSysWithCat()
	{}
	
	java.util.Set<ProductAdminVoucher>		getVouchers()
	{}
	
}

@InheritConstructors
class BpCyclosBaseEntity
	extends BpCyclosNetworkedEntity
{
	
}

@InheritConstructors
class BpCyclosBaseStaticContent
	extends BpCyclosBaseEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [BaseStaticContent]
	}
	
}

class BpCyclosBaseHelpContent
	extends BpCyclosBaseStaticContent
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [BaseHelpContent]
	}
	
	public BpCyclosBaseHelpContent(
		  BpTreeReferenceCollection			reference
		, BaseHelpContent			se
		)
	{
		super(reference, se, new CyclosPropertyExclusions(['originalContent']));
		
		if (se.getDefinitionType() == HelpContentDefinitionType.ORIGINAL)
		{
			put('originalContent', se.getOriginalContent(), true)
		}
		
	}
}

@InheritConstructors
class BpCyclosEnum
	extends \
		BpTreeEnum \
	implements \
		  IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [Enum,BpEnum,BpEnumTrait]
	}
}

class BpCyclosTransaction
	extends BpCyclosNetworkedEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [Transaction]
	}
	
	public BpCyclosTransaction(
			BpTreeReferenceCollection				reference
			, Transaction				ta
			)
	{
		super(reference, ta, new CyclosPropertyExclusions(['by']));
		
//		if (e.getDefinitionType() == HelpContentDefinitionType.ORIGINAL)
//		{
//			put('originalContent', e.getOriginalContent(), true)
//		}
		
	}
}

@InheritConstructors
class BpCyclosBasicGroup
	extends BpCyclosConfigurationEntity
{
//	public static Boolean initialized = BpCyclosWrapper.initializeMe(BpCyclosBasicGroup, BasicGroup)
	
	public Configuration					getConfiguration()
	{}
	
	public BasicGroupNature					getNature()
	{}
	
	public java.util.Set<Product>			getProducts()
	{}
}

class BpCyclosBasicProfileField
	extends BpCyclosEnum
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [BasicProfileField]
	}
	
	BpCyclosBasicProfileField(
		  BpTreeReferenceCollection		reference
		, BasicProfileField		bpf
		)
	{
		super(reference, bpf);
	}
}

@InheritConstructors
class BpCyclosChannel
	extends BpCyclosConfigurationEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [Channel]
	}
	
}

@InheritConstructors
class BpCyclosConfigurationEntity
	extends BpCyclosNamedEntity
{
}

class BpCyclosCustomField
	extends BpCyclosOrderableEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [CustomField]
	}
	
	public static List defaultExclusion = [
		'allowedMimeTypes',				// only for CustomFieldType.FILE
		'decimalDigits',				// only for CustomFieldType.DECIMAL
		'decimalValueRange',			// only for CustomFieldType.DECIMAL
		'defaultBooleanValue',			// only for CustomFieldType.BOOLEAN
		'defaultDateToday',				// only for CustomFieldType.DATE
		'defaultDateValue',				// only for CustomFieldType.DATE
		'defaultDecimalValue',			// only for CustomFieldType.DECIMAL
		'defaultIntegerValue',			// only for CustomFieldType.INTEGER
		'defaultRichTextValue',			// only for CustomFieldType.RICH_TEXT
		'defaultStringValue',			// only for CustomFieldType.STRING
		'defaultTextValue',				// only for CustomFieldType.TEXT
		'dynamicScript',				// only for CustomFieldType.DYNAMIC_SELECTION
		'dynamicScriptParameters',		// only for CustomFieldType.DYNAMIC_SELECTION
		'integerValueRange',			// only for CustomFieldType.INTEGER
		'lengthConstraint',				// only for CustomFieldType.CHAR[STRING,TEXT,RICH_TEXT]
		'linkedEntityType',				// only for CustomFieldType.LINKED_ENTITY
		'maxFiles',						// only for CustomFieldType.FILE
		'maxWordSize',					// only for CustomFieldType.CHAR[STRING,TEXT,RICH_TEXT]
		'otherMimeTypes',				// only for CustomFieldType.FILE
		'pattern',						// only for CustomFieldType.CHAR[STRING,TEXT,RICH_TEXT]
		'possibleValueCategories',		// only for CustomFieldType.SELECTION[SINGLE_SELECTION,MULTI_SELECTION]
		'possibleValueClass',			// only for CustomFieldType.SELECTION[SINGLE_SELECTION,MULTI_SELECTION]
		'possibleValues',				// only for CustomFieldType.SELECTION[SINGLE_SELECTION,MULTI_SELECTION]
	];
	
	public static List defaultExclusionCHAR = defaultExclusion - [
		'lengthConstraint',				// only for CustomFieldType.[STRING,TEXT,RICH_TEXT]
		'maxWordSize',					// only for CustomFieldType.[STRING,TEXT,RICH_TEXT]
		'pattern',						// only for CustomFieldType.[STRING,TEXT,RICH_TEXT]
	];
	
	public static List defaultExclusionSELECTION = defaultExclusion + [
	];
	
	public static List defaultExclusionMANUAL_SELECTION = defaultExclusionSELECTION + [
		'possibleValueCategories',		// only for CustomFieldType.SELECTION[SINGLE_SELECTION,MULTI_SELECTION]
		'possibleValueClass',			// only for CustomFieldType.SELECTION[SINGLE_SELECTION,MULTI_SELECTION]
		'possibleValues',				// only for CustomFieldType.SELECTION[SINGLE_SELECTION,MULTI_SELECTION]
	];

	public static CyclosPropertyExclusions defaultExclusionBOOLEAN				= new CyclosPropertyExclusions(
		defaultExclusion + [
		'defaultBooleanValue',			// only for CustomFieldType.TEXT
	]);

	public static CyclosPropertyExclusions defaultExclusionDATE					= new CyclosPropertyExclusions(
		defaultExclusion + [
		'defaultDateToday',				// only for CustomFieldType.DATE
		'defaultDateValue',				// only for CustomFieldType.DATE
	]);
	
	public static CyclosPropertyExclusions defaultExclusionDECIMAL				= new CyclosPropertyExclusions(
		defaultExclusion + [
		'decimalDigits',				// only for CustomFieldType.DECIMAL
		'decimalValueRange',			// only for CustomFieldType.DECIMAL
		'defaultDecimalValue',			// only for CustomFieldType.DECIMAL
	]);
	
	public static CyclosPropertyExclusions defaultExclusionDYNAMIC_SELECTION	= new CyclosPropertyExclusions(
		defaultExclusionSELECTION + [
		'dynamicScript',				// only for CustomFieldType.DYNAMIC_SELECTION
		'dynamicScriptParameters',		// only for CustomFieldType.DYNAMIC_SELECTION
	]);
	
	public static CyclosPropertyExclusions defaultExclusionFILE					= new CyclosPropertyExclusions(
		defaultExclusion + [
		'allowedMimeTypes',				// only for CustomFieldType.FILE
		'maxFiles',						// only for CustomFieldType.FILE
		'otherMimeTypes',				// only for CustomFieldType.FILE
	]);
	
	public static CyclosPropertyExclusions defaultExclusionIMAGE				= new CyclosPropertyExclusions(
		defaultExclusion + [
	]);
	
	public static CyclosPropertyExclusions defaultExclusionINTEGER				= new CyclosPropertyExclusions(
		defaultExclusion + [
		'defaultIntegerValue',			// only for CustomFieldType.INTEGER
		'integerValueRange',			// only for CustomFieldType.INTEGER
	]);
	
	public static CyclosPropertyExclusions defaultExclusionLINKED_ENTITY		= new CyclosPropertyExclusions(
		defaultExclusion + [
		'linkedEntityType',				// only for CustomFieldType.LINKED_ENTITY
	]);
	
	public static CyclosPropertyExclusions defaultExclusionMULTI_SELECTION		= new CyclosPropertyExclusions(
		defaultExclusionMANUAL_SELECTION + [
	]);
	
	public static CyclosPropertyExclusions defaultExclusionSINGLE_SELECTION		= new CyclosPropertyExclusions(
		defaultExclusionMANUAL_SELECTION + [
	]);
	
	public static CyclosPropertyExclusions defaultExclusionRICH_TEXT			= new CyclosPropertyExclusions(
		defaultExclusionCHAR + [
		'defaultRichTextValue',			// only for CustomFieldType.TEXT
	]);
	
	public static CyclosPropertyExclusions defaultExclusionTEXT					= new CyclosPropertyExclusions(
		defaultExclusionCHAR + [
		'defaultTextValue',				// only for CustomFieldType.TEXT
	]);
	
	public static CyclosPropertyExclusions defaultExclusionSTRING				= new CyclosPropertyExclusions(
		defaultExclusionCHAR + [
		'defaultStringValue',			// only for CustomFieldType.TEXT
	]);
	
	public static CyclosPropertyExclusions defaultExclusionURL					= new CyclosPropertyExclusions(
		defaultExclusion + [
	]);
	
	public BpCyclosCustomField(
			BpTreeReferenceCollection		reference
			, CustomField		cf
			)
	{
		super(reference, cf, BpCyclosCustomField.@"defaultExclusion${cf.getType()}");
	}
}

@InheritConstructors
class BpCyclosCustomScript
	extends BpCyclosConfigurationEntity
{
//	public static Boolean initialized = BpCyclosWrapper.initializeMe(BpCyclosCustomScript, CustomScript)
	
	java.util.Set<BpCyclosCustomScript>	getDependencies()
	{
		//ToDo
		//return	customScriptService.getReverseDependencyIds(this.id)
	}
}

@InheritConstructors
class BpCyclosGroup
extends BpCyclosBasicGroup
{
//	public static Boolean initialized = BpCyclosWrapper.initializeMe(BpCyclosGroup, Group)
	
	public UserStatus						getInitialUserStatus()
	{}
	
	public Role								getRole()
	{}
}

class BpCyclosLanguage
	extends BpCyclosConfigurationEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [Language]
	}
	
	private int useCount		= 0;
	
	BpCyclosLanguage(
		  BpTreeReferenceCollection		reference
		, SimpleEntity			se
		)
	{
		super(reference);
		
		Language eParent		= se.getParent();
		
		if (eParent)
		{
			this.initTreePathCyclosSimpleEntity(se)
			//TODO increase useCount in parent
		}
		else
		{
			this.put('name'			, se.getName())
			this.put('internalName'	, se.getInternalName())
			this.put('children'		, se.getChildren())
			
			this.isSimpleEntityLoaded(true);
		}
	}
}

@InheritConstructors
class BpCyclosNamedEntity
	extends BpCyclosBaseEntity
{
	
}

class BpCyclosNetwork
	extends BpCyclosConfigurationEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [Network]
	}
	
	private Long			_id;
	private ScriptBindingBP	_binding;
	
	public BpCyclosNetwork(
		  BpTreeReferenceCollection	reference
		, Network			n
		)
	{
		super(reference, n)
		
		assert n != null
		
		this._id		= n?.id ?: 0
		this._binding	= this.root().binding();
		
		//<debug>println ""
		//<debug>println "${this.toString()}<init>(${parent.toString()}.path, $slug, ${n.toString()}) " + binding.networkService.GLOBAL_INTERNAL_NAME
		////<debug>println "BpCyclosNetwork<init>() ${this._config}, ${this._binding}, $binding "
		
	}
	
	
	public int load(int loadingStatesMask)
	{
		if (loadingStatesMask == 0)
		{
			return actionsRemaining();
		}
		
		if (this.isReady())
		{
			return 0;
		}
		
		//<debug>println "${this.toString()} -> _seLoaded = ${this.isSimpleEntityLoaded()}"
		if(!this.isSimpleEntityLoaded() && IBpTreeLoadable.LoadingStates.CONTENT.match(loadingStatesMask))
		{
			if (!this._id)
			{
				assert this.@_binding.sessionData.loggedUser.isGlobalAdmin();
				
				this.put('channels'		, this._binding.sessionData.getChannelConfiguration().getConfiguration().channels)
				this.put('passwordTypes', this._binding.passwordTypeService.listAll())
				this.put('networks'		, this.getNetworks())
				
			}
			
			this.put('administrators'	, this.getAdministrators())
			this.put('customScripts'	, this.getScripts())
			
			this.isSimpleEntityLoaded(true);
			
			return IBpTreeLoadable.LoadingStates.CONTENT.mask;
		}
		
		return this.load(loadingStatesMask, super.values());
	}
	
	private BpTreeMap getNetworks()
	{
		NetworkServiceLocal networkService = this.@_binding.networkService;
		
		BpTreeMap networks = new BpTreeMap(this, 'networks');
		
		//<debug>println "BpCyclosNetwork<init>(${parent.toString()}, $slug, ${n.toString()}) looking up networks ..." + binding.networkService.GLOBAL_INTERNAL_NAME
		
		networkService.search(new NetworkQuery(
			returnDisabled: true,
			pageSize: 10
		)).each
		{ NetworkVO nvo ->
			//<debug>println "${nvo}"
			networks.put("$nvo.internalName" as String, networkService.find(nvo.id))
		}
		
		return networks
	}
	
	private BpTreeMap getScripts()
	{
		//<debug>println "${this.toString()}.getScripts(): ${getPath()} , ${this.id}"
		
		BpTreeMap scriptTypes = new BpTreeMap(this, 'customScripts');
		
		CustomScriptServiceLocal customScriptService = this.@_binding.customScriptService
		Long id = this._id
		
		org.cyclos.model.system.scripts.ScriptType.values().each
		{
			ScriptType scriptType ->
			
			//<debug>println "${this.toString()}.getScripts(): ${getPath()} $scriptType"
			
			BpTreeMap scripts = new BpTreeMap(scriptTypes, scriptType as String);
			
			customScriptService.listByType(scriptType).each
			{
				CustomScript script ->
				
				//<debug>println "${this.toString()}.getScripts(): ${getPath()} $script.name, $script.global, $script.network"
				//scripts.put(script.name, script)
				if (id == 0 ? script.isGlobal() : id == script.network?.id)
				{
					scripts.put(script.name, script)
				}
			}
			
			if (scripts.size())
			{
				scriptTypes.put(scriptType.toString(), scripts);
			}
		}
		
		return scriptTypes
	}

	public AdminGroup getAdministrators()
	{
		//<debug>println "${this.toString()}.getAdministrators() looking up AdminGroup ..."
		
		return this.simpleEntity().getAdministrators()
		
		return new BpCyclosAdminGroup(
			this
			, 'administrators'
			, this.simpleEntity().getAdministrators()
		)
	}

	/*
	public Configuration					getDefaultConfiguration()
	{}

	public java.util.Set<BpCyclosAdminGroup>	getManagedByGroups()
	{
		java.util.Set<BpCyclosAdminGroup> s = []
		
		this.s().getManagedByGroups().each{
			ag ->
			s << new BpCyclosAdminGroup(this, 'managedByGroups', ag)
		}
		
		return s
	}

	public Configuration					getParentConfiguration()
	{
		return ''
	}
	
	public BpCyclosAdminGroup					getRegisteredByGroup()
	{
		return new BpCyclosAdminGroup(
			this
			, 'registeredByGroup'
			, this.s().getRegisteredByGroup()
		)
	}
	*/
}

@InheritConstructors
class BpCyclosNetworkedEntity
	extends BpCyclosSimpleEntity
{
	public Network					getNetwork()
	{
		if (this.isGlobal())
		{
			this.root().getGlobalNetwork();
		}
		
		return this.simpleEntity().getNetwork()
	}
	
	public boolean					isGlobal()
	{
		return this.simpleEntity().isGlobal()
	}
}


@InheritConstructors
class BpCyclosOrderableEntity
	extends BpCyclosConfigurationEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [OrderableEntity]
	}
	
}

@InheritConstructors
class BpCyclosPasswordType
	extends BpCyclosConfigurationEntity
	implements \
		IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [PasswordType]
	}
	
}









////<debug>println BpCyclosAdminGroup.initialized

@InheritConstructors
class BpCyclosProduct
	extends BpCyclosConfigurationEntity
{
//	public static Boolean initialized = BpCyclosWrapper.initializeMe(BpCyclosProduct, Product)
	
	java.util.Set<BasicGroup>				getAccessibleUserGroups()
	{}
	
	java.util.Set<Agreement>				getAgreements()
	{}
	
	java.util.Set<Channel>					getChannels()
	{}
	
	java.util.Set<ProductDashboardAction>	getDashboardActions()
	{}
	
	java.util.Set<DocumentCategory>			getDocumentCategoryView()
	{}
	
	java.util.Set<? extends BasicGroup>		getGroups()
	{}
	
	java.util.Set<BasicGroup>				getGroupsForUserDirectory()
	{}
	
	java.util.Set<ProductMyAccessClient>	getMyAccessClients()
	{}
	
	java.util.Set<ProductMyProfileField>	getMyProfileFields()
	{}
	
	java.util.Set<ProductMyRecordType>		getMyRecordTypes()
	{}
	
	//abstract ProductNature					getNature()

	java.util.Set<MyProductPasswordAction>	getPasswordActions()
	{}
	
	java.util.Set<Permission>				getPermissions()
	{}
	
	java.util.Set<PrincipalType>			getPrincipalTypes()
	{}
	
	Role									getRole()
	{}
	
	java.util.Set<ProductTransferStatusFlow>	getTransferStatusFlows()
	{}
	
	UserGroupAccessibility					getUserDirectoryOnGroups()
	{}
	
	UserGroupAccessibility					getUserGroupAccessibility()
	{}
	
	java.util.Set<ProductUserProfileField>	getUserProfileFields()
	{}
	
	java.util.Set<TransactionCustomField>	getVisibleTransactionFields()
	{}
	
}

class BpCyclosSimpleEntity
	extends BpTreeMap
	implements \
		  IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [SimpleEntity]
	}
	
	public int compare(BpCyclosSimpleEntity o1, BpCyclosSimpleEntity o2)
	{
		return o1.simpleEntity().id - o2.simpleEntity().id
	}
	
	private SimpleEntity				_se;
	protected boolean					_seLoaded			= false;
	
	public BpCyclosSimpleEntity(
			BpTreeReferenceCollection				reference
			)
	{
		super(reference);
	}
	
	public BpCyclosSimpleEntity(
		  BpTreeReferenceCollection				reference
		, SimpleEntity					se
		)
	{
		super(reference);
		this.initTreePathCyclosSimpleEntity(se)
	}
	
	public BpCyclosSimpleEntity(
		  BpTreeReferenceCollection				reference
		, SimpleEntity					se
		, CyclosPropertyExclusions		exclusions
		)
	{
		super(reference, exclusions);
		this.initTreePathCyclosSimpleEntity(se);
	}
	
	public BpCyclosSimpleEntity(
		  BpTreeReferenceCollection				reference
		, SimpleEntity					se
		, CyclosPropertySortCustomOrder	specialSort
		)
	{
		super(reference, specialSort);
		this.initTreePathCyclosSimpleEntity(se);
	}

	public BpCyclosSimpleEntity(
		  BpTreeReferenceCollection				reference
		, SimpleEntity					se
		, CyclosPropertyExclusions		exclusions
		, CyclosPropertySortCustomOrder	specialSort
		)
	{
		super(reference, exclusions, specialSort);
		this.initTreePathCyclosSimpleEntity(se);
	}
	
	protected initTreePathCyclosSimpleEntity(SimpleEntity se)
	{
		this._se = se;
		
		// if wer're not in lazy mode, just load streigth away
		if (this.loadType())
		{
			this.loadAll(this.loadType());
		}
	}
	
	public boolean isSimpleEntityLoaded()
	{
		this.@_seLoaded;
	}
	
	public void isSimpleEntityLoaded(boolean loaded)
	{
		this._seLoaded = loaded;
	}
	
	public int load(int loadingStatesMask)
	{
		if (loadingStatesMask == 0)
		{
			return actionsRemaining();
		}
		
		if (this.isReady())
		{
			return 0;
		}
		
		if(!this.isSimpleEntityLoaded() && IBpTreeLoadable.LoadingStates.CONTENT.match(loadingStatesMask))
		{
			putAll();
			return IBpTreeLoadable.LoadingStates.CONTENT.mask;
		}
		
		return this.load(loadingStatesMask, super.values());
	}
	
	public putAll()
	{
		putAll(this.simpleEntity());
	}
	
	public putAll(SimpleEntity se)
	{
		assert this.@_se != se || this.isSimpleEntityLoaded() == false;
		
		String				propertyName;
		String				funcName;
		BpClassInfo			sourceInfo		= BpClassInfo.getOrAddClazzInfo(se)
		
		sourceInfo.prop.each{PropertyDescriptor	propertyDesc
		->
			propertyName	= propertyDesc.getName();
			
			funcName		= 'get'+ propertyName.capitalize();
			
			// skip if already existing
			if (super.containsKey(propertyName)) return;
			
			if (this.hasProperty(propertyName))
			{
				this.put(propertyName, this.getAt(propertyName))
			}
			if (this.respondsTo(funcName))
			{
				this.put(propertyName, this."$funcName"())
			}
			// skip write-only properties by checking if the property _has_ a readMethod
			else
			if (propertyDesc.getReadMethod())
			{
				//printf("%s: ",propertyName);
				
				if (!this.isExcluded(propertyName))
				{
					this.put(propertyName, se[propertyName])
				}
			}
		}
		
		if (this.@_se == se)
		{
			this.isSimpleEntityLoaded(true);
		}
	}
	
	public Object put(String key, Object value, boolean force)
	{
		//<debug>println "${this.toString()}.put('$key', ${value?.getClass()?.name} ${value?.toString()})"
		switch(true)
		{
			case !force && this.isExcluded(key):
				return
				
			case (value instanceof Closure):
				value = value()
			
		//	case (value instanceof SimpleEntity):
		//		value = super..serialize(this, key, value)
				
			default:
				return super.put(key, value, true)
		}
	}
	
	public SimpleEntity simpleEntity()
	{
		return this.@_se;
	}
	
	public SimpleEntity simpleEntity(SimpleEntity newE)
	{
		SimpleEntity oldE;
		oldE	= this.@_se;
		this._se	= newE;
		return oldE;
	}
	
	public String getJavaClass()
	{
		return this._se?.class.name;
	}
	
	@Override
	public String toString()
	{
		String myClass = this.getClass().getName();
		
		if (myClass == 'BpCyclosSimpleEntity')
		{
			myClass += "<${this.simpleEntity()}>"
		}
		
		if (this.@_se)
		{
			myClass +="#${this.@_se.id}"
		}
		return "$myClass@${System.identityHashCode(this)}"
	}
}

/*
class BpCyclosSystemAccountOwner
	extends \
		BpTreeMap \
	implements \
		  IBpCyclos \
{
	public static Class<?>[] getCyclosClasses()
	{
		return [SystemAccountOwner]
	}
	
	private SystemAccountOwner sao
	
	public BpCyclosSystemAccountOwner(
		  BpTreeTrait parent
		, String slug
		, SystemAccountOwner sao
		)
	{
		super(parent, slug);
		
		this.sao = sao
		
		put('data', sao.toString())
	}
}
*/

@InheritConstructors
class BpCyclosUserManagementProduct
	extends BpCyclosProduct
{
//	public static Boolean initialized = BpCyclosWrapper.initializeMe(BpCyclosUserManagementProduct, UserManagementProduct)
	
	java.util.Set<PaymentTransferType>		getChargebackPaymentsToUser()
	{}
	
	java.util.Set<DocumentCategory>			getDocumentCategoryManage()
	{}
	
	java.util.Set<PaymentTransferType>		getExternalPaymentsAsUser()
	{}
	
	java.util.Set<PaymentTransferType>		getPaymentsAsUser()
	{}
	
	java.util.Set<PaymentTransferType>		getRequestPaymentsAsUser()
	{}
	
	java.util.Set<PaymentTransferType>		getSelfPaymentsAsUser()
	{}
	
	java.util.Set<PaymentTransferType>		getSystemPaymentsAsUser()
	{}
	
	java.util.Set<ProductUserAccessClient>	getUserAccessClients()
	{}
	
	java.util.Set<UserAccountType>			getUserAccountsAccess()
	{}
	
	java.util.Set<ProductUserAdField>		getUserAdFields()
	{}
	
	java.util.Set<CustomOperation>			getUserCustomOperationsRun()
	{}
	
	java.util.Set<UserProductPasswordAction>	getUserPasswordActions()
	{}
	
	java.util.Set<PaymentTransferType>		getUserPaymentRequestsSendToSystem()
	{}
	
	java.util.Set<PaymentTransferType>		getUserPaymentRequestsSendToUser()
	{}
	
	java.util.Set<PaymentTransferType>		getUserPaymentsAsUser()
	{}
	
	java.util.Set<ProductUserRecordType>	getUserRecordTypes()
	{}
	
	java.util.Set<ProductUserTokenType>		getUserTokenTypes()
	{}
	
	java.util.Set<TransferFilter>			getUserTransferFilters()
	{}
		
}





//===========================================
//------  libScriptBinding ------------------
//
// @version: 1.5.0

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.zafarkhaja.semver.Version;
//import com.github.zafarkhaja.semver.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.groovy.GroovyException;

import org.cyclos.entities.access.Channel;
import org.cyclos.entities.access.Password;
import org.cyclos.entities.access.PasswordType;
import org.cyclos.entities.system.ChannelConfiguration;
import org.cyclos.entities.system.Configuration;
import org.cyclos.entities.system.CustomField;
import org.cyclos.entities.system.CustomFieldPossibleValue;
import org.cyclos.entities.system.CustomFieldValue;
import org.cyclos.entities.system.SmsChannelConfiguration;
import org.cyclos.entities.users.BasicUser;
import org.cyclos.entities.users.MobilePhone;
import org.cyclos.entities.users.QMobilePhone;
import org.cyclos.entities.users.RecordType;
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserCustomField;
import org.cyclos.entities.users.UserCustomFieldValue;
import org.cyclos.entities.users.UserRecord;
//import org.cyclos.model.EntityNotFoundException;
import org.cyclos.model.access.passwordtypes.PasswordCharacterPolicy;
import org.cyclos.model.system.channelconfigurations.SmsChannelConfigurationDTO;
import org.cyclos.model.system.channelconfigurations.SmsChannelConfigurationData;
import org.cyclos.model.system.fields.CustomFieldPossibleValueVO;
import org.cyclos.model.system.fields.CustomFieldVO;
import org.cyclos.model.system.fields.CustomFieldValueForSearchDTO
import org.cyclos.model.users.records.UserRecordQuery;
import org.cyclos.model.users.recordtypes.RecordTypeVO;
import org.cyclos.model.users.users.UserDetailedVO;
import org.cyclos.model.users.users.UserLocatorVO;
import org.cyclos.model.users.users.UserVO;

class ScriptBindingBP {

	public static final String          version                 = Version.valueOf('1.5.0');

	protected Object                            binding;

//  private static  ResultInfoList  info;
	private static HashMap<String, Long>        systemUserIds       = new HashMap<String, Long>();
	private static HashMap<String, Pattern>     compiledPatterns    = new HashMap<String, Pattern>();
	private String log      = "";

	public \
	log(String message)
	{
		this.log = "${this.log}${message}\n";
	}

	public String \
	getLog( message)
	{
		return this.log;
	}

//  private static init(binding, info)
	ScriptBindingBP(binding)
	{
		this.binding    = binding;
//      this.info       = info;
	}

	ScriptBindingBP(ScriptBindingBP scriptBindingBP)
	{
		this.binding    = scriptBindingBP.binding;
	}

	public Boolean \
	test()
	{
		return true;
	}

	protected ScriptBindingBP \
	getContext()
	{
		return this;
	}

	public Object \
	getBinding()
	{
		return this.binding;
	}

	public org.cyclos.impl.ApplicationHandler \
	getApplicationHandler()
	{
		return this.binding.applicationHandler;
	}

	public org.cyclos.impl.utils.persistence.EntityManagerHandler \
	getEntityManagerHandler()
	{
		return this.binding.entityManagerHandler;
	}

	public org.cyclos.impl.utils.conversion.ConversionHandler \
	getConversionHandler()
	{
		return this.binding.conversionHandler;
	}

	public org.cyclos.impl.access.PasswordHandler \
	getPasswordHandler()
	{
		return this.binding.passwordHandler;
	}

	public org.cyclos.impl.users.RecordFieldHandler \
	getRecordFieldHandler()
	{
		return this.binding.recordFieldHandler;
	}

	public org.cyclos.impl.utils.formatting.FormatterImpl \
	getFormatter()
	{
		return this.binding.formatter;
	}

	public \
	getFormParameters()
	{
		return this.binding.formParameters
	}

	public \
	getScriptParameters()
	{
		return this.binding.scriptParameters
	}

	public org.cyclos.impl.system.ScriptHelper \
	getScriptHelper()
	{
		return this.binding.scriptHelper;
	}

	public org.cyclos.impl.access.SessionData \
	getSessionData()
	{
		return this.binding.sessionData;
	}

	public com.fasterxml.jackson.databind.ObjectMapper \
	getObjectMapper()
	{
		return this.binding.objectMapper;
	}

	public org.cyclos.utils.ParameterStorage \
	getPathVariables()
	{
		return this.binding.pathVariables;
	}

	public org.cyclos.model.utils.RequestInfo \
	getRequest()
	{
		return this.binding.request;
	}

	public org.cyclos.impl.system.ConfigurationServiceLocal \
	getConfigurationService()
	{
		return this.binding.configurationService;
	}

	public org.cyclos.impl.system.CustomScriptServiceLocal \
	getCustomScriptService()
	{
		return this.binding.customScriptService;
	}
	
	public org.cyclos.impl.users.GroupServiceLocal \
	getGroupService()
	{
		return this.binding.groupService;
	}

	public org.cyclos.impl.system.NetworkServiceLocal \
	getNetworkService()
	{
		return this.binding.networkService;
	}
	
	public org.cyclos.impl.users.RecordServiceLocal \
	getRecordService()
	{
		return this.binding.recordService;
	}
	
	public org.cyclos.impl.access.PasswordTypeServiceLocal \
	getPasswordTypeService()
	{
		return this.binding.passwordTypeService;
	}
	
	public org.cyclos.impl.users.UserServiceLocal \
	getUserService()
	{
		return this.binding.userService;
	}

	public String \
	userMaskPassword(BasicUser u, String text)
	{
		for (Password p : u.passwords)
		{
			text = userMaskPassword(u, p, text);
		}

		return text;
	}

	public Object get(String name)
	{
		println "${this} missing property $name"
		return this.binding."$name"
	}
	
	public Object MissingProperty(String name)
	{
		println "${this} missing property $name"
		return this.binding."$name"
	}
	
	public String \
	userMaskPassword(BasicUser u, Password p, String text)
	{
		Pattern pattern;
		String  patternString = "";

		for (Object x : [
			[name: "lower", pattern: "a-z", policy: p.type.lowerCaseLetters],
			[name: "upper", pattern: "A-Z", policy: p.type.upperCaseLetters],
			[name: "number", pattern: "0-9", policy: p.type.numbers],
			[name: "special", pattern: "`@!\"#\$%&'()*+,-./:;<=>?\\[\\\\\\]^_{}~", policy: p.type.specialCharacters],
		])
		{
			switch (x.policy)
			{
				case PasswordCharacterPolicy.valueOf("NOT_ALLOWED"):
					//ignore
					continue;

				case PasswordCharacterPolicy.valueOf("ALLOWED"):
					patternString = patternString + x.pattern;
					break;

				case PasswordCharacterPolicy.valueOf("REQUIRED"):
					patternString = patternString + x.pattern;
					break;
			}
		}

		if (patternString == "")
		{
			return text;
		}

		patternString = "[" + patternString + "]{${p.type.length.min},${p.type.length.max}}"

		pattern = compiledPatterns.get(patternString);

		if (pattern == null)
		{
			pattern = Pattern.compile(patternString);
			compiledPatterns.put(patternString, pattern);
		}

		Matcher matcher = pattern.matcher(text);

		while(matcher.find())
		{
			String match = text.substring(matcher.start(), matcher.end());

			if (passwordHandler.matches(p, match))
			{
				text    = StringUtils.replace(text, match, "[${p.type.name}:${p.status}]")

				matcher.reset();
			}
		}

		return text;
	}

	public Boolean \
	userWithMobilePhoneExists(String phoneNumber)
	{
		return this.userService.existsUserWithMobilePhone(StringUtils.prependIfMissing(phoneNumber, "+"))
	}

	public Long \
	getSystemUserId()
	{
		String system_user  = this.scriptParameters.system_user;

		if (StringUtils.isNumeric(system_user))
		{
			return applicationHandler.idMask.remove(Long.parseLong(system_user));
		}
		else if(systemUserIds.containsKey(system_user))
		{
			return systemUserIds.get(system_user);
		}
		else
		{
			UserDetailedVO      userDetailedVO      = this.getUser(system_user);

			systemUserIds.put(system_user, userDetailedVO.id);

			return userDetailedVO.id;
		}
	}

	public UserDetailedVO \
	getUserVO(String username)
	{
		UserLocatorVO       userLocatorVO       = new UserLocatorVO(username: username);

		return userService.locate(userLocatorVO)
	}

	public BasicUser \
	getUser(String username)
	{
		return getUser(this.getUserVO(username).id);
	}

	public BasicUser \
	getUser(Long userId)
	{
		return entityManagerHandler.find(BasicUser, userId)
	}

	public Long \
	findUserIdByPhoneNumber(String phoneNumber)
	{
		if (this.userWithMobilePhoneExists(phoneNumber))
		{
			return findUserByPhoneNumber(phoneNumber).id;
		}

		return getSystemUserId();
	}

	public BasicUser \
	findUserByPhoneNumber(String phoneNumber)
	{
		MobilePhone             mobilePhone         = findMobilePhone(phoneNumber);

		if (mobilePhone != null)
		{
			return mobilePhone.user;
		}

		try
		{
			return entityManagerHandler.find(BasicUser, getSystemUserId())
		}
		catch (EntityNotFoundException e)
		{
			return null
		}
	}

	public MobilePhone \
	findMobilePhone(String phoneNumber)
	{
		MobilePhone             mobilePhone         = null;

		// Resolve the normalized international phone number via the subscriber parameter
		if (phoneNumber == null)
		{
			threw new Exception("The subscriber parameter is missing")
		}

		if (!this.userWithMobilePhoneExists(phoneNumber))
		{
			return null;
		}

		// Find the mobile phone in Cyclos
		phoneNumber = StringUtils.prependIfMissing(phoneNumber, "+");

		QMobilePhone mp = QMobilePhone.mobilePhone;

		return  entityManagerHandler
				.from(mp)
				.where(
					  mp.normalizedNumber.eq(phoneNumber)
					, mp.user().status.eq(
						org.cyclos.model.users.users.UserStatus.ACTIVE)
				)
				.singleResult(mp);
	}

	public UserCustomFieldValue \
	getUserCustomFieldValue(BasicUser u, String fieldname)
	{
		for (UserCustomFieldValue cv : u.customValues)
		{
			if (cv.field.internalName == fieldname)
			{
				return cv;
			}
		}

		throw new EntityNotFoundException(UserCustomFieldValue,
			"'" + fieldname + "' not found for user '" + u.username + "'");
	}

	public String \
	getEnumeratedStringValue(CustomFieldValue v)
	{
		for (CustomFieldPossibleValue pf : v.getEnumeratedValues())
		{
			return pf.internalName;
		}
	}

	public  String \
	getEnumeratedStringValueOrDefault(
			BasicUser   u,
			String      fieldname,
			String      defaultValue
			)
	{
		String s = defaultValue;

		try
		{
			if (u != null)
			{
				UserCustomFieldValue v = getUserCustomFieldValue(u, fieldname);

				s = getEnumeratedStringValue(v)
			}
		}
		catch(EntityNotFoundException ex)
		{
			// fallback to system
		}

		if (s == defaultValue)
		{
			s = getCustomFieldDefault(fieldname);
		}

		return s;
	}

	public String \
	getCustomFieldDefault(String fieldname)
	{
		for (CustomFieldVO cf : this.binding.userCustomFieldService.list())
		{
			if (
				cf.internalName == fieldname
			)
			{
				return this.getCustomFieldDefault(
					entityManagerHandler.find(UserCustomField, cf.id)
					);
			}
		}

		throw new EntityNotFoundException(UserCustomField, fieldname);
	}

	public String \
	getCustomFieldDefault(CustomField cf)
	{
		switch (cf.type)
		{

		case "MULTI_SELECTION":
		case "SINGLE_SELECTION":

			for (CustomFieldPossibleValue pf : cf.getPossibleValues())
			{
				if (pf.isDefaultValue())
				{
					return pf.internalName;
				}
			}

			throw new EntityNotFoundException(CustomFieldPossibleValue,
				"Field '" + pf.internalName + "' has no default value");


		default:
			return cf.getDefaultValue()?.stringValue;

		}
	}

	public  UserRecord \
	getUserCustomRecord(
			String      recordTypeName,
			String      phoneNumber,
			java.util.ArrayList<Object>
						searchValues
								)
	{
		return getUserCustomRecord(
			recordTypeName,
			findUserIdByPhoneNumber(phoneNumber),
			searchValues
			);
	}

	public  UserRecord \
	getUserCustomRecord(
			String      recordTypeName,
			Long        userId,
			java.util.ArrayList<Object>
						searchValues
			)
	{
		return getUserCustomRecord(
			recordTypeName,
			new UserVO(userId),
			searchValues
			);
	}

	public  UserRecord \
	getUserCustomRecord(
			String      recordTypeName,
			UserVO      userVO,
			java.util.ArrayList<Object>
						searchValues
			)
	{
		RecordType      recordType  =   entityManagerHandler.find(RecordType, recordTypeName);
		UserRecordQuery urq         =   new UserRecordQuery();
		urq.type                    =   new RecordTypeVO(id: recordType.id);
		urq.user                    =   userVO;
		urq.pageSize                =   2000;
		Set setCustomValues         =   new HashSet();

		urq.setSharedRecordFieldSearch(false);

		String log="\nrecordTypeName=${recordTypeName}\nsearchValues=\n${searchValues}";

		CustomFieldValueForSearchDTO cf;

		for (Object o : searchValues)
		{
			CustomField f           = recordFieldHandler.findField(recordType,
																new CustomFieldVO(internalName: o.name)
															  );

			cf                      = new CustomFieldValueForSearchDTO();
			cf.field                = new CustomFieldVO(id: f.id);

			log=log + "\n${o.name}=${f.id}"

			switch (f.type)
			{

			case "MULTI_SELECTION":
			case "SINGLE_SELECTION":

				for (CustomFieldPossibleValue pf : f.getPossibleValues())
				{
					if (pf.internalName == o.value)
					{
						log=log + "\n${o.value}=${pf.id}"

						cf.enumeratedValues = [new CustomFieldPossibleValueVO(pf.id)]
						break;
					}
				}
				break;

			case "TEXT":
			case "STRING":
				cf.stringValue = o.value;
				log=log + "\n${cf.stringValue}"
				break;
			}

			setCustomValues.add(cf);
		}

		urq.customValues    = setCustomValues;

		// execute query
		def pageUserRecords = recordService.search(urq);

		log=log + "\npageUserRecords.pageItems.size=" + pageUserRecords.pageItems.size()

		if (pageUserRecords.pageItems.size() == 1)
		{
			return entityManagerHandler.find(UserRecord, pageUserRecords.pageItems[0].id);
		}

		if (pageUserRecords.pageItems.size() == 0)
		{
			return null;
		}

		throw new GroovyException("not unique record." + log)
	}


//  public static ResultInfoList getInfo()
//  {
//      return info;
//  }

	//@see http://stackoverflow.com/a/18251429/3102305

	public static <T> T \
	newInstance(
		  final Class<?>    clazz
		, final Class<?>[]  types
		, final Object...   args
	)
	throws
		ClassNotFoundException,
		NoSuchMethodException,
		InstantiationException,
		IllegalAccessException,
		IllegalArgumentException
	{
	  return (T) clazz.getConstructor(types).newInstance(args);
	}

	public static <T> T \
	newInstance(
		  final Class<?>    clazz
		, final Object...   args
	)
	throws
		ClassNotFoundException,
		NoSuchMethodException,
		InstantiationException,
		IllegalAccessException,
		IllegalArgumentException
		//InvocationTargetException
	{
		// Derive the parameter types from the parameters themselves.

		Class[] types = new Class[args.length];

		for ( int i = 0; i <types.length; i++ )
		{
			types[i] = args[i].getClass();
		}

		return (T) newInstance(clazz, types, args);
	}

	public static <T> T \
	newInstance(
		  final String      className
		, final Object...   args
	)
	throws
		ClassNotFoundException,
		NoSuchMethodException,
		InstantiationException,
		IllegalAccessException,
		IllegalArgumentException
	{
		return (T) newInstance(Class.forName(className), args);
	}

	public static Version parseVersion(final String versionString, final Class error, final String formatString)
	{
		try
		{
			return Version.valueOf(versionString);
		}
		catch(ParseException ex)
		{
			throw this.newInstance(error, formatString);
		}
	}

	public SmsChannelConfiguration \
	getSmsChannelConfig()
	{
		Configuration conf = sessionData.configuration.configuration;

		log(conf.name);

		while(true)
		{
			for (ChannelConfiguration channelCfg : conf.channelConfigurations)
			{
				log("" + [channel: channelCfg.channel.name, conf: channelCfg, isSms: channelCfg.channel.isSms(), isEnabled: channelCfg.isEnabled(), isDefined: channelCfg.isDefined()] );

				if (
					channelCfg.isEnabled()
				&&  channelCfg.isDefined()
				&&  channelCfg.channel.isSms()
				)
				{
					return channelCfg;
				}
			}

			if (conf.parent)
			{
				conf = conf.parent;
			}
			else break;
		}

		throw new EntityNotFoundException(SmsChannelConfiguration);
	}

	public Object \
	getSmsChannelConfigBp()
	{
		SmsChannelConfiguration     smsconf = getSmsChannelConfig();
		SmsChannelConfigurationDTO  smsDto  = binding.channelConfigurationService.load(smsconf.id);
		SmsChannelConfigurationData smsData = binding.channelConfigurationService.getData(smsconf.id);

		return [
			password:           smsconf.password,
			username:           smsconf.username,
			isPasswordSet:      smsconf.isPasswordSet(),
			mobileNumberSource: smsconf.mobileNumberSource,
			smsMessageSource:   smsconf.smsMessageSource,
			inboundSmsUrl:      smsData.inboundSmsUrl,
			id:                 smsconf.id,

		]
	}
}

//ScriptBindingBP scriptBindingBP = new ScriptBindingBP(binding);

abstract interface bpSimpleObject
{
	abstract ScriptBindingBP                getContext();
}

//------  libScriptBinding ------------------
//===========================================
