if "%output%"=="" goto :EOF

set output=%output:\\=\%

pushd %~dp0..\..\..\..\..
call set filename=%%output:!cd!\=%%
popd

echo // %filename% >%output%
echo // Built by: %USERNAME%@%COMPUTERNAME% >>%output%
echo // Built at: %date% %time% >>%output%
echo //>>%output%
