//===========================================
//------  libWsSMS  -------------------------
//
// @version: 1.1.14

import java.util.LinkedHashMap;
import org.apache.bcel.generic.RETURN
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils
import org.cyclos.entities.messaging.InboundSms;
import org.cyclos.entities.system.CustomWebService;
import org.cyclos.entities.users.BasicUser
import org.cyclos.entities.users.Group;
import org.cyclos.entities.users.Record;
import org.cyclos.entities.users.RecordCustomField
import org.cyclos.entities.users.RecordCustomFieldPossibleValue
import org.cyclos.entities.users.RecordType
import org.cyclos.entities.users.SystemRecord
import org.cyclos.entities.users.SystemRecordType
import org.cyclos.entities.users.User
import org.cyclos.entities.users.User;
import org.cyclos.entities.users.UserRecord
import org.cyclos.entities.users.UserRecordType
import org.cyclos.impl.system.ScriptHelper
import org.cyclos.impl.users.RecordServiceLocal
import org.cyclos.impl.utils.persistence.EntityManagerHandler
import org.cyclos.impl.utils.sms.InboundSmsBasicData
import org.cyclos.impl.utils.sms.InboundSmsData;
import org.cyclos.model.Bean
import org.cyclos.model.access.RequestData;
import org.cyclos.model.EntityNotFoundException
import org.cyclos.model.messaging.sms.OutboundSmsStatus
import org.cyclos.model.system.fields.CustomFieldPossibleValueCategoryVO;
import org.cyclos.model.system.fields.CustomFieldPossibleValueQuery;
import org.cyclos.model.system.fields.CustomFieldPossibleValueVO;
import org.cyclos.model.system.fields.CustomFieldVO
import org.cyclos.model.system.fields.CustomFieldValueForSearchDTO
import org.cyclos.model.users.records.RecordDTO
import org.cyclos.model.users.records.RecordDataParams
import org.cyclos.model.users.records.SystemRecordDTO
import org.cyclos.model.users.records.SystemRecordVO
import org.cyclos.model.users.records.UserRecordDTO
import org.cyclos.model.users.records.UserRecordQuery;
import org.cyclos.model.users.records.UserRecordVO
import org.cyclos.model.users.recordtypes.RecordTypeVO
import org.cyclos.model.users.users.UserDetailedVO
import org.cyclos.model.users.users.UserLocatorVO
import org.cyclos.model.users.users.UserVO;
import org.cyclos.model.utils.RequestInfo;
import org.cyclos.model.utils.ResponseInfo;
import org.cyclos.utils.ParameterStorage;


import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.TEXT
import org.apache.http.StatusLine
import org.codehaus.groovy.GroovyException;


class WsSms
	extends ScriptBindingBP
{
	private List<WsSmsProvider>         providers           = [];
	public WsSmsProvider                p;
	private Boolean                     isAutoInit          = true;
	private String                      forceProvider;
	protected String                    sms_provider_field_name;


	WsSms(ScriptBindingBP scriptBinding)
	{
		super(scriptBinding);

		forceProvider           = scriptParameters[
									"sms.outgoing.provider.force"
								];
		sms_provider_field_name = scriptParameters[
									"sms.outgoing.provider.userProfileField"
								];
	}

	public Boolean initAutoProviders(String name, String function)
	{
		Boolean added = false;

		if (isAutoInit)
		{
			Class[] types = new Class[1];

			types[0] = WsSms;

			// load autoProviders
			for (Class<WsSmsProvider> clazz : autoProviders)
			{
				if (name != null && name.toUpperCase() != clazz.providerName.toUpperCase())
				{
					continue;
				}

				try
				{
					if (function != null
					&&  clazz.getMethod(function, (Class<?>[]) null) == null
					) continue;
				}
				catch (NoSuchMethodException | SecurityException e)
				{
				  continue;
				}

				addProvider(this.context.newInstance(clazz, types, this));
				added = true;

				if (name != null)
				{
					break;
				}
			}

		}

		return added;
	}

	public addProvider(WsSmsProvider provider)
	{
		isAutoInit  = false;
		p           = provider
		providers.push(provider);
	}

	public Boolean findProvider(String name)
	{
		return findProvider(name, (String) null);
	}

	public Boolean findProvider(String name, String function)
	{
		for (WsSmsProvider p : providers)
		{
			if (
				name == null
			||
				(
					name.toUpperCase() == p.providerName.toUpperCase()
				&& (function == null || p.respondsTo(function))
				)
			)
			{
			   this.p = p;
			   return true
			}
		}

		return initAutoProviders(name, function);
	}

	public List<WsSmsProvider> findProviders(String function)
	{
		initAutoProviders(null, function);

		List<WsSmsProvider> ps = [];

		for (WsSmsProvider p : providers)
		{
			if (function == null || p.respondsTo(function))
			{
				ps.push(p);
			}
		}

		return ps;
	}

	public WsSmsProviderSender getSender()
	{
		if (forceProvider == null
		&&  binding.hasVariable("phone")
		)
		{
			return getSender(binding.phone.user);
		}

		return getSender((BasicUser) null);
	}

	public WsSmsProviderSender getSender(String usernameOrPhoneNumber)
	{
		if (forceProvider == null)
		{
			if (StringUtils.isNumeric(usernameOrPhoneNumber))
			{
				if (userWithMobilePhoneExists(usernameOrPhoneNumber))
				{
					return getSender(findUserByPhoneNumber(usernameOrPhoneNumber));
				}
			}
			else
			{
				return getSender(getUser(usernameOrPhoneNumber));
			}
		}

		return getSender((BasicUser) null);
	}

	public WsSmsProviderSender getSender(BasicUser user)
	{
		String     providerName;

		if (forceProvider == null
		&&  sms_provider_field_name != null)
		{
			try
			{
				String  s       = getEnumeratedStringValueOrDefault(
									user,
									sms_provider_field_name,
									"system"
									);
				String[]    parts   = s.split("_");
				providerName        = parts[0];
			}
			catch(EntityNotFoundException ex)
			{
				//fail gracefully
			}
		}
		else
		{
			providerName        = forceProvider
		}

		if (findProvider(providerName, "getSender"))
		{
			return p.sender;
		}

		fail();
	}

	protected fail()
	{
		if (providers.size == 0)
		{
			throw new WsSmsNoRequestProviderRegisteredException();
		}

		throw new WsSmsNoRequestProviderSuitableException();
	}

	static List<Class<WsSmsProvider>> autoProviders = [];

	public static addProvider(Class<WsSmsProvider> provider)
	{
		for (Class<WsSmsProvider> clazz : autoProviders)
		{
			if (provider == clazz)
			{
			   return true;
			}
		}

		autoProviders.push(provider);
	}

	public static WsSmsProviderSender newSender(ScriptBindingBP scriptBinding, String usernameOrPhoneNumber)
	{
		WsSms wsSms = new WsSms(scriptBinding);

		return wsSms.getSender(usernameOrPhoneNumber);
	}

	public static WsSmsProviderSender newSender(ScriptBindingBP scriptBinding)
	{
		WsSms wsSms = new WsSms(scriptBinding);

		return wsSms.sender;
	}

	public static ResponseInfo processRequest(ScriptBindingBP scriptBinding)
	{
		WsSmsRequestHandler wsSms = new WsSmsRequestHandler(scriptBinding);

		try
		{
			return wsSms.processRequest()
		}
		catch(Exception ex)
		{
			return LibWs.ResponseInfo(ex);
		}
	}
}

class WsSmsRequestHandler
	extends WsSms
{
	WsSmsProviderRequestHandler     rh;
	WsLog                           wsLog;
	StatusLine                      result;

	WsSmsRequestHandler(ScriptBindingBP scriptBinding)
	{
		super(scriptBinding);

		wsLog   = new WsLog(this);

		result  = new org.apache.http.message.BasicStatusLine
				( new org.apache.http.ProtocolVersion("HTTP",1,1)
				, 500, "Unknown Error");

		wsLog.smsHostIp                   = this.request.requestData.remoteAddress;
		wsLog.requestMethod               = this.request.method;
		wsLog.uri                         = this.request.requestData.uri;
		wsLog.requestData                 = [
			BaseUrl:                        this.request.requestData.getBaseUrl(),
			ConfigurationId:                this.request.requestData.getConfigurationId(),
			getConsumedPaths:               this.request.requestData.getConsumedPaths(),
			getLocales:                     this.request.requestData.getLocales(),
			getNetworkId:                   this.request.requestData.getNetworkId(),
			getNetworkInternalName:         this.request.requestData.getNetworkInternalName(),
			getRemoteAddress:               this.request.requestData.getRemoteAddress(),
			getRootUrl:                     this.request.requestData.getRootUrl(),
			getUri:                         this.request.requestData.getUri(),
			isCustomUrl:                    this.request.requestData.isCustomUrl(),
			isDefaultGlobal:                this.request.requestData.isDefaultGlobal(),
			isProcessedIds:                 this.request.requestData.isProcessedIds(),
		]
		wsLog.parameters                  = this.request.parameters;
		wsLog.cookies                     = this.request.cookies;
		wsLog.headers                     = this.request.headers;

		wsLog.save();
	}

	public ResponseInfo processRequest()
	{
		try
		{
			for (WsSmsProvider p : findProviders("getRequestHandler"))
			{
				rh  = p.requestHandler;

				if (rh == null || !rh.parse())
				{
					continue;
				}

				this.p = p;

				if (rh instanceof IWsSmsInboundSms)
				{
					// check if user preferenc on provider needs to be updated
					MobilePhone mobilePhone;

					if (sms_provider_field_name != null)
					{
						mobilePhone = findMobilePhone(rh.sms.phoneNumber);
					}

					if (mobilePhone != null)
					{
						String      s                   = getEnumeratedStringValueOrDefault(mobilePhone.user, sms_provider_field_name, "system");
						String[]    smsProviderOption   = s.split("_");

						if (smsProviderOption[1].toLowerCase() == "sticky"
						&&  smsProviderOption[0].toLowerCase() != p.providerName.toLowerCase()
)
						{
							def wrapped = scriptHelper.wrap(mobilePhone.user);

							wrapped[sms_provider_field_name] = p.providerName.toLowerCase() +"_"+ "sticky";

							wsLog.log = "provider changed to "+p.providerName.toLowerCase() +"_"+ "sticky!";
						}
					}

					StatusLine  result  = rh.sms.process();

					wsLog.log = [
						logId: wsLog.id,
						SMS: rh.sms,
						result: result,
						log: wsLog.log + " " + rh.sms.log,
					];

					return LibWs.wsCreateResponse(result.statusCode, result.reasonPhrase);
				}

				if (rh instanceof IWsSmsOutboundSmsStatus)
				{
					Boolean  result  = rh.status.process();

					wsLog.log = [
						logId: wsLog.id,
						result: result,
						log: rh.status.log,
					];

					return LibWs.wsCreateResponse(200);
				}
			}

			fail();
		}
		catch(Exception ex)
		{
			if (wsLog != null)
			{
				try
				{
					wsLog.log = [
						logId: wsLog.id,
						sms: status?.sms,
						result: result,
						exeption: [ex],
					];

					wsLog.save();
				}
				catch(Exception ex2)
				{
					//ignore
				}
			}

			return LibWs.wsCreateResponse(ex);
		}
	}
}

abstract class WsSmsProvider
	extends ScriptBindingBP
{
	protected WsSms                     wsSms;
	protected WsSmsProvider             provider;
	protected String                    src_number;
	protected WsLog                     wsLog;

	abstract public static String       providerName;

	WsSmsProvider(WsSms wsSms)
	{
		super(wsSms);

		this.wsSms       = wsSms;
		this.provider    = this;
	}

	public String getSrcNumber()
	{
		return this.src_number;
	}

	public WsSms getWsSms()
	{
		return this.wsSms;
	}

	protected WsSmsProviderRequestHandler getRequestHandler(WsSmsProviderRequestHandler rh)
	{
		if (rh?.isIdentified())
		{
			this.src_number = rh.srcNumber;

			return rh;
		}

		throw new WsSmsRequestInvalidException();
	}
}


abstract class WsSmsProviderActor
	extends ScriptBindingBP
{
	protected WsSmsProvider             provider;

	WsSmsProviderActor(WsSmsProvider provider)
	{
		super(provider);

		this.provider = provider;
	}

	public WsSmsProvider getProvider()
	{
		return this.provider;
	}

	public          WsSmsLog            newSmsLog(Long userId)
	{
		return new WsSmsLog(this, userId);
	}

	public          WsSmsLog            newSmsLog(String logIdMask)
	{
		return new WsSmsLog(this, logIdMask);
	}

	public          WsSmsLog            newSmsLog(
					String              direction,
					String              vmn,
					String              phoneNumber,
					String              message
	)
	{
		return newSmsLog(
			this.findUserIdByPhoneNumber(phoneNumber),
			direction,
			vmn,
			phoneNumber,
			message
			)
	}

	public          WsSmsLog            newSmsLog(
					Long                userId,
					String              direction,
					String              vmn,
					String              phoneNumber,
					String              message
	)
	{
		WsSmsLog smsLog         = newSmsLog(userId);

		smsLog.smsDirection     = direction.toLowerCase();

		try
		{
			smsLog.smsVmn           = vmn;
		}
		catch(EntityNotFoundException ex)
		{
			if (ex.entityType == "RecordCustomFieldPossibleValue")
			{
				throw new GroovyException(
				"Incoming VMN ${vmn} not registered as a \
				possible value of the custom field 'smsVmn'."
				);
			}
		}
		smsLog.smsMobile        = phoneNumber;
		smsLog.smsText          = userMaskPassword(getUser(userId), message);

		if (direction.toLowerCase() == "out")
		{
			smsLog.smsCountQueued   = 0 ;
			smsLog.smsCountSent     = 0 ;
			smsLog.smsCountDeliverd = 0 ;
		}

		smsLog.save();

		return smsLog;
	}

	public String getParameter(String parameter)
	{
		String providerName = provider.providerName.toLowerCase();

		if (scriptParameters["${providerName}.sms.${parameter}"] != null)
		{
			return scriptParameters["${providerName}.sms.${parameter}"];
		}

		if (scriptParameters["sms.${parameter}"] != null)
		{
			return scriptParameters["sms.${parameter}"];
		}

		return null;
	}
}

abstract class WsSmsProviderSender
	extends WsSmsProviderActor
{
	WsSmsProviderSender(WsSmsProvider provider)
	{
		super(provider);
	}

	public abstract OutboundSmsStatus   send(String phoneNumber, String message);

	public String prepareMessage(String message)
	{
		return prepareMessage(message, null)
	}

	public String prepareMessage(String message, String phoneNumber)
	{
		return (getParameter("outgoing.prepend")?:"") + message + (getParameter("outgoing.append")?:"");
	}
}

abstract class WsSmsProviderRequestHandler
	extends WsSmsProviderActor
{
	protected Boolean                   isValid         = false;
	protected WsSmsRequestParameterNames    parameterNames;

	WsSmsProviderRequestHandler(WsSmsProvider provider)
	{
		super(provider);
	}

	public abstract Boolean             isIdentified();

	public Boolean parse()
	{
		// default parse function for incoming sms

		assertValid();

		sms = new WsSmsInboundSms(
			this,
			parameterNames
		)

		return true;
	}

	public String getSrcNumber()
	{
		this.assertValid();

		return request.parameters.getString(parameterNames.vmn);
	}

	protected assertValid()
	{
		if (parameterNames == null)
		{
			throw new WsSmsNoRequestProviderConfigurationException("parameterNames not set.");
		}

		if (!isIdentified())
		{
			throw new WsSmsRequestNotIdentifiedException();
		}

		parameterNames.assertValid();

		isValid = true;
	}

	protected WsSmsRequestParameterNames \
	newParameterNames(
		String                          vmn,
		String                          phoneNumber,
		String                          message
	)
	{
		return new WsSmsRequestParameterNames(
			request,
			vmn,
			phoneNumber,
			message,
			null
			);
	}

	protected WsSmsRequestParameterNames \
	newParameterNames(
		String                          vmn,
		String                          phoneNumber,
		String                          message,
		LinkedHashMap<String, String>   other
	)
	{
		return new WsSmsRequestParameterNames(
			request,
			vmn,
			phoneNumber,
			message,
			other
			);
	}

	protected WsSmsRequestParameterNames \
	newParameterNames(
		String                          vmn,
		String                          phoneNumber,
		LinkedHashMap<String, String>   other
	)
	{
		return new WsSmsRequestParameterNames(
			request,
			vmn,
			phoneNumber,
			null,
			other
			);
	}

}


abstract class WsSmsLogIdent
{
	private String id;

	WsSmsLogIdent(String id)
	{
		this.id = id;
	}

	public String toString()
	{
		return id;
	}

	public String getId()
	{
		return id;
	}
};

class WsSmsLogIdentIdMask
	extends WsSmsLogIdent
{
	WsSmsLogIdentIdMask(String id)
	{
		super(id);
	}
}

class WsSmsLogIdentPhoneNumber
	extends WsSmsLogIdent
{
	private String phoneNumber;

	WsSmsLogIdentPhoneNumber(String messageId, String phoneNumber)
	{
		super(messageId);

		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}
}

interface IWsSmsInboundSms
{
	public WsSmsInboundSms sms;
}

class WsSmsInboundSms
	extends org.cyclos.impl.utils.sms.InboundSmsBasicData
{
	private WsSmsRequestParameterNames      parameterNames;
	public WsSmsProviderActor           actor;
	protected String                    log             = "";

	WsSmsInboundSms(
		WsSmsProviderActor              actor,
		WsSmsRequestParameterNames          parameterNames
		)
	{
		super();

		this.actor              = actor;
		this.parameterNames     = parameterNames;
		this.phoneNumber        = parameterNames.get("phoneNumber");
		this.message            = parameterNames.get("message");
	}

	WsSmsInboundSms(
		WsSmsProviderActor              actor,
		String                          phoneNumber,
		String                          message
		)
	{
		super();

		this.actor              = actor;
		this.phoneNumber        = phoneNumber;
		this.message            = message;
	}

	public String \
	getInterfaceNumber()
	{
		return  ( parameterNames?.isSetValue("vmn")
				? parameterNames.get("vmn")
				: actor.srcNumber
				);
	}

	public StatusLine process()
	{
		StatusLine  statusLine;
		WsSmsLog    smsLog      = actor.newSmsLog(
									"in",
									this.interfaceNumber,
									this.phoneNumber,
									this.message
									);

		if (parameterNames?.isSetValue("uuid"))
		{
			smsLog.smsUuid      = parameterNames.get("uuid");
		}

		this.message            = this.message?.trim();
		String command          = StringUtils.substringBefore(
									this.message, " ").toLowerCase();
		String autoReply        = actor.getParameter("${command}.autoreply")
								?: actor.getParameter("autoreply");

		if (autoReply != null)
		{
			log("autoreply:${autoReply}");

			actor.provider.sender.send(
				  this.phoneNumber
				, autoReply
			);
		}

		if (actor.getParameter("${command}.noForward")  != null
		||  actor.getParameter("noForward")             != null
		)
		{
			log("noForward");

			smsLog.smsStatus    = "rejectd";

			statusLine = new org.apache.http.message.BasicStatusLine
				( new org.apache.http.ProtocolVersion("HTTP",1,1)
				, 200, "OK");
		}
		else
		{
			statusLine =  this.forwardSms(smsLog.idMask);

			if (statusLine.statusCode == 200)
			{
				smsLog.smsStatus    = "delivrd";
			}
			else
			{
				smsLog.smsStatus    = "error";
			}
		}

		smsLog.smsError = this.log;

		return statusLine;
	}

	public StatusLine forwardSms()
	{
		return this.forwardSms(null);
	}

	public StatusLine forwardSms(String idMask)
	{
		StatusLine  statusLine;

		Object      smsConfig   = actor.smsChannelConfigBp;
		HTTPBuilder http        = new HTTPBuilder(smsConfig.inboundSmsUrl);

		if (smsConfig.isPasswordSet)
		{
			http.auth.basic(smsConfig.username, smsConfig.password);
		}

		try
		{
			http.get(
					query : [
						 from: this.phoneNumber,
						 text: this.message,
						 id: idMask,
	//                       contentType : TEXT,
					 ]
					)
			{ resp, reader ->

								statusLine  = resp.getStatusLine();
				BufferedReader  buffer      = new BufferedReader(reader);
				String          body        = buffer.readLine();
				buffer.close();

				log("response status: ${resp.statusLine}");
				log('Headers: -----------');
				resp.headers.each { h ->
					log(" ${h.name} : ${h.value}");
				}

				// Read the server's response
				log('Response data: -----');
				log(body);
				log('--------------------');
			}
		}
		catch(groovyx.net.http.HttpResponseException ex)
		{
			return ex.response.statusLine;
		}
		return statusLine;
	}

	public String getLog()
	{
		return this.log;
	}

	protected log(String line)
	{
		this.log = this.log + line + "\n";
	}
}


interface IWsSmsOutboundSmsStatus
{
	public WsSmsOutboundSmsStatus status;
}


class WsSmsOutboundSmsStatus
{
	public WsSmsProvider                provider;
	public String                       vmn;
	public WsSmsLogIdent                logId;
	public String                       phoneNumber;
	public String                       status;
	public String                       network;
	private Object                      parameters;

	public String                       log             = "";

	WsSmsOutboundSmsStatus(
		WsSmsProvider                   provider,
		WsSmsLogIdent                   logId,
		String                          status,
		Object                          parameters
		)
	{
		this.provider           = provider;
		this.logId              = logId;
		this.status             = status;

		if (parameters.hasProperty("MCC") && parameters.MCC != null)
		{
			String mcc = parameters.MCC;
			String mnc = parameters.MNC;

			this.network = "${mcc}.${mnc}";
		}

	}

	public Boolean process()
	{
		WsSmsLog smsLog;

		try
		{
			if (logId instanceof WsSmsLogIdentPhoneNumber)
			{
				smsLog                  = new WsSmsLog(
											provider,
											provider.srcNumber,
											logId.phoneNumber,
											logId.id
											);
				log                     = parameters;
			}
			else if (logId instanceof WsSmsLogIdentIdMask)
			{
				smsLog                  = new WsSmsLog(provider, logId.toString());
			}
			else
			{
				throw new GroovyException("No log identification recognised");
			}
		}
		catch(WsRecordNotFoundException ex)
		{
			log = log + "\n no log entry found;"
			return false;
		}

		switch (this.status.toLowerCase())
		{
			case "queued":
			case "acceptd":
				if (smsLog.smsCountQueued + 1 == smsLog.smsParts)
				{
					smsLog.smsStatus    = "acceptd";
				}
				smsLog.smsNetwork       = this.network;
				smsLog.smsCountQueued   = smsLog.smsCountQueued + 1;

				return true;

			case "sent":
				if (smsLog.smsCountSent + 1 == smsLog.smsParts)
				{
					smsLog.smsStatus    = "sent";
				}
				smsLog.smsNetwork       = this.network;
				smsLog.smsCountSent     = smsLog.smsCountSent + 1;
				return true;

			case "delivered":   //Plivo
			case "delivrd":     //HSL
				if (smsLog.smsCountDeliverd + 1 == smsLog.smsParts)
				{
					smsLog.smsStatus    = "delivrd";
				}
				smsLog.smsNetwork       = this.network;
				smsLog.smsCountDeliverd = smsLog.smsCountDeliverd + 1;
				return true;

			default:
				smsLog.smsStatus    = "unknown";
				throw new Exception("Status '"+status+"' not recognised.");
		}
	}

	public String getLog()
	{
		return this.log;
	}

	protected log(String line)
	{
		this.log = this.log + line + "\n";
	}
}


class WsSmsRequestParameterNames
{
	private RequestInfo request;

	public LinkedHashMap<String, String> parameterNames;

	WsSmsRequestParameterNames(
		RequestInfo         request,
		String              vmn,
		String              phoneNumber,
		String              message,
		LinkedHashMap<String, String>
							other
	)
	{
		this.request        = request;
		this.parameterNames = other ?: [:];

		this.parameterNames.put("vmn",          vmn);
		this.parameterNames.put("phoneNumber",  phoneNumber);

		if (message != null)
		{
			this.parameterNames.put("message",  message);
		}
	}

	public getVmn()
	{
		return this.parameterNames.getAt("vmn");
	}

	public getPhoneNumber()
	{
		return this.parameterNames.getAt("phoneNumber");
	}

	public getMessage()
	{
		return this.parameterNames.getAt("message");
	}

	public Boolean isSetName(String parameter)
	{
		return this.parameterNames.keySet().contains(parameter);
	}

	public Boolean isSetValue(String parameter)
	{
		return (    isSetName(parameter)
				&&  request.parameters.isSet(this.parameterNames.getAt(key))
				);
	}

	public String getName(String parameter)
	{
		String key;

		if (!isSetName(parameter))
			return null;

		return this.parameterNames.getAt(parameter);
	}

	public String get(String parameter)
	{
		if (!isSetName(parameter))
			return null;

		return request.parameters.getString(
				this.parameterNames.getAt(parameter)
				);
	}

	public assertValid()
	{
		if (!isSetName("phoneNumber"))
		{
			throw new WsSmsRequestInvalidException("phoneNumber parameter unknown.");
		}

		if (!isSetName("vmn"))
		{
			throw new WsSmsRequestInvalidException("vmn parameter unknown.");
		}

		for(String key : this.parameterNames.keySet())
		{
			if (!request.parameters.isSet(this.parameterNames.getAt(key)))
			{
				throw new WsSmsRequestInvalidException("${key} not provided.");
			}
		}
	}
}


class WsSmsLog
	extends WsRecord
{
	public static final String recordTypeName = "smsLogUser";

	WsSmsLog(ScriptBindingBP scriptBinding, Long userId)
	{
		super(
			scriptBinding,
			recordTypeName,
			new UserLocatorVO(id: userId)
		);
	}

	WsSmsLog(ScriptBindingBP scriptBinding, String logIdMask)
	{
		super(
			scriptBinding,
			recordTypeName,
			scriptBinding.applicationHandler.idMask.remove(Long.parseLong(logIdMask))
		);
	}

	WsSmsLog(
		ScriptBindingBP scriptBinding,
		String          vmn,
		String          phoneNumber,
		String          uuid
	)
	{
		super(scriptBinding);

		setRecordType(recordTypeName);

		try
		{
			this.record = scriptBinding.getUserCustomRecord(
						WsSmsLog.recordTypeName,
						phoneNumber,
						[
							[name: "smsVmn", value: "x"+vmn],
							[name: "smsDirection", value: "out"],
							[name: "smsUuid", value: uuid],
						]
					)
		}
		catch(EntityNotFoundException ex)
		{
			if (ex.entityType == "RecordCustomFieldPossibleValue")
			{
				throw new GroovyException(
				"Incoming VMN ${vmn} not registered as a \
				possible value of the custom field 'smsVmn'."
				);
			}
		}
	}

	public String getSmsProvider()
	{
		for (CustomFieldPossibleValue pf : wrapped.smsVmn.getEnumeratedValues())
		{
			return pf.internalName;
		}
	}

	public String getSmsVmn()
	{
		return StringUtils.removeStart(this.wrapped["smsVmn"], "x");
	}
	public setSmsVmn(String v)
	{
		this.wrapped["smsVmn"] = StringUtils.prependIfMissing(StringUtils.removeStart(v, "+"), "x");
	}

	public String getSmsDirection()
	{
		return this.wrapped["smsDirection"];
	}
	public setSmsDirection(String v)
	{
		this.wrapped["smsDirection"] = v;
	}

	public String getSmsMobile()
	{
		return this.wrapped["smsMobile"];
	}
	public setSmsMobile(String v)
	{
		this.wrapped["smsMobile"] = StringUtils.removeStart(v, "+");
	}

	public String getSmsText()
	{
		return this.wrapped["smsText"];
	}
	public setSmsText(String v)
	{
		switch (scriptParameters["sms.log.message"])
		{
			case "full":
				break;

			case "masked":
				v = StringUtils.substringBefore(v, " ");
				break;

			default:
				return;
		}

		this.wrapped["smsText"] = v;
	}

	public String getSmsUuid()
	{
		return this.wrapped["smsUuid"];
	}
	public setSmsUuid(String v)
	{
		this.wrapped["smsUuid"] = v;
	}

	public int getSmsCountQueued()
	{
		return this.wrapped["smsCountQueued"];
	}
	public setSmsCountQueued(int v)
	{
		this.wrapped["smsCountQueued"] = v;
	}

	public int getSmsParts()
	{
		return this.wrapped["smsParts"];
	}
	public setSmsParts(int v)
	{
		this.wrapped["smsParts"] = v;
	}

	public int getSmsCountSent()
	{
		return this.wrapped["smsCountSent"];
	}
	public setSmsCountSent(int v)
	{
		this.wrapped["smsCountSent"] = v;
	}

	public int getSmsCountDeliverd()
	{
		return this.wrapped["smsCountDeliverd"];
	}
	public setSmsCountDeliverd(int v)
	{
		this.wrapped["smsCountDeliverd"] = v;
	}

	public String getSmsStatus()
	{
		return this.wrapped["smsStatus"];
	}
	public setSmsStatus(String v)
	{
		this.wrapped["smsStatus"] = v;
	}

	public String getSmsError()
	{
		return this.wrapped["smsError"];
	}
	public setSmsError(String v)
	{
		this.wrapped["smsError"] = v;
	}

	public String getSmsHostIp()
	{
		return this.wrapped["smsHostIp"];
	}
	public setSmsHostIp(String v)
	{
		this.wrapped["smsHostIp"] = v;
	}

	public String getSmsNetwork()
	{
		return this.wrapped["smsNetwork"];
	}
	public setSmsNetwork(String v)
	{
		this.wrapped["smsNetwork"] = v;
	}
}


class WsSmsNoRequestProviderRegisteredException
	extends GroovyException
{
	public WsSmsNoRequestProviderRegisteredException()
	{
	}

	public WsSmsNoRequestProviderRegisteredException(String message)
	{
		super(message);
	}

	public WsSmsNoRequestProviderRegisteredException(Throwable cause)
	{
		super(cause);
	}

	public WsSmsNoRequestProviderRegisteredException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

class WsSmsNoRequestProviderSuitableException
	extends GroovyException
{
	public WsSmsNoRequestProviderSuitableException()
	{
	}

	public WsSmsNoRequestProviderSuitableException(String message)
	{
		super(message);
	}

	public WsSmsNoRequestProviderSuitableException(Throwable cause)
	{
		super(cause);
	}

	public WsSmsNoRequestProviderSuitableException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

class WsSmsNoRequestProviderConfigurationException
	extends GroovyException
{
	public WsSmsNoRequestProviderConfigurationException()
	{
	}

	public WsSmsNoRequestProviderConfigurationException(String message)
	{
		super(message);
	}

	public WsSmsNoRequestProviderConfigurationException(Throwable cause)
	{
		super(cause);
	}

	public WsSmsNoRequestProviderConfigurationException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

class WsSmsRequestNotIdentifiedException
	extends GroovyException
{
	public WsSmsRequestNotIdentifiedException()
	{
	}

	public WsSmsRequestNotIdentifiedException(String message)
	{
		super(message);
	}

	public WsSmsRequestNotIdentifiedException(Throwable cause)
	{
		super(cause);
	}

	public WsSmsRequestNotIdentifiedException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

class WsSmsRequestInvalidException
	extends GroovyException
{
	public WsSmsRequestInvalidException()
	{
	}

	public WsSmsRequestInvalidException(String message)
	{
		super(message);
	}

	public WsSmsRequestInvalidException(Throwable cause)
	{
		super(cause);
	}

	public WsSmsRequestInvalidException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

//------  libWsSMS  -------------------------
//===========================================
