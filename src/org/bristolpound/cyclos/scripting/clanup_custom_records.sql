
SELECT
	*
FROM
	public.record_custom_fields
WHERE
	record_type_id IN (
        SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
    )
    OR
    id IN (
		SELECT field_id FROM public.record_types_shared_record_fields WHERE record_type_id IN (
            SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
		)
	)
;




DELETE 
FROM
	public.record_enum_values
WHERE
	owner_id IN (
	SELECT id FROM
        public.record_custom_field_values
    WHERE
        field_id IN (
		SELECT
            id
        FROM
            public.record_custom_fields
        WHERE
            record_type_id IN (
                SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
            )
            OR
            id IN (
                SELECT field_id FROM public.record_types_shared_record_fields WHERE record_type_id IN (
                    SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
                )
            )
    	)
	)
 

DELETE
FROM
	public.record_custom_field_values
WHERE
	field_id IN (
	SELECT
		id
    FROM
        public.record_custom_fields
    WHERE
        record_type_id IN (
            SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
        )
        OR
        id IN (
            SELECT field_id FROM public.record_types_shared_record_fields WHERE record_type_id IN (
                SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
            )
        )
        OR
        owner_id IN (
            SELECT id
            FROM public.records
            WHERE type_id IN (
                SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
            )
        )
)
;

DELETE
FROM public.record_types_shared_record_fields
WHERE record_type_id IN (
    SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
)



DELETE
FROM
	public.record_custom_field_possible_values
WHERE
	field_id IN (
	SELECT
		id
    FROM
        public.record_custom_fields
    WHERE
        record_type_id IN (
            SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
        )
	)
;


DELETE
FROM
	public.record_custom_field_value_cats
WHERE
	field_id IN (
	SELECT
		id
    FROM
        public.record_custom_fields
    WHERE
        record_type_id IN (
            SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
        )
	)
;


DELETE
FROM
	public.products_record_fields
WHERE
	product_record_type_id IN (
	SELECT
		id
    FROM
        public.products_record_types
    WHERE
        record_type_id IN (
            SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
        )
	)
;


DELETE
FROM
	public.products_record_fields
WHERE
	custom_field_id IN (
	SELECT
		id
    FROM
        public.record_custom_fields
    WHERE
        record_type_id IN (
            SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
        )
	)
;

DELETE
FROM public.products_record_types
WHERE record_type_id IN (
    SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
)



DELETE
FROM public.record_custom_fields
WHERE record_type_id IN (
    SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
)


DELETE
FROM public.records
WHERE type_id IN (
    SELECT id FROM public.record_types WHERE internal_name = 'wsLog'
)


DELETE
FROM public.record_types
WHERE internal_name = 'wsLog'

