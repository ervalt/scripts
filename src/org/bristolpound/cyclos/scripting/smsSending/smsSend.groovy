//===========================================
//------  smsSend                      ------
//
// @version: 0.0.2

import org.cyclos.model.messaging.sms.OutboundSmsStatus


try
{
    WsSmsProviderSender sms = WsSms.newSender(scriptBindingBP);

    return sms.send(
          phoneNumber
        , message
    );
}
catch (ex)
{
    return OutboundSmsStatus.UNKNOWN_ERROR
}

//------  smsSend                      ------
//===========================================
